//
//  CWorldEditor.h
//  WorldEditor
//
//  Created by Михайлов Алексей on 20.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __WorldEditor__CWorldEditor__
#define __WorldEditor__CWorldEditor__

#include "../engine/Input/interfaces/IMouseListener.h"
#include "../engine/Input/interfaces/IKeyboardListener.h"

#include "../third_party/sdl/SDL_events.h"

#include "../engine/Render/gui/CGUIButtonView.h"

class CWorldEditor : public IMouseListener, IKeyboardListener
{
public:
	CWorldEditor();
	~CWorldEditor();
	
	void handleEvent(SDL_Event* e);
	int update();

	void onMouseButtonDown(sMouseState state, eMouseButton button);
	void onMouseButtonUp(sMouseState state, eMouseButton button);
	void onMouseMove(sMouseState state);
	void onMouseWheelScroll(sMouseState state);

	void onKeyboardButtonDown(sKeyboardState state, unsigned short button);
	void onKeyboardButtonUp(sKeyboardState state, unsigned short button);
	void onKeyboardTextInput(sKeyboardState state, wchar_t character);
private:
	
};

#endif /* defined(__WorldEditor__CWorldEditor__) */
