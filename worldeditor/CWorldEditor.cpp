//
//  CWorldEditor.cpp
//  WorldEditor
//
//  Created by Михайлов Алексей on 20.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CWorldEditor.h"

#include "../engine/Core/console/CConsole.h"
#include "../engine/Core/console/CConsoleCommand.h"
#include "../engine/Core/console/CConsoleVariable.h"

#include "../engine/Input/CInputManager.h"

#include "../engine/Render/CRenderAbstract.h"

#include "../engine/Render/camera/CFreeCamera.h"

#include "../engine/Render/renderable/CRenderable2dRectangle.h"

#include "../engine/Render/gui/CGUIManager.h"
#include "../engine/Render/gui/CGUIWindowView.h"
#include "../engine/Render/gui/CGUILabelView.h"
#include "../engine/Render/gui/CGUIButtonView.h"
#include "../engine/Render/gui/CGUIScrollView.h"
#include "../engine/Render/gui/CGUITextfieldView.h"

#include "../engine/Render/texture/CTexture.h"
#include "../engine/Render/texture/CTextureManager.h"

#include "../engine/Render/renderable/CRenderable2d.h"
#include "../engine/Render/renderable/CRenderable2dText.h"
#include "../engine/Render/renderable/CRenderable3d.h"
#include "../engine/Render/renderable/CRenderable3dPyramid.h"
#include "../engine/Render/renderable/CRenderable3dCube.h"

#include "../engine/Render/shaders/CShadersManager.h"

#include "../engine/Render/CScene.h"

#include "../engine/Render/phases/CRenderPhaseGeometry.h"
#include "../engine/Render/phases/CRenderPhaseGeometrySimpleOutput.h"
#include "../engine/Render/phases/CRenderPhaseGuiOutput.h"

#include "../common/console/gui/CConsoleView.h"
#include "../common/console/commands/base_commands.h"

#include "../third_party/glm/gtc/matrix_transform.hpp"

#include <fstream>

CFreeCamera* camera = NULL;
CMouse* mouse = NULL;
CKeyboard* keyboard = NULL;

CConsoleView* pConsoleView = NULL;
CConsole* pConsole = NULL;

CScene* pScene = NULL;

CRenderPhaseGeometry* pPhaseGeometry = NULL;
CRenderPhaseGeometrySimpleOutput* pPhaseGeometryOutput = NULL;
CRenderPhaseGuiOutput* pPhaseGuiOutput = NULL;

int exitcode = 0;

void testcmd(CConsole *console, int argc, wchar_t* argv[]) {
	console->log(L"argc: %d\n", argc);

	for (int i = 0; i < argc; i++) {
		console->log(L"argv[%d]: %ls\n", i, argv[i]);
	}

	console->log(L"\n");
}

int testvar = 9;

CRenderable3dPyramid* pyramid = NULL;
//!!! тестовый кубик !!!
CRenderable3dCube* cubik = NULL;

CWorldEditor::CWorldEditor()
{
	std::ifstream fs;
	
	int x, y, w, h;
	CInputManager::getInstance()->windowRect(&x, &y, &w, &h);

	pConsole = new CConsole();

	base_register_commands(pConsole);

	pConsole->
		registerSymbol(L"testcmd", new CConsoleCommand(testcmd, L"test command"))->
		registerSymbol(L"testvar", new CConsoleVariable<int>(&testvar, L"test variable"));

	// render

	CRenderAbstract* render = CRenderAbstract::getInstance();

	render->init(NULL);

	camera = new CFreeCamera(glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f), glm::translate(glm::mat4(), glm::vec3(0, 0, -3.0f)));
	render->setCamera(camera);

	CShadersManager* sm = render->shadersManager();
	
	fs.open("shaders/mesh.xml", std::fstream::in);
	if(!fs.fail()) {
		CShaderProgram* sp = sm->loadShaderProgram("3d", fs);
		
		CRenderable3d::setShaderProgram(sp);
		
		fs.close();
	}
	
	fs.open("shaders/2d.xml", std::fstream::in);
	if(!fs.fail()) {
		CShaderProgram* sp = sm->loadShaderProgram("2d", fs);
		
		CRenderable2d::setShaderProgram(sp);
		
		fs.close();
	}
	
	fs.open("shaders/text.xml", std::fstream::in);
	if(!fs.fail()) {
		CShaderProgram* sp = sm->loadShaderProgram("text", fs);
		
		CRenderable2dText::setShaderProgram(sp);
		
		fs.close();
	}
	
	pScene = new CScene();
	
	pPhaseGeometry = new CRenderPhaseGeometry(w, h);
	pPhaseGeometry->setScene(pScene);
	pPhaseGeometryOutput = new CRenderPhaseGeometrySimpleOutput(w, h);
	pPhaseGeometryOutput->setRenderPhaseGeometry(pPhaseGeometry);
	pPhaseGuiOutput = new CRenderPhaseGuiOutput(w, h);
	
	fs.open("shaders/gbuffer.xml", std::fstream::in);
	if(!fs.fail()) {
		CShaderProgram* sp = sm->loadShaderProgram("gbuffer", fs);
		
		pPhaseGeometry->setShaderProgram(sp);
		
		fs.close();
	}
	
	CShaderProgram* outputShader = NULL;
	CShaderProgram* lightShader = NULL;
	
	fs.open("shaders/simple.xml", std::fstream::in);
	if(!fs.fail()) {
		outputShader = sm->loadShaderProgram("simple", fs);
		
		fs.close();
	}
	
	fs.open("shaders/light.xml", std::fstream::in);
	if(!fs.fail()) {
		lightShader = sm->loadShaderProgram("light", fs);
		
		fs.close();
	}
	
	pPhaseGeometryOutput->setShaderPrograms(outputShader, lightShader);
	
	pyramid = new CRenderable3dPyramid();
	//pyramid->setPosition(-1.0f, 0.0f, 0.0f);
	render->loadBuffers(pyramid);
	
	cubik = new CRenderable3dCube();
	//cubik->setPosition(1.0f, 0.0f, 0.0f);
	render->loadBuffers(cubik);
	
	pScene->addObject(pyramid);
	//pScene->addObject(cubik);
	
	// GUI
	pConsoleView = new CConsoleView();
	pConsoleView->
		setConsole(pConsole)->
		setHidden(true);
	
	render->guiManager()->addView(pConsoleView);
	
	//int cx = w/2.;
	//int cy = h/2.;

	mouse = CInputManager::getInstance()->getMouse();
	//mouse->setCursorVisibility(false);
	//mouse->setPosition(cx, cy);
	mouse->addListener(this);

	keyboard = CInputManager::getInstance()->getKeyboard();
	keyboard->addListener(this);
}

CWorldEditor::~CWorldEditor()
{
	
}

void CWorldEditor::handleEvent(SDL_Event* e)
{
	switch(e->type) {
		case SDL_WINDOWEVENT:
			switch(e->window.event) {
				case SDL_WINDOWEVENT_RESIZED:
				{
					int w = e->window.data1;
					int h = e->window.data2;
					
					if(pPhaseGeometry) delete pPhaseGeometry;
					if(pPhaseGeometryOutput) delete pPhaseGeometryOutput;
					if(pPhaseGuiOutput) delete pPhaseGuiOutput;
					
					pPhaseGeometry = new CRenderPhaseGeometry(w, h);
					pPhaseGeometry->setScene(pScene);
					pPhaseGeometryOutput = new CRenderPhaseGeometrySimpleOutput(w, h);
					pPhaseGeometryOutput->setRenderPhaseGeometry(pPhaseGeometry);
					pPhaseGuiOutput = new CRenderPhaseGuiOutput(w, h);
					
					CRenderAbstract::getInstance()->resize(e->window.data1, e->window.data2);
					break;
				}
				case SDL_WINDOWEVENT_CLOSE:
					exitcode = 1;
					break;
			}
			break;
			
		default:
			CInputManager::getInstance()->handleEvent(e);
			break;
	}
}

int CWorldEditor::update()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	CRenderPhaseAbstract* phases[3];
	phases[0] = pPhaseGeometry;
	phases[1] = pPhaseGeometryOutput;
	phases[2] = pPhaseGuiOutput;
	
	render->render(&(phases[0]), 3);

	if (!pConsoleView->isHidden()) {
		return exitcode;
	}

	sKeyboardState state = keyboard->getState();
	
	if(state.keys[SDL_SCANCODE_W]) camera->moveLocal(glm::vec3(0.0f, 0.0f, 0.1f));
	if(state.keys[SDL_SCANCODE_S]) camera->moveLocal(glm::vec3(0.0f, 0.0f, -0.1f));
	if(state.keys[SDL_SCANCODE_A]) camera->moveLocal(glm::vec3(0.1f, 0.0f, 0.0f));
	if(state.keys[SDL_SCANCODE_D]) camera->moveLocal(glm::vec3(-0.1f, 0.0f, 0.0f));
	
	if(state.keys[SDL_SCANCODE_UP]) camera->moveGlobal(glm::vec3(0.0f, 0.0f, 0.1f));
	if(state.keys[SDL_SCANCODE_DOWN]) camera->moveGlobal(glm::vec3(0.0f, 0.0f, -0.1f));
	if(state.keys[SDL_SCANCODE_LEFT]) camera->moveGlobal(glm::vec3(0.1f, 0.0f, 0.0f));
	if(state.keys[SDL_SCANCODE_RIGHT]) camera->moveGlobal(glm::vec3(-0.1f, 0.0f, 0.0f));
	
	return exitcode;
}

void CWorldEditor::onMouseButtonDown(sMouseState state, eMouseButton button)
{
	CRenderAbstract::getInstance()->guiManager()->input(button, true, (float)state.x, (float)state.y);
	
	switch(button) {
		case MOUSE_BUTTON_LEFT:
			// nope
			break;
		case MOUSE_BUTTON_RIGHT:
			// nope
			break;
		case MOUSE_BUTTON_MIDDLE:
			// nope
			break;
	}
}

void CWorldEditor::onMouseButtonUp(sMouseState state, eMouseButton button)
{
	CRenderAbstract::getInstance()->guiManager()->input(button, false, (float)state.x, (float)state.y);

}

void CWorldEditor::onMouseMove(sMouseState state)
{
	CRenderAbstract::getInstance()->guiManager()->input((float)state.x, (float)state.y, (float)state.xrel, (float)state.yrel);
	
	if (!pConsoleView->isHidden()) {
		return;
	}
	//return; // not move camera!
	
	int x, y, w, h;
	CInputManager::getInstance()->windowRect(&x, &y, &w, &h);
	
	int cx = w/2.;
	int cy = h/2.;
	
	if(state.x == cx && state.y == cy) return;
	
	camera->rotateCamera(state.xrel * 0.01f, state.yrel * 0.01f);
	
	mouse->setPosition(cx, cy);
}

void CWorldEditor::onMouseWheelScroll(sMouseState state)
{
	glm::vec3 pos = camera->getPosition();
	
	pos.z += state.wheel * 0.1f;
	
	camera->setPosition(pos);
}

void CWorldEditor::onKeyboardButtonDown(sKeyboardState state, unsigned short key)
{
	CRenderAbstract::getInstance()->guiManager()->input(key, true);
	
	switch(key) {
		case SDL_SCANCODE_GRAVE:
			pConsoleView->setHidden(!pConsoleView->isHidden());
			break;
		case SDL_SCANCODE_ESCAPE:
			exitcode = 1;
			break;
		case SDL_SCANCODE_1:
			camera->setPosition(glm::vec3(1.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_2:
			camera->setPosition(glm::vec3(2.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_3:
			camera->setPosition(glm::vec3(3.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_4:
			camera->setPosition(glm::vec3(4.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_5:
			camera->setPosition(glm::vec3(5.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_6:
			camera->setPosition(glm::vec3(6.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_7:
			camera->setPosition(glm::vec3(7.0f, 0.0f, -3.0f));
			break;
		case SDL_SCANCODE_8:
			camera->setPosition(glm::vec3(8.0f, 0.0f, -3.0f));
			break;
	}
}

void CWorldEditor::onKeyboardButtonUp(sKeyboardState state, unsigned short key)
{
	CRenderAbstract::getInstance()->guiManager()->input(key, false);
	
}

void CWorldEditor::onKeyboardTextInput(sKeyboardState state, wchar_t character)
{
	CRenderAbstract::getInstance()->guiManager()->input(character);
}