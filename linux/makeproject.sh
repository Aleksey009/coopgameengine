#/bin/bash 

cd build/

DEFAULT_LIBS="-lGLEW -lGL -lglut -lGLU -lm -lglfw -lSDL2 -lfreeimage `freetype-config --libs` -lstdc++"
LIBS="Core Input Render"

# compile libs
for lib in $LIBS; do
	for f in `find ../../engine/$lib -name '*.cpp'`; do
		gcc -fPIC -c -std=c++11 $f
	done

	gcc -shared -o lib$lib.so *.o
	ar rc lib$lib.a *.o
	ranlib lib$lib.a
	rm *.o
done


# compile Third Party lib
gcc -fPIC -c -xc ../../third_party/freetype-gl/*.c -I/usr/include/freetype2
gcc -fPIC -c ../../third_party/pugixml/*.cpp

gcc -shared -o libthird_party.so *.o
ar rc libthird_party.a *.o
ranlib libthird_party.a
rm *.o


# compile WorldEditor
for f in `find ../../worldeditor -name '*.cpp'`; do
	gcc -fPIC -c -std=c++11 $f
done

for f in `find ../WorldEditor -name '*.cpp'`; do
	gcc -fPIC -c -std=c++11 $f
done

for f in `find ../../common -name '*.cpp'`; do
	gcc -fPIC -c -std=c++11 $f
done

# generate lib string
for lib in $LIBS; do
	LIBS_PARAM=$LIBS_PARAM" -l$lib"
done

# compile project
gcc *.o -o main.bin $DEFAULT_LIBS -L. $LIBS_PARAM -lthird_party
rm *.o
