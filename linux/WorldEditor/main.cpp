//
//  main.cpp
//  WorldEditor
//
//  Created by Михайлов Алексей on 22.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#define GLEW_STATIC
#include "../../third_party/gl/glew.h"

#include "../../third_party/sdl/SDL.h"


#include "../../worldeditor/CWorldEditor.h"
#include "../../engine/Core/CFactory.h"
#include "../../engine/Input/CInputManager.h"

#include <stdio.h>

CWorldEditor* app;

int main(int argc, char* argv[])
{
	SDL_Event e;
	
	SDL_Init(SDL_INIT_EVERYTHING);
	
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	
	SDL_Window* window = SDL_CreateWindow("World Editor", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);
	
	SDL_GLContext context = SDL_GL_CreateContext(window);
	
	glewExperimental = GL_TRUE;
	if(glewInit() != GLEW_OK) return -1;
	
	GLenum err = glGetError();
	if(err != 0) printf("glew init error 0x%x\n", err);
	
	if(GLEW_VERSION_3_3) printf("have 3.3\n");
	
	SDL_GL_SetSwapInterval(1);
	
	CInputManager::getInstance()->init(window);
	

	app = new CWorldEditor();
	int error = 0;
	
	while (!error) {
		while(SDL_PollEvent(&e)) app->handleEvent(&e);
		
		error = app->update();
		
		SDL_GL_SwapWindow(window);
		
		SDL_Delay(5);
	}
	
	delete app;
	
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();
	
	return error;
}