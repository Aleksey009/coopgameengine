//
//  main.m
//  WorldEditor
//
//  Created by Михайлов Алексей on 20.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
	return NSApplicationMain(argc, argv);
}
