//
//  AppDelegate.h
//  WorldEditor
//
//  Created by Михайлов Алексей on 20.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
