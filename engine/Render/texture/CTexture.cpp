//
//  CTexture.cpp
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CTexture.h"

#include <string.h>
#include <stdio.h>

CTexture::CTexture(int width, int height, eTextureFormat format, CTexture::sPixel* pixels)
{
	this->format = format;
	this->width = width;
	this->height = height;

	if(pixels) {
		this->pixels = new sPixel[width * height];
		memcpy(this->pixels, pixels, (width * height * sizeof(sPixel)));
	}
	else {
		this->pixels = NULL;
	}
	
	this->pTextureMap = NULL;
	this->uv_x = 0.0f;
	this->uv_y = 0.0f;
	this->uv_width = 1.0f;
	this->uv_height = 1.0f;
}

CTexture::~CTexture()
{
	if(this->pixels) delete [] this->pixels;
}

int CTexture::getWidth()
{
	return this->width;
}

int CTexture::getHeight()
{
	return this->height;
}

CTexture::sPixel* CTexture::getPixels(int* size)
{
	if(this->pixels) {
		*size = this->width * this->height * sizeof(sPixel);
		return this->pixels;
	}
	else {
		*size = 0;
		return NULL;
	}
}

CTexture* CTexture::createTexturePart(int x, int y, int width, int height)
{
	if(!this->pixels) return NULL;
	
	CTexture* result = createTexture(width, height, format, NULL);
	
	result->pTextureMap = this;
	result->uv_x = x * (1.0f / this->width);
	result->uv_y = y * (1.0f / this->height);
	result->uv_width = width * (1.0f / this->width);
	result->uv_height = height * (1.0f / this->height);
	
	return result;
}

void CTexture::getTransformUV(float* x, float* y, float* width, float* height)
{
	*x = this->uv_x;
	*y = this->uv_y;
	*width = this->uv_width;
	*height = this->uv_height;
}

void CTexture::incCounter()
{
	if(pTextureMap) {
		pTextureMap->incCounter();
		return;
	}
	
	CRefCounter::incCounter();
	
	if(!isLoadedIntoGPU()) loadToGPU();
}

bool CTexture::decCounter()
{
	if(pTextureMap) {
		return pTextureMap->decCounter();
	}
	
	if(CRefCounter::decCounter()) return true;
	
	if(isLoadedIntoGPU()) unloadFromGPU();
	return false;
}

