//
//  CTextureManager.h
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CTextureManager__
#define __Render__CTextureManager__

#include "../../common/defines.h"

#include "CTexture.h"

#include <iostream>

/** Отвечает за загрузку текстур с диска в оперативную память */
class RENDER_API CTextureManager
{
public:
	CTextureManager();
	virtual ~CTextureManager();
	
	/**
	 Загружает текстуру с диска
	 @param ios поток ввода
	 @return объект текстура
	 */
	virtual CTexture* loadTexture(std::istream& ios);
	
	/**
	 Создает объект текстуры требуемого класса (от OpenGL или другого рендера)
	 @return объект текстуры
	 */
	virtual CTexture* createTexture(int width, int height, eTextureFormat format, CTexture::sPixel* pixels) {return NULL;}
};

#endif /* defined(__Render__CTextureManager__) */
