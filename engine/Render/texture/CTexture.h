//
//  CTexture.h
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CTexture__
#define __Render__CTexture__

#include "../../common/CRefCounter.h"
#include "../../common/defines.h"

enum eTextureFormat {
	TEXTURE_FORMAT_RGBA,
	TEXTURE_FORMAT_DEPTH,
	TEXTURE_FORMAT_STENCIL
};

/** Базовый класс текстуры (без работы с видеопамятью) */
class RENDER_API CTexture: public CRefCounter
{
public:
	/** Структура одного пикселя */
	struct sPixel {
		/** Красный цвет (0-255) */
		unsigned char r;
		/** Зеленый цвет (0-255) */
		unsigned char g;
		/** Синий цвет (0-255) */
		unsigned char b;
		/** Альфа канал (0-255) */
		unsigned char a;
	};
	
	CTexture(int width, int height, eTextureFormat format, sPixel* pixels);
	virtual ~CTexture();
	
	/**
	 Получает ширину текстуры
	 @return ширина
	 */
	int getWidth();
	/**
	 Получает высоту текстуры
	 @return высота
	 */
	int getHeight();
	/**
	 Получает указатель на массив пикселей и размер массива
	 @param size выходной параметр, передастся размер массива пикселей
	 @return указатель на массив пикселей
	 */
	sPixel* getPixels(int* size);

	/**
	 Создает подчиненную текстуру, являющейся частью родительской текстурной карты (не имеет собственного набора пикселей)
	 @param x начальная координата x на родительской текстуре
	 @param y начальная координата y на родительской текстуре
	 @param width ширина на родительской текстуре
	 @param height высота на родительской текстуре
	 @return подчиненная текстура - часть
	 */
	virtual CTexture* createTexturePart(int x, int y, int width, int height);
	/**
	 Получает коэффициенты трансформации UV координат, для текстурных карт
	 @param x начало по оси x текстуры на текстурной карте
	 @param y начало по оси y текстуры на текстурной карте
	 @param width ширина текстуры на текстурной карте
	 @param height высота текстуры на текстурной карте
	 */
	virtual void getTransformUV(float* x, float* y, float* width, float* height);
	
	/**
	 Загружает текстуру в видеопамять
	 */
	virtual void loadToGPU() {}
	/**
	 Выгружает текстуру из видеопамяти
	 */
	virtual void unloadFromGPU() {}
	/**
	 Проверяет загружена ли текстура в видеопамять
	 @return true - текстура в видеопамяти
	 */
	virtual bool isLoadedIntoGPU() {return false;}
	
	/**
	 Закрепляет текстуру для использования
	 */
	virtual void bind() {}
	/**
	 Отменяет использование текстуры
	 */
	virtual void unbind() {}
	
	virtual void incCounter();
	virtual bool decCounter();
	
protected:
	/** 
	 Создание класса текстуры (создается наследник - от opengl / dx)
	 @param width ширина
	 @param height высота
	 @param pixels данные о пикселях (массив размером width*height)
	 @return указатель на объект текстуры
	*/
	virtual CTexture* createTexture(int width, int height, eTextureFormat format, sPixel* pixels) {return NULL;}
	
	/** Формат */
	eTextureFormat format;
	/** Ширина */
	int width;
	/** Высота */
	int height;
	/** Данные о пикселях (массив размером width*height) */
	sPixel* pixels;
	/** Родительская текстура (если это часть одной большой текстуры, то она не имеет данных о пикселях, но имеет родителя) */
	CTexture* pTextureMap;
	/** Начало текстурных координат по х (для родителя - 0) */
	float uv_x;
	/** Начало текстурных координат по y (для родителя - 0) */
	float uv_y;
	/** Ширина текстурных координат (для родителя - 1) */
	float uv_width;
	/** Высота текстурных координат (для родителя - 1) */
	float uv_height;
};

#endif /* defined(__Render__CTexture__) */
