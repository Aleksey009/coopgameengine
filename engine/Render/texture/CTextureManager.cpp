//
//  CTextureManager.cpp
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CTextureManager.h"
#include "CTexture.h"

#include "../../../third_party/freeimage/FreeImage.h"

static unsigned int DLL_CALLCONV global_readProc( void* buffer, unsigned int size, unsigned int count, fi_handle handle )
{
    std::istream* fin = (std::istream*)handle;
    if ( fin )
    {
		unsigned int readed = 0;
		char* local = (char*)buffer;
		for(unsigned int i = 0;i < count;i++) {
			fin->read(local, size);
			local += size;
			readed += size;
		}
		
        return readed;
    }
    return 0;
}

static int DLL_CALLCONV global_seekProc( fi_handle handle, long offset, int origin )
{
    std::istream* fin = (std::istream*)handle;
    if (fin)
    {
		switch (origin) {
			case SEEK_SET:
				fin->seekg(offset, std::ios::beg);
			case SEEK_CUR:
				fin->seekg(offset, std::ios::cur);
			case SEEK_END:
				fin->seekg(offset, std::ios::end);
		}
    }
    return 0;
}

static long DLL_CALLCONV global_tellProc( fi_handle handle )
{
    std::istream* fin = (std::istream*)handle;

	if(fin) return (long)fin->tellg();
	else return 0;
}



CTextureManager::CTextureManager()
{
	FreeImage_Initialise();
}

CTextureManager::~CTextureManager()
{
	FreeImage_DeInitialise();
}

CTexture* CTextureManager::loadTexture(std::istream& ios)
{
	FreeImageIO io;
	
	io.read_proc = global_readProc;
	io.seek_proc = global_seekProc;
	io.tell_proc = global_tellProc;
	io.write_proc = NULL;
	
	FIBITMAP *bitmap = FreeImage_LoadFromHandle(FIF_PNG, &io, &ios);
	
	if(!bitmap) return NULL;
	
	if(FreeImage_GetBPP(bitmap) != 32) bitmap = FreeImage_ConvertTo32Bits(bitmap);
	
	int width = FreeImage_GetWidth(bitmap);
	int height = FreeImage_GetHeight(bitmap);
	BYTE* bytes = FreeImage_GetBits(bitmap);
	
	CTexture::sPixel* pixels = new CTexture::sPixel[width*height];
	
	for(int i = 0;i < height;i++) {
		for(int j = 0;j < width;j++) {
			int ppos = (height - i - 1)*height + j;
			int bpos = i*height + j;
			
			pixels[ppos].r = bytes[bpos * 4 + 2];
			pixels[ppos].g = bytes[bpos * 4 + 1];
			pixels[ppos].b = bytes[bpos * 4 + 0];
			pixels[ppos].a = bytes[bpos * 4 + 3];
		}
	}
		
	CTexture* result = createTexture(width, height, TEXTURE_FORMAT_RGBA, pixels);
	
	delete [] pixels;
				
	return result;
}
