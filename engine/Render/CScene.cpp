//
//  CScene.cpp
//  Render
//
//  Created by Михайлов Алексей on 12.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CScene.h"
#include "IRenderable.h"
#include "CRenderAbstract.h"

CScene::CScene()
{
	
}

CScene::~CScene()
{
	
}

void CScene::addObject(IRenderable* object)
{
	objects.add(object);
}

void CScene::removeObject(IRenderable* object)
{
	objects.remove(object);
}

void CScene::draw(CRenderAbstract* render)
{
	/** @todo сделать определение какие объекты НУЖНЫ для кадра */
	CLinkedList<IRenderable*>::iterator i = objects.begin();
	while(i != NULL) {
		render->draw(i->pData);
		i = i->pNext;
	}
}
