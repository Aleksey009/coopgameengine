//
//  CRenderPhaseAbstract.h
//  Render
//
//  Created by Михайлов Алексей on 11.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef Render_CRenderPhaseAbstract_h
#define Render_CRenderPhaseAbstract_h

#include "../common/defines.h"

/** Базовый класс фазы отрисовки */
class RENDER_API CRenderPhaseAbstract
{
public:
	/**
	 Событие начала фазы отрисовки
	 */
	virtual void onStartDrawing() = 0;
	/**
	 Событие выполнения фазы отрисовки
	 */
	virtual void onDrawing() = 0;
	/**
	 Событие завершения фазы отрисовки
	 */
	virtual void onEndDrawing() = 0;
};

#endif
