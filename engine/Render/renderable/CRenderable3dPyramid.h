//
//  CRenderable3dPyramid.h
//  Render
//
//  Created by Михайлов Алексей on 09.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderable3dPyramid__
#define __Render__CRenderable3dPyramid__

#include "CRenderable3d.h"

/** Класс отрисовки пирамиды */
class RENDER_API CRenderable3dPyramid: public CRenderable3d
{
public:
	CRenderable3dPyramid();
	virtual ~CRenderable3dPyramid();
	
	virtual unsigned int getVertexCount();
	
	virtual unsigned long getVertexBufferSize(unsigned char bufferid);
	
	virtual eDrawMode getDrawMode();
	
};

#endif /* defined(__Render__CRenderable3dPyramid__) */
