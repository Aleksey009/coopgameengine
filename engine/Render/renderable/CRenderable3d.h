//
//  CRenderable3d.h
//  Render
//
//  Created by Михайлов Алексей on 08.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderable3d__
#define __Render__CRenderable3d__

#include "../IRenderable.h"

class CShaderProgram;

enum eRenderable3dUniforms
{
	RENDERABLE_3D_UNIFORM_VIEW_PROJECTION = 0,
	RENDERABLE_3D_UNIFORM_MODEL,
	RENDERABLE_3D_UNIFORM_USE_TEXTURE,
	RENDERABLE_3D_UNIFORM_COUNT
};

enum eRenderable3dBuffers
{
	RENDERABLE_3D_BUFFER_POSITION = 0,
	RENDERABLE_3D_BUFFER_UV,
	RENDERABLE_3D_BUFFER_COLOR,
	RENDERABLE_3D_BUFFER_NORMAL,
	RENDERABLE_3D_BUFFER_COUNT
	
};

/** Базовый класс 3д объектов для отрисовки */
class RENDER_API CRenderable3d: public IRenderable
{
public:
	CRenderable3d();
	virtual ~CRenderable3d();
	
	/**
	 Устанавливает шейдерную программу, используемую для отрисовки 3д объектов
	 @param shaderProgram шейдерная программа
	 */
	static void setShaderProgram(CShaderProgram* shaderProgram);
	
	/**
	 Изменяет положение объекта в 3д мире
	 @param x координата x
	 @param y координата y
	 @param z координата z
	 @return сам себя
	 */
	virtual CRenderable3d* setPosition(float x, float y, float z);
	/**
	 Получает положение объекта в 3д мире
	 @param x координата x
	 @param y координата y
	 @param z координата z
	 */
	virtual void getPosition(float* x, float* y, float* z);
	
	/**
	 Изменяет масштаб объекта в 3д мире
	 @param scale масштаб по всем осям (1.0 - базовый)
	 @return сам себя
	 */
	virtual CRenderable3d* setScale(float scale);
	/**
	 Изменяет масштаб объекта в 3д мире
	 @param sx масштаб по оси x (1.0 - базовый)
	 @param sy масштаб по оси y (1.0 - базовый)
	 @param sz масштаб по оси z (1.0 - базовый)
	 @return сам себя
	 */
	virtual CRenderable3d* setScale(float sx, float sy, float sz);
	/**
	 Получает масштаб объекта в 3д мире
	 @param sx масштаб по оси x
	 @param sy масштаб по оси y
	 @param sz масштаб по оси z
	 */
	virtual void getScale(float* sx, float* sy, float* sz);
	
	/**
	 Изменяет поворот объекта в 3д мире
	 @param rx поворот по оси x
	 @param ry поворот по оси y
	 @param rz поворот по оси z
	 @return сам себя
	 */
	virtual CRenderable3d* setRotation(float rx, float ry, float rz);
	/**
	 Получает поворот объекта в 3д мире
	 @param rx поворот по оси x
	 @param ry поворот по оси y
	 @param rz поворот по оси z
	 */
	virtual void getRotation(float* rx, float* ry, float* rz);
	
	virtual void onPreDraw(CRenderAbstract* render);
	virtual void onPostDraw(CRenderAbstract* render);
	virtual unsigned char	getUniformsCount();
	virtual const char*		getUniformName(unsigned char uniformid);
	virtual eUniformType	getUniformType(unsigned char uniformid);
	virtual void*			getUniformData(unsigned char uniformid);
	virtual unsigned char	getVertexBuffersCount();
	virtual const char*		getVertexBufferName(unsigned char bufferid);
	virtual void*			getVertexBufferData(unsigned char bufferid);
	virtual unsigned char	getVertexBufferDimensions(unsigned char bufferid);
	virtual void*			getElementsBufferData();
	
protected:
	static CShaderProgram* pShaderProgram;
	
	float* pUniformModel;
	
	float* pVertexBufferPos;
	float* pVertexBufferUV;
	float* pVertexBufferColor;
	float* pVertexBufferNormal;
	
	unsigned int* pElementsBuffer;
};

#endif /* defined(__Render__CRenderable3d__) */
