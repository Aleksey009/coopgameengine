//
//  CRenderableFrame.cpp
//  Render
//
//  Created by Михайлов Алексей on 22.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderableFrame.h"

#define vertex_count 4

float frame_pos[vertex_count*2] = {
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,
};
float frame_uv[vertex_count*2] = {
	0.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
};
unsigned int frame_elements[6] = {
	0, 1, 2,
	2, 3, 0
};

CRenderableFrame::CRenderableFrame()
{

}

CRenderableFrame::~CRenderableFrame()
{
	
}

void CRenderableFrame::pushTexture(const char* textureName, CTexture* texture)
{
	sTexturesHolder holder;
	strcpy(holder.name, textureName);
	holder.pTexture = texture;
	
	textures.push_back(holder);
}

void CRenderableFrame::clearTextures()
{
	textures.clear();
}

unsigned char CRenderableFrame::getTexturesCount()
{
	return textures.size();
}

const char* CRenderableFrame::getTextureName(unsigned char textureid)
{
	return textures[textureid].name;
}

CTexture* CRenderableFrame::getTexture(unsigned char textureid)
{
	return textures[textureid].pTexture;
}

unsigned char CRenderableFrame::getVertexBuffersCount()
{
	return 2;
}

const char*	CRenderableFrame::getVertexBufferName(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_FRAME_BUFFER_POSITION) return "inVertexPos";
	if(bufferid == RENDERABLE_FRAME_BUFFER_UV) return "inVertexUV";
	return NULL;
}

unsigned long CRenderableFrame::getVertexBufferSize(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_FRAME_BUFFER_POSITION) return vertex_count*sizeof(float)*2;
	if(bufferid == RENDERABLE_FRAME_BUFFER_UV) return vertex_count*sizeof(float)*2;
	return 0;
}

void* CRenderableFrame::getVertexBufferData(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_FRAME_BUFFER_POSITION) return &(frame_pos[0]);
	if(bufferid == RENDERABLE_FRAME_BUFFER_UV) return &(frame_uv[0]);
	return NULL;
}

unsigned char CRenderableFrame::getVertexBufferDimensions(unsigned char bufferid)
{
	return 2;
}

unsigned int CRenderableFrame::getVertexCount()
{
	return 6;
}

unsigned long CRenderableFrame::getElementsBufferSize()
{
	return 6*sizeof(unsigned int);
}

void* CRenderableFrame::getElementsBufferData()
{
	return &(frame_elements[0]);
}

eDrawMode CRenderableFrame::getDrawMode()
{
	return DRAW_TRIANGLES;
}
