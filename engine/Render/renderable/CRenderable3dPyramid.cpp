//
//  CRenderable3dPyramid.cpp
//  Render
//
//  Created by Михайлов Алексей on 09.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderable3dPyramid.h"

const unsigned int vertex_count = 12;

float pyramid_pos[vertex_count * 3] = {
	-0.5, 0, 0.5,
	0, 0, -0.5,
	0.5, 0, 0.5,
	
	0, 0, -0.5,
	0.5, 0, 0.5,
	0, 1, 0,
	
	-0.5, 0, 0.5,
	0, 0, -0.5,
	0, 1, 0,
	
	0.5, 0, 0.5,
	-0.5, 0, 0.5,
	0, 1, 0
};
float pyramid_normals[vertex_count * 3] =
{
	0, -1, 0,
	0, -1, 0,
	0, -1, 0,
	
	1, 0.25, -0.5,
	1, 0.25, -0.5,
	1, 0.25, -0.5,
	
	-1, 0.25, -0.5,
	-1, 0.25, -0.5,
	-1, 0.25, -0.5,
	
	0, 0.5, 1,
	0, 0.5, 1,
	0, 0.5, 1
};
float pyramid_colors[vertex_count * 4] =
{
	1.0, 1.0, 1.0, 1.0,
	1.0, 1.0, 1.0, 1.0,
	1.0, 1.0, 1.0, 1.0,
	
	1.0, 0.0, 0.0, 1.0,
	1.0, 0.0, 0.0, 1.0,
	1.0, 0.0, 0.0, 1.0,
	
	0.0, 1.0, 0.0, 1.0,
	0.0, 1.0, 0.0, 1.0,
	0.0, 1.0, 0.0, 1.0,
	
	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 1.0, 1.0,
};

CRenderable3dPyramid::CRenderable3dPyramid(): CRenderable3d()
{
	pVertexBufferPos = &(pyramid_pos[0]);
	pVertexBufferUV = new float[vertex_count * 2];
	pVertexBufferColor = &(pyramid_colors[0]);
	pVertexBufferNormal = &(pyramid_normals[0]);
}

CRenderable3dPyramid::~CRenderable3dPyramid()
{
	
}

unsigned int CRenderable3dPyramid::getVertexCount()
{
	return vertex_count;
}

unsigned long CRenderable3dPyramid::getVertexBufferSize(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_3D_BUFFER_POSITION) return vertex_count*sizeof(float)*3;
	if(bufferid == RENDERABLE_3D_BUFFER_UV) return vertex_count*sizeof(float)*2;
	if(bufferid == RENDERABLE_3D_BUFFER_COLOR) return vertex_count*sizeof(float)*4;
	if(bufferid == RENDERABLE_3D_BUFFER_NORMAL) return vertex_count*sizeof(float) * 3;

	return 0;
}

eDrawMode CRenderable3dPyramid::getDrawMode()
{
	return DRAW_TRIANGLES;
}