//
//  CRenderable2d.cpp
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderable2d.h"

#include "../CRenderAbstract.h"

#include "../shaders/CShadersManager.h"
#include "../shaders/CShaderProgram.h"
#include "../texture/CTexture.h"

#include <string.h>
#include <fstream>

CShaderProgram* CRenderable2d::pShaderProgram = NULL;

CRenderable2d::CRenderable2d()
{
	pTexture = NULL;
	pUniformUseTexture = 0;
	
	pUniformCoordSystem = NULL;
	pUniformCoordSystemByViewportBuffer = NULL;
	pUniformTranform = glm::mat3(1.0f);
	
	pVertexBufferPos = NULL;
	pVertexBufferUV = NULL;
	pVertexBufferColor = NULL;
	
	pElementsBuffer = NULL;
}

CRenderable2d::~CRenderable2d()
{
	if(pTexture) pTexture->decCounter();
	
	if(pUniformCoordSystem) delete pUniformCoordSystem;
	if(pUniformCoordSystemByViewportBuffer) delete pUniformCoordSystemByViewportBuffer;
	
	if(pVertexBufferPos) delete pVertexBufferPos;
	if(pVertexBufferUV) delete pVertexBufferUV;
	if(pVertexBufferColor) delete pVertexBufferColor;

	if(pElementsBuffer) delete pElementsBuffer;
}

void CRenderable2d::setShaderProgram(CShaderProgram* shaderProgram)
{
	pShaderProgram = shaderProgram;
}

unsigned char CRenderable2d::getTexturesCount()
{
	if(pUniformUseTexture) return 1;
	else return 0;
}

const char* CRenderable2d::getTextureName(unsigned char textureid)
{
	return "uTexture2d";
}

CTexture* CRenderable2d::getTexture(unsigned char textureid)
{
	return pTexture;
}

unsigned char CRenderable2d::getUniformsCount()
{
	return RENDERABLE_2D_UNIFORM_COUNT;
}

const char* CRenderable2d::getUniformName(unsigned char uniformid)
{
	if(uniformid == RENDERABLE_2D_UNIFORM_COORD_SYSTEM) return "uCoordSystem";
	if(uniformid == RENDERABLE_2D_UNIFORM_TRANSFORM) return "uTransform";
	if(uniformid == RENDERABLE_2D_UNIFORM_USE_TEXTURE) return "uUseTexture";
	
	return 0;
}

eUniformType CRenderable2d::getUniformType(unsigned char uniformid)
{
	if(uniformid == RENDERABLE_2D_UNIFORM_USE_TEXTURE) return UNIFORM_TYPE_INT_1;
	else return UNIFORM_TYPE_MATRIX_3;
}

void* CRenderable2d::getUniformData(unsigned char uniformid)
{
	if(uniformid == RENDERABLE_2D_UNIFORM_COORD_SYSTEM) {
		if(pUniformCoordSystem) return &((*pUniformCoordSystem)[0][0]);
		else {
			if(!pUniformCoordSystemByViewportBuffer) pUniformCoordSystemByViewportBuffer = new float[3*3];
			
			CRenderAbstract::getInstance()->getViewportMatrix2D(pUniformCoordSystemByViewportBuffer);
			
			return pUniformCoordSystemByViewportBuffer;
		}
	}
	if(uniformid == RENDERABLE_2D_UNIFORM_TRANSFORM) return &(pUniformTranform[0][0]);
	if(uniformid == RENDERABLE_2D_UNIFORM_USE_TEXTURE) return &pUniformUseTexture;
	
	return NULL;
}

unsigned char CRenderable2d::getVertexBuffersCount()
{
	return RENDERABLE_2D_BUFFER_COUNT;
}

const char* CRenderable2d::getVertexBufferName(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_2D_BUFFER_POSITION) return "inVertexPos";
	if(bufferid == RENDERABLE_2D_BUFFER_UV) return "inVertexUV";
	if(bufferid == RENDERABLE_2D_BUFFER_COLOR) return "inVertexColor";
	
	return NULL;
}

unsigned char CRenderable2d::getVertexBufferDimensions(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_2D_BUFFER_POSITION) return 2;
	if(bufferid == RENDERABLE_2D_BUFFER_UV) return 2;
	if(bufferid == RENDERABLE_2D_BUFFER_COLOR) return 4;
	
	return 0;
}

void* CRenderable2d::getVertexBufferData(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_2D_BUFFER_POSITION) return pVertexBufferPos;
	if(bufferid == RENDERABLE_2D_BUFFER_UV) return pVertexBufferUV;
	if(bufferid == RENDERABLE_2D_BUFFER_COLOR) return pVertexBufferColor;
	
	return NULL;
}

void* CRenderable2d::getElementsBufferData()
{
	return pElementsBuffer;
}

void CRenderable2d::onPreDraw(CRenderAbstract* render)
{
	if(pShaderProgram) render->pushShaderProgram(pShaderProgram);
}

void CRenderable2d::onPostDraw(CRenderAbstract* render)
{
	if(pShaderProgram) render->popShaderProgram();
}
