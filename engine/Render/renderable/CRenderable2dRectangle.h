//
//  CRenderable2dRectangle.h
//  Render
//
//  Created by Михайлов Алексей on 26.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderable2dRectangle__
#define __Render__CRenderable2dRectangle__

#include "CRenderable2d.h"

#include "../../../third_party/glm/glm.hpp"

class RENDER_API CRenderable2dRectangle: public CRenderable2d
{
public:
	CRenderable2dRectangle();
	
	virtual CRenderable2dRectangle* setRectangle(float x, float y, float width, float height);
	virtual void getRectangle(float* x, float* y, float* width, float* height);
	
	virtual void setTransform(glm::mat3 transform);
	virtual glm::mat3 getTransform();
	
	virtual CRenderable2dRectangle* setVertexColor(unsigned long vertexid, float r, float g, float b, float a);
	virtual void getVertexColor(unsigned long vertexid, float* r, float* g, float* b, float* a);
	
	virtual CRenderable2dRectangle* setAllVertexColor(float r, float g, float b, float a);
	
	virtual CRenderable2dRectangle* setTexture(CTexture* texture);
	
	virtual unsigned int getVertexCount();
	
	virtual unsigned long getVertexBufferSize(unsigned char bufferid);
	virtual unsigned long getElementsBufferSize();
	
	virtual eDrawMode getDrawMode();
};

#endif /* defined(__Render__CRenderable2dRectangle__) */
