//
//  CRenderable3dCube.h
//  Render
//
//  Created by Кузьмин Владимир on 11.09.14.
//  Copyright (c) aunshilord@yahoo.com. All rights reserved.
//


#ifndef __Render__CRenderable3dCube__
#define __Render__CRenderable3dCube__

#include "CRenderable3d.h"

/** Класс отрисовки куба */
class RENDER_API CRenderable3dCube : public CRenderable3d
{
public:
	CRenderable3dCube();
	virtual ~CRenderable3dCube();

	virtual unsigned int getVertexCount();

	virtual unsigned long getVertexBufferSize(unsigned char bufferid);
	virtual unsigned long getElementsBufferSize();

	virtual eDrawMode getDrawMode();

	/*
	@todo
	getTexturesCount() - 1 текстура (для всех граней будет 1 текстура)
	getTextureName(unsigned char textureid) - имя текстурной переменной в шейдере
	getTexture(unsigned char textureid) - сама текстура
	getUniformData(unsigned char uniformid) - массивы с данными униформ

	getVertexBufferData(unsigned char bufferid) - массивы с данными

	getElementsBufferData() - данные буфера индексов
	*/

};

#endif /* defined(__Render__CRenderable3dCube__) */