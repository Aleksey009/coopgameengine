//
//  CRenderable3d.cpp
//  Render
//
//  Created by Михайлов Алексей on 08.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderable3d.h"

#include "../CRenderAbstract.h"

#include "../shaders/CShadersManager.h"
#include "../shaders/CShaderProgram.h"

#include "../../common/Utils.h"

#include <string.h>
#include <fstream>

int not_use_texture = 0;

CShaderProgram* CRenderable3d::pShaderProgram = NULL;

CRenderable3d::CRenderable3d()
{
	pUniformModel = new float[4*4];
	memset(pUniformModel, 0, sizeof(float)*(4*4));
	pUniformModel[INDEX4(0, 0)] = 1.0f;
	pUniformModel[INDEX4(1, 1)] = 1.0f;
	pUniformModel[INDEX4(2, 2)] = 1.0f;
	pUniformModel[INDEX4(3, 3)] = 1.0f;
	
	pVertexBufferPos = NULL;
	pVertexBufferUV = NULL;
	pVertexBufferColor = NULL;
	pVertexBufferNormal = NULL;
	
	pElementsBuffer = NULL;
}

CRenderable3d::~CRenderable3d()
{
	if(pUniformModel) delete pUniformModel;
	
	if(pVertexBufferPos) delete pVertexBufferPos;
	if(pVertexBufferUV) delete pVertexBufferUV;
	if(pVertexBufferColor) delete pVertexBufferColor;
	if (pVertexBufferNormal) delete pVertexBufferNormal;
	
	if(pElementsBuffer) delete pElementsBuffer;
}

void CRenderable3d::setShaderProgram(CShaderProgram* shaderProgram)
{
	pShaderProgram = shaderProgram;
}

CRenderable3d* CRenderable3d::setPosition(float x, float y, float z)
{
	if(pUniformModel) {
		pUniformModel[INDEX4(0, 3)] = x;
		pUniformModel[INDEX4(1, 3)] = y;
		pUniformModel[INDEX4(2, 3)] = z;
	}
	
	return this;
}

void CRenderable3d::getPosition(float* x, float* y, float* z)
{
	if(!pUniformModel) return;
	
	*x = pUniformModel[INDEX4(0, 3)];
	*y = pUniformModel[INDEX4(1, 3)];
	*z = pUniformModel[INDEX4(2, 3)];
}

CRenderable3d* CRenderable3d::setScale(float scale)
{
	return setScale(scale, scale, scale);
}

CRenderable3d* CRenderable3d::setScale(float sx, float sy, float sz)
{
	if(pUniformModel) {
		pUniformModel[INDEX4(0, 0)] = sx;
		pUniformModel[INDEX4(1, 1)] = sy;
		pUniformModel[INDEX4(2, 2)] = sz;
	}
	
	return this;
}

void CRenderable3d::getScale(float* sx, float* sy, float* sz)
{
	if(!pUniformModel) return;
	
	*sx = pUniformModel[INDEX4(0, 0)];
	*sy = pUniformModel[INDEX4(1, 1)];
	*sz = pUniformModel[INDEX4(2, 2)];
}

CRenderable3d* CRenderable3d::setRotation(float rx, float ry, float rz)
{
	/** @todo придумать как работать с ротацией и всеми остальными параметрами (мб хранить отдельно, а матрицу собирать при изменениях?) */
	return this;
}

void CRenderable3d::getRotation(float* rx, float* ry, float* rz)
{
	
}

unsigned char CRenderable3d::getUniformsCount()
{
	return RENDERABLE_3D_UNIFORM_COUNT;
}

const char* CRenderable3d::getUniformName(unsigned char uniformid)
{
	if(uniformid == RENDERABLE_3D_UNIFORM_VIEW_PROJECTION) return "uViewProjection";
	if(uniformid == RENDERABLE_3D_UNIFORM_MODEL) return "uModel";
	if(uniformid == RENDERABLE_3D_UNIFORM_USE_TEXTURE) return "uUseTexture";
	
	return 0;
}

eUniformType CRenderable3d::getUniformType(unsigned char uniformid)
{
	if(uniformid == RENDERABLE_3D_UNIFORM_USE_TEXTURE) return UNIFORM_TYPE_INT_1;
	return UNIFORM_TYPE_MATRIX_4;
}

void* CRenderable3d::getUniformData(unsigned char uniformid)
{
	if(uniformid == RENDERABLE_3D_UNIFORM_VIEW_PROJECTION) {
		CRenderAbstract* render = CRenderAbstract::getInstance();
		
		return render->getViewProjectionMatrix();
	}
	if(uniformid == RENDERABLE_3D_UNIFORM_MODEL) return pUniformModel;
	if(uniformid == RENDERABLE_3D_UNIFORM_USE_TEXTURE) return &not_use_texture;
	
	return NULL;
}

unsigned char CRenderable3d::getVertexBuffersCount()
{
	return RENDERABLE_3D_BUFFER_COUNT;
}

const char* CRenderable3d::getVertexBufferName(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_3D_BUFFER_POSITION) return "inVertexPos";
	if(bufferid == RENDERABLE_3D_BUFFER_UV) return "inVertexUV";
	if(bufferid == RENDERABLE_3D_BUFFER_COLOR) return "inVertexColor";
	if(bufferid == RENDERABLE_3D_BUFFER_NORMAL) return "inVertexNormal";
	
	return NULL;
}

unsigned char CRenderable3d::getVertexBufferDimensions(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_3D_BUFFER_POSITION) return 3;
	if(bufferid == RENDERABLE_3D_BUFFER_UV) return 2;
	if(bufferid == RENDERABLE_3D_BUFFER_COLOR) return 4;
	if(bufferid == RENDERABLE_3D_BUFFER_NORMAL) return 3;
	
	return 0;
}

void* CRenderable3d::getVertexBufferData(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_3D_BUFFER_POSITION) return pVertexBufferPos;
	if(bufferid == RENDERABLE_3D_BUFFER_UV) return pVertexBufferUV;
	if(bufferid == RENDERABLE_3D_BUFFER_COLOR) return pVertexBufferColor;
	if(bufferid == RENDERABLE_3D_BUFFER_NORMAL) return pVertexBufferNormal;
	
	return NULL;
}

void* CRenderable3d::getElementsBufferData()
{
	return pElementsBuffer;
}

void CRenderable3d::onPreDraw(CRenderAbstract* render)
{
	if(pShaderProgram) render->pushShaderProgram(pShaderProgram);
}

void CRenderable3d::onPostDraw(CRenderAbstract* render)
{
	if(pShaderProgram) render->popShaderProgram();
}
