//
//  CRenderable2dText.cpp
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderable2dText.h"
#include "CRenderable2d.h"

#include "../shaders/CShadersManager.h"
#include "../shaders/CShaderProgram.h"

#include "../font/CFont.h"
#include "../font/CFontManager.h"

#include "../CRenderAbstract.h"

#include "../../../third_party/freetype-gl/freetype-gl.h"
#include "../../../third_party/freetype-gl/vertex-buffer.h"
#include "../../../third_party/freetype-gl/markup.h"
#include "../../../third_party/glm/glm.hpp"

#include <fstream>

typedef struct {
    float x, y;
    float s, t;
    float r, g, b, a;
} vertex_t;

CShaderProgram* CRenderable2dText::pShaderProgram = NULL;

CRenderable2dText::CRenderable2dText(): CRenderable2dText(NULL)
{
	prefferedLineWidth = 9999;
	maxLines = 1;
}

CRenderable2dText::CRenderable2dText(CFont* font)
{
	if(font) this->pFont = font;
	else {
		CFontManager* fm = CRenderAbstract::getInstance()->fontManager();
		
		this->pFont = fm->build("Times New Roman.ttf", 20.0f)->setForegroundColor({0.0f, 0.0f, 0.0f, 1.0f});
	}
	
	this->pBuffer = vertex_buffer_new("vertex:2f,tex_coord:2f,color:4f");
	
	this->prefferedLineWidth = 1000.0f;
	this->maxLines = 0;
	
	pUniformTranform = new float[3*3];
	memcpy(pUniformTranform, &(glm::mat4(1.0f)[0][0]), sizeof(float)*(3*3));
}

CRenderable2dText::~CRenderable2dText()
{
	delete this->pFont;
	delete this->pBuffer;
}

void CRenderable2dText::setShaderProgram(CShaderProgram* shaderProgram)
{
	pShaderProgram = shaderProgram;
}

vertex_buffer_t* CRenderable2dText::getVertexBuffer()
{
	return this->pBuffer;
}

void CRenderable2dText::setFont(CFont *font)
{
	this->pFont = font;
	
	rebuildBuffer();
}

CFont* CRenderable2dText::getFont()
{
	return this->pFont;
}

void CRenderable2dText::setText(std::wstring text)
{
	this->text = text;
	
	rebuildBuffer();
}

void CRenderable2dText::setText(const wchar_t* text)
{
	setText(std::wstring(text));
}

void CRenderable2dText::setText(const char* text)
{
	size_t len = strlen(text);
	wchar_t* wtext = new wchar_t[len];
	mbstowcs(wtext, text, len);
	
	setText(std::wstring(wtext));
}

std::wstring CRenderable2dText::getText()
{
	return this->text;
}

void CRenderable2dText::setPrefferedLineWidth(float width)
{
	this->prefferedLineWidth = width;
	
	rebuildBuffer();
}

float CRenderable2dText::getPrefferedLineWidth()
{
	return this->prefferedLineWidth;
}

sSize<float> CRenderable2dText::getTextSize()
{
	return this->textSize;
}

void CRenderable2dText::setMaxLines(int lines)
{
	this->maxLines = lines;
	
	rebuildBuffer();
}

int CRenderable2dText::getMaxLines()
{
	return this->maxLines;
}

void CRenderable2dText::setPosition(float x, float y)
{
	this->pUniformTranform[3*2 + 0] = x;
	this->pUniformTranform[3*2 + 1] = y;
}

void CRenderable2dText::getPosition(float* x, float* y)
{
	*x = this->pUniformTranform[3*2 + 0];
	*y = this->pUniformTranform[3*2 + 1];
}

void CRenderable2dText::setTransform(float* transform)
{
	memcpy(pUniformTranform, transform, sizeof(float)*(3*3));
}

float* CRenderable2dText::getTransform()
{
	return this->pUniformTranform;
}

void CRenderable2dText::rebuildBuffer()
{
	vec2 pen;
	markup_t* markup = this->pFont->getMarkup();
	int line = 1;
	
	pen.x = 0;
    pen.y = 0;
	
    vertex_buffer_clear(this->pBuffer);
	textSize.width = 0.0f;
	textSize.height = 0.0f;
	
	const wchar_t* str = text.c_str();

	size_t len = wcslen(str);
	if(len == 0) return;
	
	wchar_t prev = L'\0';
	pen.y = -markup->font->height;
	
	for(size_t i = 0;i < len;i++) {
		wchar_t current = str[i];
		float prev_kerning = 0.0f;
		
		if (current == L'\n') {
			line++;
			pen.x = 0.0f;
			pen.y -= markup->font->height;
			prev_kerning = 0.0f;
			continue;
		}

		texture_glyph_t *glyph  = texture_font_get_glyph(markup->font, current);
		if(prev != L'\0') prev_kerning = texture_glyph_get_kerning(glyph, prev);
		
		float glyph_offset_x = (float)glyph->offset_x;
		float glyph_offset_y = (float)glyph->offset_y;
		float glyph_width = (float)glyph->width;
		float glyph_height = (float)glyph->height;
		
		if(glyph_offset_x == 0) glyph_offset_x = 1.0f;
		if(glyph_offset_y == 0) glyph_offset_y = markup->font->descender;
		if(glyph_width == 0) glyph_width = 1.0f;
		if(glyph_height == 0) glyph_height = - (markup->font->height - markup->font->linegap);
		
		// check limits
		if((pen.x + prev_kerning + glyph_offset_x + glyph_width) > prefferedLineWidth) {
			line++;
			pen.x = 0.0f;
			pen.y -= markup->font->height;
			prev_kerning = 0.0f;
		}
		if((maxLines != 0) && (line > maxLines)) break;
		
		pen.x += prev_kerning;
		
		float r = markup->foreground_color.r;
		float g = markup->foreground_color.g;
		float b = markup->foreground_color.b;
		float a = markup->foreground_color.a;
		
		float x0  = pen.x + glyph_offset_x;
		float y0  = pen.y + glyph_offset_y;
		float x1  = x0 + glyph_width;
		float y1  = y0 - glyph_height;
				
		float s0 = glyph->s0;
		float t0 = glyph->t0;
		float s1 = glyph->s1;
		float t1 = glyph->t1;
		
		GLuint indices[] = {0,1,2, 0,2,3};
		
		vertex_t vertices[] = {
			{ x0,y0,  s0,t0,  r,g,b,a },
			{ x0,y1,  s0,t1,  r,g,b,a },
			{ x1,y1,  s1,t1,  r,g,b,a },
			{ x1,y0,  s1,t0,  r,g,b,a }
		};
		
		vertex_buffer_push_back(this->pBuffer, vertices, 4, indices, 6);
		
		pen.x += glyph->advance_x;
		pen.y += glyph->advance_y;

		prev = current;
		
		float height = -(pen.y + markup->font->descender);
		
		if(pen.x > textSize.width) textSize.width = pen.x;
		if(height > textSize.height) textSize.height = height;
	}
}

void CRenderable2dText::onPreDraw(CRenderAbstract* render)
{
	if(pShaderProgram) render->pushShaderProgram(pShaderProgram);
}

void CRenderable2dText::onPostDraw(CRenderAbstract* render)
{
	if(pShaderProgram) render->popShaderProgram();
}
