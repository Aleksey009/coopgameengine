//
//  CRenderableFrame.h
//  Render
//
//  Created by Михайлов Алексей on 22.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderableFrame__
#define __Render__CRenderableFrame__

#include "../IRenderable.h"

#include <vector>
#include <string.h>

enum eRenderableFrameBuffers
{
	RENDERABLE_FRAME_BUFFER_POSITION = 0,
	RENDERABLE_FRAME_BUFFER_UV,
	RENDERABLE_FRAME_BUFFER_COUNT
};

/** 
 Специальный класс вывода данных фрейм буфера:
   выводит квадрат на всю область вьюпорта
   не выставляет своих шейдеров
   выдает все назначенные текстуры) 
 */
class RENDER_API CRenderableFrame: public IRenderable
{
public:
	CRenderableFrame();
	virtual ~CRenderableFrame();
	
	/**
	 Добавляет текстуру в данные отрисовки
	 @param textureName название текстуры в шейдере
	 @param texture указатель на текстуру
	 */
	virtual void pushTexture(const char* textureName, CTexture* texture);
	/**
	 Очищает текстуры из данных отрисовки
	 */
	virtual void clearTextures();
	
	virtual unsigned char	getTexturesCount();
	virtual const char*		getTextureName(unsigned char textureid);
	virtual CTexture*		getTexture(unsigned char textureid);
	virtual unsigned char	getVertexBuffersCount();
	virtual const char*		getVertexBufferName(unsigned char bufferid);
	virtual unsigned long	getVertexBufferSize(unsigned char bufferid);
	virtual void*			getVertexBufferData(unsigned char bufferid);
	virtual unsigned char	getVertexBufferDimensions(unsigned char bufferid);
	virtual unsigned int	getVertexCount();
	virtual unsigned long	getElementsBufferSize();
	virtual void*			getElementsBufferData();
	virtual eDrawMode		getDrawMode();
	
protected:
	struct sTexturesHolder {
		char name[32];
		CTexture* pTexture;
	};
	
	std::vector<sTexturesHolder> textures;
};

#endif /* defined(__Render__CRenderableFrame__) */
