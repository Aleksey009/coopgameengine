//
//  CRenderable2dRectangle.cpp
//  Render
//
//  Created by Михайлов Алексей on 26.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderable2dRectangle.h"

#include "../CRenderAbstract.h"

#include "../texture/CTexture.h"

float rect_pos[4*2] = {
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,
};
float rect_uv[4*2] = {
	0.0f, 1.0f,
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
};
unsigned int rect_elements[6] = {
	0, 1, 2,
	2, 3, 0
};

CRenderable2dRectangle::CRenderable2dRectangle(): CRenderable2d()
{
	pVertexBufferPos = &(rect_pos[0]);
	pVertexBufferUV = new float[4*2];
	pVertexBufferColor = new float [4*4];
	for(int i = 0;i < 4*4;i++) pVertexBufferColor[i] = 1.0f;
	
	pElementsBuffer = &(rect_elements[0]);
}

CRenderable2dRectangle* CRenderable2dRectangle::setRectangle(float x, float y, float width, float height)
{
	pUniformTranform = glm::mat3(1.0f);
	
	pUniformTranform[0][0] = width;
	pUniformTranform[1][1] = height;
	pUniformTranform[2][0] = x;
	pUniformTranform[2][1] = y;
	
	return this;
}

void CRenderable2dRectangle::getRectangle(float* x, float* y, float* width, float* height)
{
	*x = pUniformTranform[2][0];
	*y = pUniformTranform[2][1];
	*width = pUniformTranform[0][0];
	*height = pUniformTranform[1][1];
}

void CRenderable2dRectangle::setTransform(glm::mat3 transform)
{
	this->pUniformTranform = transform;
}

glm::mat3 CRenderable2dRectangle::getTransform()
{
	return this->pUniformTranform;
}

CRenderable2dRectangle* CRenderable2dRectangle::setVertexColor(unsigned long vertexid, float r, float g, float b, float a)
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	bool needLoad = false;
	
	if(render->isVertexBufferLoaded(this, RENDERABLE_2D_BUFFER_COLOR)) {
		render->unloadVertexBuffer(this, RENDERABLE_2D_BUFFER_COLOR);
		needLoad = true;
	}
	
	pVertexBufferColor[vertexid*4 + 0] = r;
	pVertexBufferColor[vertexid*4 + 1] = g;
	pVertexBufferColor[vertexid*4 + 2] = b;
	pVertexBufferColor[vertexid*4 + 3] = a;
	
	if(needLoad) render->loadVertexBuffer(this, RENDERABLE_2D_BUFFER_COLOR);
	
	return this;
}

void CRenderable2dRectangle::getVertexColor(unsigned long vertexid, float* r, float* g, float* b, float* a)
{
	*r = pVertexBufferColor[vertexid*4 + 0];
	*g = pVertexBufferColor[vertexid*4 + 1];
	*b = pVertexBufferColor[vertexid*4 + 2];
	*a = pVertexBufferColor[vertexid*4 + 3];
}

CRenderable2dRectangle* CRenderable2dRectangle::setAllVertexColor(float r, float g, float b, float a)
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	bool needLoad = false;
	
	if(render->isVertexBufferLoaded(this, RENDERABLE_2D_BUFFER_COLOR)) {
		render->unloadVertexBuffer(this, RENDERABLE_2D_BUFFER_COLOR);
		needLoad = true;
	}
	
	for(int i = 0;i < 4;i++) {
		pVertexBufferColor[i*4 + 0] = r;
		pVertexBufferColor[i*4 + 1] = g;
		pVertexBufferColor[i*4 + 2] = b;
		pVertexBufferColor[i*4 + 3] = a;
	}
		
	if(needLoad) render->loadVertexBuffer(this, RENDERABLE_2D_BUFFER_COLOR);
	
	return this;
}

CRenderable2dRectangle* CRenderable2dRectangle::setTexture(CTexture* texture)
{
	CTexture* old = pTexture;
	
	pTexture = texture;
	
	if(texture == NULL) pUniformUseTexture = 0;
	else {
		CRenderAbstract* render = CRenderAbstract::getInstance();
		bool needLoad = false;
		
		if(render->isVertexBufferLoaded(this, RENDERABLE_2D_BUFFER_UV)) {
			render->unloadVertexBuffer(this, RENDERABLE_2D_BUFFER_UV);
			needLoad = true;
		}
		
		pTexture->incCounter();
		
		pUniformUseTexture = 1;
		
		float sx, sy, width, height;
		texture->getTransformUV(&sx, &sy, &width, &height);
		
		for(int i = 0;i < 4;i++) {
			pVertexBufferUV[2*i + 0] = sx + rect_uv[2*i + 0] * width;
			pVertexBufferUV[2*i + 1] = sy + rect_uv[2*i + 1] * height;
		}
		
		if(needLoad) render->loadVertexBuffer(this, RENDERABLE_2D_BUFFER_UV);
	}
	
	// вынесено ПОСЛЕ подключения новой, чтобы при смене текстуры в пределах одной текстурной карты не происходило перезагрузки в видеопамять
	if(old) old->decCounter();
	
	return this;
}

unsigned int CRenderable2dRectangle::getVertexCount()
{
	return 6;
}

unsigned long CRenderable2dRectangle::getVertexBufferSize(unsigned char bufferid)
{
	if(bufferid == RENDERABLE_2D_BUFFER_COLOR) return sizeof(float)*(4*4);
	else return sizeof(float)*(4*2);
}

unsigned long CRenderable2dRectangle::getElementsBufferSize()
{
	return sizeof(unsigned int)*6;
}

eDrawMode CRenderable2dRectangle::getDrawMode()
{
	return DRAW_TRIANGLES;
}
