//
//  CRenderable3dCube.h
//  Render
//
//  Created by Кузьмин Владимир on 11.09.14.
//  Copyright (c) aunshilord@yahoo.com. All rights reserved.
//

#include "CRenderable3dCube.h"

const unsigned int vertex_count = 8;

float cube_pos[vertex_count * 3] = {
	// x     y     z
	1.0f, -1.0f, -1.0f, // v0
	-1.0f, -1.0f, -1.0f, // v1
	-1.0f, 1.0f, -1.0f, // v2
	1.0f, 1.0f, -1.0f, // v3
	1.0f, -1.0f, 1.0f, // v4
	-1.0f, -1.0f, 1.0f, // v5
	-1.0f, 1.0f, 1.0f, // v6
	1.0f, 1.0f, 1.0f,  // v7
};

unsigned int cube_elements[36] = {
	0, 4, 5,
	0, 5, 1,
	1, 5, 6,
	1, 6, 7,
	2, 6, 7,
	2, 7, 3,
	3, 7, 4,
	3, 4, 0,
	5, 4, 7,
	5, 7, 6,
	1, 0, 3,
	1, 2, 3
};

float cube_normals[vertex_count * 3] = {
	1.0f, 2.0f, 1.0f,
	0.0f, 1.6f, 0.0f,
	0.0f, 0.0f, -1.33f,
	1.6f, -0.8f, -0.8f,
	4.0f, 0.0f, -0.8f,
	2.4f, 1.6f, 1.6f,
	2.0f, 0.0f, 2.0f,
	2.0f, -2.0f, 2.0f
};

CRenderable3dCube::CRenderable3dCube() : CRenderable3d()
{
	pVertexBufferPos = &(cube_pos[0]);
	pVertexBufferUV = new float[vertex_count * 2];
	pVertexBufferColor = new float[vertex_count * 4];
	for (int i = 0; i < vertex_count * 4; i++) pVertexBufferColor[i] = 1.0f;
	pVertexBufferNormal = &(cube_normals[0]);

	pElementsBuffer = &(cube_elements[0]);
}

CRenderable3dCube::~CRenderable3dCube()
{

}

unsigned int CRenderable3dCube::getVertexCount()
{
	return 36;
}

unsigned long CRenderable3dCube::getVertexBufferSize(unsigned char bufferid)
{
	if (bufferid == RENDERABLE_3D_BUFFER_POSITION) return vertex_count*sizeof(float) * 3;
	if (bufferid == RENDERABLE_3D_BUFFER_UV) return vertex_count*sizeof(float) * 2;
	if (bufferid == RENDERABLE_3D_BUFFER_COLOR) return vertex_count*sizeof(float) * 4;
	if (bufferid == RENDERABLE_3D_BUFFER_NORMAL) return vertex_count*sizeof(float) * 3;

	return 0;
}

unsigned long CRenderable3dCube::getElementsBufferSize()
{
	return sizeof(unsigned int)*(36);
}

eDrawMode CRenderable3dCube::getDrawMode()
{
	return DRAW_TRIANGLES;
}
