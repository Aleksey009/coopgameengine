//
//  CRenderable2dText.h
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderable2dText__
#define __Render__CRenderable2dText__

#include "../../common/defines.h"
#include "../../common/sSize.h"

#include <string>

class CFont;
class CShaderProgram;
class CRenderAbstract;
struct vertex_buffer_t;

class RENDER_API CRenderable2dText
{
public:
	CRenderable2dText();
	CRenderable2dText(CFont* font);
	virtual ~CRenderable2dText();
	
	static void setShaderProgram(CShaderProgram* shaderProgram);
	
	virtual vertex_buffer_t* getVertexBuffer();
	
	virtual void setFont(CFont* font);
	virtual CFont* getFont();
	
	virtual void setText(std::wstring text);
	virtual void setText(const wchar_t* text);
	virtual void setText(const char* text);
	virtual std::wstring getText();
	
	virtual void setPrefferedLineWidth(float width);
	virtual float getPrefferedLineWidth();
	
	virtual sSize<float> getTextSize();
	
	virtual void setMaxLines(int lines);
	virtual int getMaxLines();
	
	virtual void setPosition(float x, float y);
	virtual void getPosition(float* x, float* y);
	
	virtual void setTransform(float* transform);
	virtual float* getTransform();
	
	virtual void onPreDraw(CRenderAbstract* render);
	virtual void onPostDraw(CRenderAbstract* render);
	
private:
	virtual void rebuildBuffer();
	
	static CShaderProgram* pShaderProgram;
	
	vertex_buffer_t* pBuffer;
	CFont* pFont;
	std::wstring text;
	
	float prefferedLineWidth;
	int maxLines;
	
	sSize<float> textSize;
	
	float* pUniformTranform;
};

#endif /* defined(__Render__CRenderable2dText__) */
