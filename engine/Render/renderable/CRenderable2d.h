//
//  CRenderable2d.h
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderable2d__
#define __Render__CRenderable2d__

#include "../IRenderable.h"

#include "../../../third_party/glm/glm.hpp"

#include <list>
#include <stack>

class CShaderProgram;

enum eRenderable2dUniforms
{
	RENDERABLE_2D_UNIFORM_COORD_SYSTEM = 0,
	RENDERABLE_2D_UNIFORM_TRANSFORM,
	RENDERABLE_2D_UNIFORM_USE_TEXTURE,
	RENDERABLE_2D_UNIFORM_COUNT
};

enum eRenderable2dBuffers
{
	RENDERABLE_2D_BUFFER_POSITION = 0,
	RENDERABLE_2D_BUFFER_UV,
	RENDERABLE_2D_BUFFER_COLOR,
	RENDERABLE_2D_BUFFER_COUNT
};

class RENDER_API CRenderable2d : public IRenderable
{
	friend class CGUIView;
public:
	CRenderable2d();
	virtual ~CRenderable2d();
	
	/**
	 Устанавливает шейдерную программу, используемую для отрисовки 2д объектов
	 @param shaderProgram шейдерная программа
	 */
	static void setShaderProgram(CShaderProgram* shaderProgram);
	
	// ---- IRenderable ----
	virtual void onPreDraw(CRenderAbstract* render);
	virtual void onPostDraw(CRenderAbstract* render);
	
	virtual unsigned char	getTexturesCount();
	virtual const char*		getTextureName(unsigned char textureid);
	virtual CTexture*		getTexture(unsigned char textureid);
	
	virtual unsigned char	getUniformsCount();
	virtual const char*		getUniformName(unsigned char uniformid);
	virtual eUniformType	getUniformType(unsigned char uniformid);
	virtual void*			getUniformData(unsigned char uniformid);
	
	virtual unsigned char	getVertexBuffersCount();
	virtual const char*		getVertexBufferName(unsigned char bufferid);
	virtual void*			getVertexBufferData(unsigned char bufferid);
	virtual unsigned char	getVertexBufferDimensions(unsigned char bufferid);
	
	virtual void*			getElementsBufferData();
	
protected:	
	static CShaderProgram* pShaderProgram;
	
	CTexture* pTexture;

	glm::mat3* pUniformCoordSystem;
	float* pUniformCoordSystemByViewportBuffer;
	glm::mat3 pUniformTranform;
	int   pUniformUseTexture;
	
	float* pVertexBufferPos;
	float* pVertexBufferUV;
	float* pVertexBufferColor;
	
	unsigned int* pElementsBuffer;
};

#endif /* defined(__Render__CRenderable2d__) */
