//
//  CGUIWindowView.cpp
//  Render
//
//  Created by Михайлов Алексей on 27.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUIWindowView.h"
#include "CGUIButtonView.h"
#include "CGUILabelView.h"

#include "../CRenderAbstract.h"
#include "../renderable/CRenderable2dRectangle.h"

#include "../../Input/devices/sMouseState.h"

#include "../../../third_party/glm/gtx/matrix_transform_2d.hpp"

enum eState {
	STATE_IDLE,
	STATE_DRAGGING,
	STATE_RESIZING_LEFT_BORDER,
	STATE_RESIZING_TOP_BORDER,
	STATE_RESIZING_RIGHT_BORDER,
	STATE_RESIZING_BOTTOM_BORDER,
	STATE_RESIZING_LEFT_TOP_POINT,
	STATE_RESIZING_RIGHT_TOP_POINT,
	STATE_RESIZING_RIGHT_BOTTOM_POINT,
	STATE_RESIZING_LEFT_BOTTOM_POINT,
};

CGUIWindowView::CGUIWindowView(): CGUIView()
{
	this->movable = false;
	this->resizable = false;
	this->closable = false;
	this->currentState = STATE_IDLE;
	
	for(int i = 0;i < EDGE_COUNT;i++) borderSize[i] = 7;
	this->headerSize = 30;
	
	for(int i = 0;i < PARTS_COUNT;i++) pWindowPart[i] = NULL;
	pWindowHeader = NULL;
	pWindowCloseBtn = NULL;
	
	initWindowParts();
}

CGUIWindowView::~CGUIWindowView()
{
	
}

CGUIWindowView* CGUIWindowView::setMovable(bool movable)
{
	this->movable = movable;
	
	return this;
}

bool CGUIWindowView::isMovable()
{
	return this->movable;
}

CGUIWindowView* CGUIWindowView::setResizable(bool resizable)
{
	this->resizable = resizable;
	
	return this;
}

bool CGUIWindowView::isResizable()
{
	return this->resizable;
}

CGUIWindowView* CGUIWindowView::setClosable(bool closable)
{
	this->closable = closable;
	
	return this;
}

bool CGUIWindowView::isClosable()
{
	return this->closable;
}

CGUIWindowView* CGUIWindowView::setBorderSize(eEdge border, int borderSize)
{
	this->borderSize[border] = borderSize;
	
	setNeedLayout();
	
	return this;
}

int CGUIWindowView::getBorderSize(eEdge border)
{
	return this->borderSize[border];
}

CGUIWindowView* CGUIWindowView::setHeaderSize(int headerSize)
{
	this->headerSize = headerSize;
	
	setNeedLayout();
	
	return this;
}

int CGUIWindowView::getHeaderSize()
{
	return this->headerSize;
}

CRenderable2dRectangle* CGUIWindowView::getWindowPart(eGUIWindowViewParts part)
{
	return this->pWindowPart[part];
}

CGUILabelView* CGUIWindowView::getHeaderText()
{
	return this->pWindowHeader;
}

CGUIButtonView* CGUIWindowView::getCloseButton()
{
	return this->pWindowCloseBtn;
}

CGUIWindowView* CGUIWindowView::setBordersColor(float r, float g, float b, float a)
{
	float lightr = r + 0.5f;
	float lightg = g + 0.5f;
	float lightb = b + 0.5f;
	
	if(lightr > 1.0f) lightr = 1.0f;
	if(lightg > 1.0f) lightg = 1.0f;
	if(lightb > 1.0f) lightb = 1.0f;
	
	for(int i = 0;i < PARTS_COUNT;i++) {
		switch(i) {
			case PART_TOP_BORDER:
			case PART_RIGHT_TOP_POINT:
			case PART_LEFT_TOP_POINT:
				pWindowPart[i]->setVertexColor(1, lightr, lightg, lightb, a);
				pWindowPart[i]->setVertexColor(2, lightr, lightg, lightb, a);
				pWindowPart[i]->setVertexColor(0, r, g, b, a);
				pWindowPart[i]->setVertexColor(3, r, g, b, a);
				break;
			case PART_RIGHT_BORDER:
			case PART_LEFT_BORDER:
			case PART_BOTTOM_BORDER:
			case PART_LEFT_BOTTOM_POINT:
			case PART_RIGHT_BOTTOM_POINT:
				pWindowPart[i]->setAllVertexColor(r, g, b, a);
				break;
		}
	}
	
	return this;
}

void CGUIWindowView::input(unsigned char button, bool pressed, float x, float y)
{
	CGUIView::input(button, pressed, x, y);
	
	if(!userInteractionEnabled) return;
	
	if(closable && pWindowCloseBtn) pWindowCloseBtn->input(button, pressed, x, y);
	
	int ix = (int)x;
	int iy = (int)y;

	if(button == MOUSE_BUTTON_LEFT) {
		if(pressed) {
			sRectangle<int> iFrame = getFrame();
			
			if(iFrame.havePoint(ix, iy)) {
				
				int lx = ix - iFrame.position.x;
				int ly = iy - iFrame.position.y;
				
				if(movable) {
					if((lx > borderSize[EDGE_LEFT]) &&
					   (lx < (iFrame.size.width - borderSize[EDGE_RIGHT])) &&
					   (ly > (iFrame.size.height - borderSize[EDGE_TOP] - headerSize)) &&
					   (ly < (iFrame.size.height - borderSize[EDGE_TOP]))) {
						currentState = STATE_DRAGGING;
					}
				}
				
				if(resizable) {
					if(lx < borderSize[EDGE_LEFT]) {
						if(ly < borderSize[EDGE_TOP]) {
							currentState = STATE_RESIZING_LEFT_TOP_POINT;
						}
						else if(ly > (iFrame.size.height - borderSize[EDGE_BOTTOM])) {
							currentState = STATE_RESIZING_LEFT_BOTTOM_POINT;
						}
						else {
							currentState = STATE_RESIZING_LEFT_BORDER;
						}
					}
					else if(lx > (iFrame.size.width - borderSize[EDGE_RIGHT])) {
						if(ly < borderSize[EDGE_TOP]) {
							currentState = STATE_RESIZING_RIGHT_TOP_POINT;
						}
						else if(ly > (iFrame.size.height - borderSize[EDGE_BOTTOM])) {
							currentState = STATE_RESIZING_RIGHT_BOTTOM_POINT;
						}
						else {
							currentState = STATE_RESIZING_RIGHT_BORDER;
						}
					}
					else {
						if(ly < borderSize[EDGE_TOP]) {
							currentState = STATE_RESIZING_TOP_BORDER;
						}
						else if(ly > (iFrame.size.height - borderSize[EDGE_BOTTOM])) {
							currentState = STATE_RESIZING_BOTTOM_BORDER;
						}
					}
				}
			}
		}
		else {
			if(currentState != STATE_IDLE) currentState = STATE_IDLE;
		}
	}
}

void CGUIWindowView::input(float x, float y, float dx, float dy)
{
	if(!userInteractionEnabled) {
		CGUIView::input(x, y, dx, dy);
		return;
	}
	
	if(closable && pWindowCloseBtn) pWindowCloseBtn->input(x, y, dx, dy);
	
	sRectangle<int> iFrame = getFrame();
	
	int idx = (int)dx;
	int idy = (int)dy;

	switch(currentState) {
		case STATE_DRAGGING:
			iFrame.position.x += idx;
			iFrame.position.y += idy;
			break;
			
		case STATE_RESIZING_LEFT_TOP_POINT:
			iFrame.position.x += idx;
			iFrame.position.y += idy;
			
			iFrame.size.width -= idx;
			iFrame.size.height -= idy;
			break;
			
		case STATE_RESIZING_TOP_BORDER:
			iFrame.position.y += idy;
			
			iFrame.size.height -= idy;
			break;
			
		case STATE_RESIZING_RIGHT_TOP_POINT:
			iFrame.position.y += idy;
			
			iFrame.size.width += idx;
			iFrame.size.height -= idy;
			break;
			
		case STATE_RESIZING_RIGHT_BORDER:
			iFrame.size.width += idx;
			break;
			
		case STATE_RESIZING_RIGHT_BOTTOM_POINT:
			iFrame.size.width += idx;
			iFrame.size.height += idy;
			break;
			
		case STATE_RESIZING_BOTTOM_BORDER:
			iFrame.size.height += idy;
			break;
			
		case STATE_RESIZING_LEFT_BOTTOM_POINT:
			iFrame.position.x += idx;
			
			iFrame.size.width -= idx;
			iFrame.size.height += idy;
			break;
			
		case STATE_RESIZING_LEFT_BORDER:
			iFrame.position.x += idx;
			
			iFrame.size.width -= idx;
			break;
			
		default:
			CGUIView::input(x, y, dx, dy);
			return;
	}
	
	setFrame(iFrame);
}

void CGUIWindowView::onLayout()
{
	sRectangle<int> pFrame;
	
	setInsets(borderSize[EDGE_LEFT],
			  borderSize[EDGE_TOP] + headerSize,
			  borderSize[EDGE_RIGHT],
			  borderSize[EDGE_BOTTOM]);
	
	if(pWindowPart[PART_LEFT_TOP_POINT]) {
		pFrame = getFrame();
		pFrame.position.y += pFrame.size.height - (borderSize[EDGE_TOP] + headerSize);
		pFrame.size.width = borderSize[EDGE_LEFT];
		pFrame.size.height = borderSize[EDGE_TOP] + headerSize;
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_LEFT_TOP_POINT]->setTransform(transform);
	}
	
	if(pWindowPart[PART_TOP_BORDER]) {
		pFrame = getFrame();
		pFrame.position.x += borderSize[EDGE_LEFT];
		pFrame.position.y += pFrame.size.height - (borderSize[EDGE_TOP] + headerSize);
		pFrame.size.width -= borderSize[EDGE_LEFT] + borderSize[EDGE_RIGHT];
		pFrame.size.height = borderSize[EDGE_TOP] + headerSize;
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_TOP_BORDER]->setTransform(transform);
	}
	
	if(pWindowPart[PART_RIGHT_TOP_POINT]) {
		pFrame = getFrame();
		pFrame.position.x += pFrame.size.width - borderSize[EDGE_LEFT];
		pFrame.position.y += pFrame.size.height - (borderSize[EDGE_TOP] + headerSize);
		pFrame.size.width = borderSize[EDGE_RIGHT];
		pFrame.size.height = borderSize[EDGE_TOP] + headerSize;
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_RIGHT_TOP_POINT]->setTransform(transform);
	}
	
	if(pWindowPart[PART_RIGHT_BORDER]) {
		pFrame = getFrame();
		pFrame.position.x += pFrame.size.width - borderSize[EDGE_RIGHT];
		pFrame.position.y += borderSize[EDGE_BOTTOM];
		pFrame.size.width = borderSize[EDGE_RIGHT];
		pFrame.size.height -= borderSize[EDGE_TOP] + borderSize[EDGE_BOTTOM] + headerSize;
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_RIGHT_BORDER]->setTransform(transform);
	}
	
	if(pWindowPart[PART_RIGHT_BOTTOM_POINT]) {
		pFrame = getFrame();
		pFrame.position.x += pFrame.size.width - borderSize[EDGE_RIGHT];
		pFrame.size.width = borderSize[EDGE_RIGHT];
		pFrame.size.height = borderSize[EDGE_BOTTOM];
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_RIGHT_BOTTOM_POINT]->setTransform(transform);
	}
	
	if(pWindowPart[PART_BOTTOM_BORDER]) {
		pFrame = getFrame();
		pFrame.position.x += borderSize[EDGE_LEFT];
		pFrame.size.width -= borderSize[EDGE_LEFT] + borderSize[EDGE_RIGHT];
		pFrame.size.height = borderSize[EDGE_BOTTOM];
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_BOTTOM_BORDER]->setTransform(transform);
	}
	
	if(pWindowPart[PART_LEFT_BOTTOM_POINT]) {
		pFrame = getFrame();
		pFrame.size.width = borderSize[EDGE_LEFT];
		pFrame.size.height = borderSize[EDGE_BOTTOM];
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_LEFT_BOTTOM_POINT]->setTransform(transform);
	}
	
	if(pWindowPart[PART_LEFT_BORDER]) {
		pFrame = getFrame();
		pFrame.position.y += borderSize[EDGE_BOTTOM];
		pFrame.size.width = borderSize[EDGE_LEFT];
		pFrame.size.height -= borderSize[EDGE_BOTTOM] +borderSize[EDGE_TOP] + headerSize;
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_LEFT_BORDER]->setTransform(transform);
	}
	
	if(pWindowPart[PART_CONTENT]) {
		pFrame = getFrame();
		pFrame.position.x += borderSize[EDGE_LEFT];
		pFrame.position.y += borderSize[EDGE_BOTTOM];
		pFrame.size.width -= borderSize[EDGE_LEFT] + borderSize[EDGE_RIGHT];
		pFrame.size.height -= borderSize[EDGE_BOTTOM] +borderSize[EDGE_TOP] + headerSize;
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pWindowPart[PART_CONTENT]->setTransform(transform);
	}
	
	int closeBtnWidth = 0;
	if(closable && pWindowCloseBtn) closeBtnWidth = pWindowCloseBtn->getFrame().size.width;
	
	if(pWindowHeader) {
		pFrame = getFrame();
		pFrame.position.x += borderSize[EDGE_LEFT];
		pFrame.position.y += pFrame.size.height - (borderSize[EDGE_TOP]);
		pFrame.size.width -= borderSize[EDGE_LEFT] + borderSize[EDGE_RIGHT] + closeBtnWidth;
		pFrame.size.height = headerSize;
		
		pWindowHeader->setFrame(pFrame);
	}
	
	if(pWindowCloseBtn) {
		pFrame = getFrame();
		pFrame.position.x += pFrame.size.width - borderSize[EDGE_RIGHT] - closeBtnWidth;
		pFrame.position.y += pFrame.size.height - (borderSize[EDGE_TOP] + pWindowCloseBtn->getFrame().size.height);
		
		pWindowCloseBtn->setPosition(pFrame.position);
	}
}

void CGUIWindowView::onDrawPre(CRenderAbstract* render)
{
	for(int i = 0;i < PARTS_COUNT;i++) {
		if(pWindowPart[i]) {
			render->loadBuffers(pWindowPart[i]);
			render->draw(pWindowPart[i]);
		}
	}
	
	if(pWindowHeader) pWindowHeader->draw(render);
	if(closable && pWindowCloseBtn) pWindowCloseBtn->draw(render);
}

void CGUIWindowView::initWindowParts()
{
	pWindowPart[PART_LEFT_TOP_POINT] = new CRenderable2dRectangle();
	pWindowPart[PART_TOP_BORDER] = new CRenderable2dRectangle();
	pWindowPart[PART_RIGHT_TOP_POINT] = new CRenderable2dRectangle();
	pWindowPart[PART_RIGHT_BORDER] = new CRenderable2dRectangle();
	pWindowPart[PART_RIGHT_BOTTOM_POINT] = new CRenderable2dRectangle();
	pWindowPart[PART_BOTTOM_BORDER] = new CRenderable2dRectangle();
	pWindowPart[PART_LEFT_BOTTOM_POINT] = new CRenderable2dRectangle();
	pWindowPart[PART_LEFT_BORDER] = new CRenderable2dRectangle();
	pWindowPart[PART_CONTENT] = new CRenderable2dRectangle();
	
	setBordersColor(1.0f, 0.6f, 0, 0.8f);
	
	pWindowHeader = new CGUILabelView();
	pWindowHeader->setText(L"CGUIWindowView");
	
	pWindowCloseBtn = new CGUIButtonView();
	pWindowCloseBtn->setSize({20, 20});
	pWindowCloseBtn->setListener(this);
	
	setNeedLayout();
}

bool CGUIWindowView::isSizeAllowed(sSize<int> newSize)
{
	int closeBtnWidth = 0;
	if(closable && pWindowCloseBtn) closeBtnWidth = pWindowCloseBtn->getFrame().size.width;
	
	if(newSize.width < (borderSize[EDGE_LEFT] + borderSize[EDGE_RIGHT] + closeBtnWidth)) return false;
	if(newSize.height < (borderSize[EDGE_TOP] + borderSize[EDGE_BOTTOM] + headerSize)) return false;
	
	return true;
}

void CGUIWindowView::onButtonPressed(CGUIButtonView* sender)
{
	if(sender != pWindowCloseBtn) return;
	
	printf("close window 0x%lX\n", (unsigned long)this);
}

CGUIView* CGUIWindowView::setFrame(sRectangle<int> frame)
{
	if(isSizeAllowed(frame.size)) return CGUIView::setFrame(frame);
	
	return this;
}
