﻿//
//  CGUICollectionViewCell.h
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUICollectionViewCell__
#define __Render__CGUICollectionViewCell__

#include "../../common/defines.h"

#include "CGUIView.h"

/** Класс ячейки коллекции */
class RENDER_API CGUICollectionViewCell: public CGUIView
{
public:
	
};

#endif /* defined(__Render__CGUICollectionViewCell__) */
