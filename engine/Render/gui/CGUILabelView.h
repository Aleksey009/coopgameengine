//
//  CGUILabelView.h
//  Render
//
//  Created by Михайлов Алексей on 29.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUILabelView__
#define __Render__CGUILabelView__

#include "CGUIView.h"

class CRenderable2dText;

class RENDER_API CGUILabelView: public CGUIView
{
public:
	CGUILabelView();
	virtual ~CGUILabelView();
	
	/**
	 Изменяет текст надписи
	 @param text новый текст
	 @return сам себя
	 */
	virtual CGUILabelView* setText(const wchar_t* text);
	/**
	 Получает текущий текст надписи
	 @return текст
	 */
	virtual const wchar_t* getText();
	
	/**
	 Изменяет максимальное количество строк надписи
	 @param lines количество строк (0 - неограничено)
	 @return сам себя
	 */
	virtual CGUILabelView* setLinesLimit(int lines);
	/**
	 Получает максимальное количество строк
	 @return количество строк
	 */
	virtual int getLinesLimit();
	
	virtual void onLayout();
	virtual void onDrawPre(CRenderAbstract* render);
	
protected:
	CRenderable2dText* pLabelText;
};

#endif /* defined(__Render__CGUILabelView__) */
