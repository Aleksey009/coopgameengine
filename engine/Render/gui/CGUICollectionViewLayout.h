//
//  CGUICollectionViewLayout.h
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUICollectionViewLayout__
#define __Render__CGUICollectionViewLayout__

#include "../../common/defines.h"
#include "../../common/sSize.h"
#include "../../common/sRectangle.h"

class CGUICollectionView;

/** Интерфейс расстановщика элементов коллекции */
class RENDER_API CGUICollectionViewLayout
{
public:
	CGUICollectionViewLayout();
	virtual ~CGUICollectionViewLayout();
	
	/**
	 Изменяет коллекцию, к которой относится расстановщик
	 @param collectionView указатель на коллекцию
	*/
	virtual void setCollectionView(CGUICollectionView* collectionView);
	/**
	 Отмечает необходимость пересборки коллекции
	*/
	virtual void invalidateLayout();
	/**
	 Расставляет элементы если необходимо
	*/
	virtual void prepareIfNeeded();

	/** 
	 Получает размер контента коллекции
	 @return размер
	*/
	virtual sSize<> getContentSize() = 0;
	/**
	 Получает положение и размер элемента коллекции
	 @param itemid индекс элемента коллекции
	 @return положение и размер
	*/
	virtual sRectangle<> getItemFrame(int itemid) = 0;
	/**
	 Выполняет расстановку элементов
	*/
	virtual void prepare() = 0;

protected:
	/** Указатель на прикрепленную коллекцию */
	CGUICollectionView* pCollectionView;
	
private:
	/** Флаг необходимости пересборки коллекции */
	bool needPrepareLayout;
};

#endif /* defined(__Render__CGUICollectionViewLayout__) */
