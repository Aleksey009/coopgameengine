//
//  CGUIScrollView.cpp
//  Render
//
//  Created by Михайлов Алексей on 24.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUIScrollView.h"

#include "../renderable/CRenderable2dRectangle.h"

#include "../CRenderAbstract.h"

#include "../../Input/devices/sMouseState.h"

enum
{
	SCROLL_NONE,
	SCROLL_HORIZONTAL,
	SCROLL_VERTICAL
};

CGUIScrollView::CGUIScrollView()
{
	this->contentSize = {0, 0};
	this->contentOffset = {0, 0};
	
	this->scrollEnabled = false;
	this->scrollState = SCROLL_NONE;
	
	this->horizontalScrollBarMode = SCROLL_BAR_SHOW_NEVER;
	this->verticalScrollBarMode = SCROLL_BAR_SHOW_NEVER;
	
	this->scrollBarsThinkness = 5;
	
	this->horizontalScrollBarAlpha = 0.0f;
	this->verticalScrollBarAlpha = 0.0f;
	
	this->lastScrollTime = 0;
		
	initScrollBars();
}

CGUIScrollView::~CGUIScrollView()
{
	if(this->pHorizontalScrollBar) delete this->pHorizontalScrollBar;
	if(this->pVerticalScrollBar) delete this->pVerticalScrollBar;
}

CGUIScrollView* CGUIScrollView::setContentSize(sSize<> size)
{
	this->contentSize = size;
	
	return this;
}

sSize<> CGUIScrollView::getContentSize()
{
	return this->contentSize;
}

CGUIScrollView* CGUIScrollView::setContentOffset(sPoint<> offset)
{
	this->contentOffset = offset;
	
	setNeedLayout();
	
	return this;
}

sPoint<> CGUIScrollView::getContentOffset()
{
	return this->contentOffset;
}

CGUIScrollView* CGUIScrollView::setScrollEnabled(bool enabled)
{
	this->scrollEnabled = enabled;
	
	return this;
}

bool CGUIScrollView::getScrollEnabled()
{
	return this->scrollEnabled;
}

CGUIScrollView* CGUIScrollView::setHorizontalScrollBarShowMode(eScrollBarShowModes mode)
{
	this->horizontalScrollBarMode = mode;
	
	return this;
}

eScrollBarShowModes CGUIScrollView::getHorizontalScrollBarShowMode()
{
	return this->horizontalScrollBarMode;
}

CGUIScrollView* CGUIScrollView::setVerticalScrollBarShowMode(eScrollBarShowModes mode)
{
	this->verticalScrollBarMode = mode;
	
	return this;
}

eScrollBarShowModes CGUIScrollView::getVerticalScrollBarShowMode()
{
	return this->verticalScrollBarMode;
}

CGUIScrollView* CGUIScrollView::setScrollBarsThinkness(int thinkness)
{
	this->scrollBarsThinkness = thinkness;
	
	setNeedLayout();
	
	return this;
}

int CGUIScrollView::getScrollBarsThinkness()
{
	return this->scrollBarsThinkness;
}

CGUIView* CGUIScrollView::setHorizontalScrollBarColor(sColor color)
{
	if(pHorizontalScrollBar) pHorizontalScrollBar->setAllVertexColor(color.r, color.g, color.b, color.a);
	return this;
}

CGUIView* CGUIScrollView::setHorizontalScrollBarColor(float r, float g, float b, float a)
{
	return setHorizontalScrollBarColor({r, g, b, a});
}

CGUIView* CGUIScrollView::setHorizontalScrollBarTexture(CTexture* texture)
{
	if(pHorizontalScrollBar) pHorizontalScrollBar->setTexture(texture);
	return this;
}

CTexture* CGUIScrollView::getHorizontalScrollBarTexture()
{
	if(pHorizontalScrollBar) return pHorizontalScrollBar->getTexture(0);
	return NULL;
}

CGUIView* CGUIScrollView::setVerticalScrollBarColor(sColor color)
{
	if(pVerticalScrollBar) pVerticalScrollBar->setAllVertexColor(color.r, color.g, color.b, color.a);
	return this;
}

CGUIView* CGUIScrollView::setVerticalScrollBarColor(float r, float g, float b, float a)
{
	return setVerticalScrollBarColor({r, g, b, a});
}

CGUIView* CGUIScrollView::setVerticalScrollBarTexture(CTexture* texture)
{
	if(pVerticalScrollBar) pVerticalScrollBar->setTexture(texture);
	return this;
}

CTexture* CGUIScrollView::getVerticalScrollBarTexture()
{
	if(pVerticalScrollBar) return pVerticalScrollBar->getTexture(0);
	return NULL;
}

void CGUIScrollView::input(unsigned char button, bool pressed, float x, float y)
{
	CGUIView::input(button, pressed, x, y);
	
	int ix = (int)x;
	int iy = (int)y;

	if(button == MOUSE_BUTTON_LEFT) {
		if(pressed) {
			if(scrollState == SCROLL_NONE) {
				sRectangle<> hFrame = getHorizontalScrollBarFrame();
				sRectangle<> vFrame = getVerticalScrollBarFrame();
				
				if(hFrame.havePoint(ix, iy)) scrollState = SCROLL_HORIZONTAL;
				if(vFrame.havePoint(ix, iy)) scrollState = SCROLL_VERTICAL;
			}
		}
		else if(scrollState != SCROLL_NONE) scrollState = SCROLL_NONE;
	}
	
	CGUIView::input(button, pressed, x, y);
}

void CGUIScrollView::input(float x, float y, float dx, float dy)
{
	CGUIView::input(x, y, dx, dy);
	
	sRectangle<> frame = getFrame();
	sPoint<> newOffset;
	
	int idx = (int)dx;
	int idy = (int)dy;

	switch(scrollState) {
		case SCROLL_NONE:
			CGUIView::input(x, y, dx, dy);
			break;
			
		case SCROLL_HORIZONTAL:
			newOffset = contentOffset;
			
			newOffset.x += (int)(idx * ((float)contentSize.width / frame.size.width));
			
			if(newOffset.x > (contentSize.width - frame.size.width)) newOffset.x = (contentSize.width - frame.size.width);
			if(newOffset.x < 0) newOffset.x = 0;
			setContentOffset(newOffset);
			break;
			
		case SCROLL_VERTICAL:
			newOffset = contentOffset;
			
			newOffset.y += (int)(idy * ((float)contentSize.height / frame.size.height));
			
			if(newOffset.y > (contentSize.height - frame.size.height)) newOffset.y = (contentSize.height - frame.size.height);
			if(newOffset.y < 0) newOffset.y = 0;
			setContentOffset(newOffset);
			break;
	}
}

void CGUIScrollView::onLayout()
{
	// для фона
	CGUIView::onLayout();
	
	// проверяем чтоб не уехал контент куда не надо
	sRectangle<> frame = getFrame();
	sPoint<> offset = contentOffset;
	
	if((offset.x + frame.size.width) > contentSize.width) offset.x -= ((frame.size.width + offset.x) - contentSize.width);
	if((offset.y + frame.size.height) > contentSize.height) offset.y -= ((frame.size.height + offset.y) - contentSize.height);
	
	if(offset.x < 0) offset.x = 0;
	if(offset.y < 0) offset.y = 0;
	
	contentOffset = offset;
	
	// скроллбары
	sRectangle<int> pFrame;
	
	if(pHorizontalScrollBar) {
		pFrame = getHorizontalScrollBarFrame();
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pHorizontalScrollBar->setTransform(transform);
	}
	
	if(pVerticalScrollBar) {
		pFrame = getVerticalScrollBarFrame();
		
		glm::mat3 transform = matrixFromFrame(pFrame);
		
		pVerticalScrollBar->setTransform(transform);
	}
}

void CGUIScrollView::onDrawPre(CRenderAbstract* render)
{
	CGUIView::onDrawPre(render);
}

void CGUIScrollView::onDrawPost(CRenderAbstract *render)
{
	CGUIView::onDrawPost(render);

	drawScollBar(horizontalScrollBarMode, pHorizontalScrollBar, render, &horizontalScrollBarAlpha);
	drawScollBar(verticalScrollBarMode, pVerticalScrollBar, render, &verticalScrollBarAlpha);
}

void CGUIScrollView::correctChildsFrame(sRectangle<>* frame)
{
	(*frame).position.x -= contentOffset.x;
	(*frame).position.y -= contentOffset.y;
}

void CGUIScrollView::initScrollBars()
{
	this->pHorizontalScrollBar = new CRenderable2dRectangle();
	this->pHorizontalScrollBar->
		setAllVertexColor(0.8f, 0.8f, 0.8f, 1.0f);
	
	this->pVerticalScrollBar = new CRenderable2dRectangle();
	this->pVerticalScrollBar->
		setAllVertexColor(0.8f, 0.8f, 0.8f, 1.0f);
}

sRectangle<> CGUIScrollView::getHorizontalScrollBarFrame()
{
	sRectangle<> pFrame = getFrame();
	float part;
	
	part = (float)contentOffset.x / (float)contentSize.width;
	if(part > 1.0f) part = 1.0f;
	pFrame.position.x += (int)(part * (pFrame.size.width));
	
	part = (float)(pFrame.size.width) / (float)contentSize.width;
	if(part > 1.0f) part = 1.0f;
	pFrame.size.width = (int)(part * (pFrame.size.width));
	pFrame.size.height = scrollBarsThinkness;
	
	return pFrame;
}

sRectangle<> CGUIScrollView::getVerticalScrollBarFrame()
{
	sRectangle<> pFrame = getFrame();
	float part;
	
	pFrame.position.x += pFrame.size.width - scrollBarsThinkness;
	
	part = (float)contentOffset.y / (float)contentSize.height;
	if(part > 1.0f) part = 1.0f;
	pFrame.position.y += (int)(part * (pFrame.size.height));
	pFrame.size.width = scrollBarsThinkness;
	
	part = (float)(pFrame.size.height) / (float)contentSize.height;
	if(part > 1.0f) part = 1.0f;
	pFrame.size.height = (int)(part * (pFrame.size.height));
	
	return pFrame;
}

void CGUIScrollView::drawScollBar(eScrollBarShowModes mode, CRenderable2dRectangle* scrollbar, CRenderAbstract* render, float* alpha)
{
	bool needDraw = false;
	switch(mode) {
		case SCROLL_BAR_SHOW_NEVER:
			break;
			
		case SCROLL_BAR_SHOW_ALWAYS:
			needDraw = true;
			break;
			
		case SCROLL_BAR_SHOW_WHEN_SCROLLABLE:
			if(scrollbar == pHorizontalScrollBar) {
				if(contentSize.width > getFrame().size.width) needDraw = true;
			}
			if(scrollbar == pVerticalScrollBar) {
				if(contentSize.height > getFrame().size.height) needDraw = true;
			}
			break;
			
		case SCROLL_BAR_SHOW_WHEN_SCROLL:
			/** @todo сделать появление при скролле и плавное исчезание */
			break;
	}
				   
	if(scrollbar && needDraw) {
	   render->loadBuffers(scrollbar);
	   render->draw(scrollbar);
	}
}
