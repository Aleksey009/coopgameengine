//
//  CGUIWindowView.h
//  Render
//
//  Created by Михайлов Алексей on 27.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUIWindowView__
#define __Render__CGUIWindowView__

#include "CGUIView.h"
#include "CGUIButtonView.h"

#include "stdio.h"

enum eGUIWindowViewParts
{
	PART_LEFT_TOP_POINT,
	PART_TOP_BORDER,
	PART_RIGHT_TOP_POINT,
	PART_RIGHT_BORDER,
	PART_RIGHT_BOTTOM_POINT,
	PART_BOTTOM_BORDER,
	PART_LEFT_BOTTOM_POINT,
	PART_LEFT_BORDER,
	PART_CONTENT,
	PARTS_COUNT
};

class CGUILabelView;

/** Элемент интерфейса - окно */
class RENDER_API CGUIWindowView: public CGUIView, CGUIButtonView::IGUIButtonViewListener
{
public:
	CGUIWindowView();
	virtual ~CGUIWindowView();
	
	/**
	 Включает/выключает возможность перемещения окна
	 @param movable true - окно можно перемещать
	 @return сам себя
	 */
	virtual CGUIWindowView* setMovable(bool movable);
	/**
	 Проверяет возможность перемещать окно
	 @return true - окно можно перемещать
	 */
	virtual bool isMovable();
	/**
	 Включает/выключает возможность изменения размеров окна
	 @param resizable true - можно изменять размеры
	 @return сам себя
	 */
	virtual CGUIWindowView* setResizable(bool resizable);
	/**
	 Проверяет возможность изменять размеры окна
	 @return true - можно изменять размеры окна
	 */
	virtual bool isResizable();
	/**
	 Включает/выключает возможность закрытия окна
	 @param closable true - окно можно закрыть
	 @return сам себя
	 */
	virtual CGUIWindowView* setClosable(bool closable);
	/**
	 Проверяет возможность закрывать окно
	 @return true - окно можно закрывать
	 */
	virtual bool isClosable();
	
	/**
	 Изменяет толщину границы окна
	 @param border граница которую нужно изменить
	 @param borderSize толщина
	 @return сам себя
	 */
	virtual CGUIWindowView* setBorderSize(eEdge border, int borderSize);
	/**
	 Получает толщину границы окна
	 @param border граница
	 @return толщина границы
	 */
	virtual int getBorderSize(eEdge border);
	/**
	 Изменяет высоту заголовка окна
	 @param headerSize высота заголовка
	 @return сам себя
	 */
	virtual CGUIWindowView* setHeaderSize(int headerSize);
	/**
	 Получает высоту заголовка окна
	 @return высота заголовка
	 */
	virtual int getHeaderSize();
	
	/**
	 Получает указатель на прямоугольник часть окна (для изменения внешнего вида)
	 @param part идентификатор части окна
	 @return 2D прямоугольник
	 */
	virtual CRenderable2dRectangle* getWindowPart(eGUIWindowViewParts part);
	/**
	 Получает указатель на заголовок окна (для изменения текста и внешнего вида)
	 @return указатель на надпись
	 */
	virtual CGUILabelView* getHeaderText();
	/**
	 Получает указатель на кнопку закрытия (для изменения текста и внешнего вида)
	 @return указатель на кнопку
	 */
	virtual CGUIButtonView* getCloseButton();
	
	/**
	 Изменяет цвет границ окна
	 @return сам себя
	 */
	virtual CGUIWindowView* setBordersColor(float r, float g, float b, float a);
	
	virtual void input(unsigned char button, bool pressed, float x, float y);
	virtual void input(float x, float y, float dx, float dy);
	
	virtual void onLayout();
	virtual void onDrawPre(CRenderAbstract* render);
	
	virtual void onButtonPressed(CGUIButtonView* sender);
	
	virtual CGUIView* setFrame(sRectangle<int> frame);
	
protected:
	/** Текущее активное окно (все остальные окна будут затемнены) */
	static CGUIWindowView* activeWindow;
	
	/**
	 Выполняет создание нужных частей окна
	 */
	virtual void initWindowParts();
	/**
	 Проверяет допустимость размера окна
	 @param newSize проверяемый размер окна
	 @return true - размер допустим
	 */
	virtual bool isSizeAllowed(sSize<int> newSize);
	
	/** Флаг возможности перемещения окна */
	bool movable;
	/** Флаг возможности изменения размеров окна */
	bool resizable;
	/** Флаг возможности закрытия окна */
	bool closable;
	/** Текущее состояние окна (перемещается, изменяет размеры) */
	unsigned char currentState;
	
	/** Толщина границ */
	int borderSize[EDGE_COUNT];
	/** Высота заголовка */
	int headerSize;
	
	/** Части окна (2D прямоугольники) */
	CRenderable2dRectangle* pWindowPart[PARTS_COUNT];
	/** Заголовок окна */
	CGUILabelView* pWindowHeader;
	/** Кнопка закрытия окна */
	CGUIButtonView* pWindowCloseBtn;
};

#endif /* defined(__Render__CGUIWindowView__) */
