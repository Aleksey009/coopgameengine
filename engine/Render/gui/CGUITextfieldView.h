//
//  CGUITextfieldView.h
//  Render
//
//  Created by Михайлов Алексей on 25.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUITextfieldView__
#define __Render__CGUITextfieldView__

#include "CGUIScrollView.h"

#include <string>

class CRenderable2dText;

/** Элемент интерфейса - текстовое поле для ввода */
class RENDER_API CGUITextfieldView: public CGUIScrollView
{
public:
	/** Интерфейс слушателя событий текстового поля */
	class IGUITextfieldViewListener {
	public:
		/**
		 Событие завершения ввода
		 @param sender отправитель события (текстовое поле)
		*/
		virtual void onInputCompleted(CGUITextfieldView* sender) {}
		/**
		 Событие ввода символа
		 @param sender отправитель события
		 @param character отправленный символ
		 @return при возврате true - символ будет добавлен в строку ввода, false - отброшен
		 */
		virtual bool onInputCharacter(CGUITextfieldView* sender, wchar_t character) {return true;}
	};

	CGUITextfieldView();
	virtual ~CGUITextfieldView();
	
	/** 
	 Изменяет объект слушатель событий
	 @param listener указатель на слушатель
	 @return сам себя
	*/
	virtual CGUITextfieldView* setListener(IGUITextfieldViewListener* listener);
	/**
	 Получает объект слушатель событий
	 @return указатель на слушатель
	*/
	virtual IGUITextfieldViewListener* getListener();

	/**
	 Изменяет текст
	 @param text текст
	 @return сам себя
	 */
	virtual CGUITextfieldView* setText(std::wstring text);
	/**
	 Изменяет текст
	 @param text текст
	 @return сам себя
	 */
	virtual CGUITextfieldView* setText(const wchar_t* text);
	/**
	 Получает текст
	 @return текст
	 */
	virtual std::wstring getText();
	
	/**
	 Изменяет текст подсказки
	 @param text текст
	 @return сам себя
	 */
	virtual CGUITextfieldView* setHintText(std::wstring text);
	/**
	 Изменяет текст подсказки
	 @param text текст
	 @return сам себя
	 */
	virtual CGUITextfieldView* setHintText(const wchar_t* text);
	/**
	 Получает текст подсказки
	 @return текст
	 */
	virtual std::wstring getHintText();

	/**
	 Изменяет флаг многострочности
	 @param multilined true - многострочный ввод, иначе - однострочный
	 @return сам себя
	*/
	virtual CGUITextfieldView* setMultiline(bool multilined);
	/**
	 Получает флаг многострочности
	 @return true - многострочный ввод
	*/
	virtual bool isMultiline();

	/**
	 Изменяет флаг только для чтения
	 @param readonly true - только для чтения
	 @return сам себя
	*/
	virtual CGUITextfieldView* setReadonly(bool readonly);
	/**
	 Получает флаг только для чтения
	 @return true - только для чтения
	*/
	virtual bool isReadonly();

	/**
	 Изменяет область выделения
	 @param startpos позиция начала выделения
	 @param endpos позиция конца выделения
	 */
	virtual void setSelection(size_t startpos, size_t endpos);
	/**
	 Получает область выделения
	 @param startpos позиция начала выделения
	 @param endpos позиция конца выделения
	 */
	virtual void getSelection(size_t* startpos, size_t* endpos);
	/**
	 Получает длину области выделения
	 @return длина
	 */
	virtual size_t getSelectionLength();
	
	/**
	 Изменяет позицию каретки
	 @param pos позиция
	 */
	virtual void setCaretPosition(size_t pos);
	/**
	 Получает позицию каретки
	 @return позиция
	 */
	virtual size_t getCaretPosition();
	
	virtual void input(unsigned short key, bool pressed);
	virtual void input(wchar_t character);
	virtual void input(unsigned char button, bool pressed, float x, float y);
	virtual void input(float x, float y, float dx, float dy);
	
	virtual void onLayout();
	virtual void onDrawPre(CRenderAbstract* render);
	virtual void onDrawPost(CRenderAbstract* render);
	virtual void onFocused(CGUIView* lastFocusedView);
	virtual void onLostFocus(CGUIView* newFocusedView);
	
protected:
	/** 
	 Выполняет обновление текста под текущие параметры
	 */
	virtual void rebuildText();

	/** Слушатель событий */
	IGUITextfieldViewListener* pListener;
	/** Набранный текст */
	std::wstring text;
	/** Подсказка (когда текста нет) */
	std::wstring hint;
	/** Флаг только для чтения */
	bool readonly;
	/** Флаг многострочности */
	bool multilined;
	/** Область выделения (0 - начало, 1 - конец) */
	size_t selection[2];
	/** Положение каретки */
	size_t caretPos;
	/** Последнее переключение видимости каретки */
	long lastCaretFlashTime;
	/** Текст рендеринга */
	CRenderable2dText* pRenderText;
};

#endif /* defined(__Render__CGUITextfieldView__) */
