﻿//
//  CGUICollectionView.cpp
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUICollectionView.h"
#include "CGUICollectionViewLayout.h"
#include "CGUICollectionViewDataSource.h"
#include "CGUICollectionViewCell.h"

CGUICollectionView::CGUICollectionView() : CGUIScrollView()
{
	cells = NULL;
	cellsCount = 0;
	pLayout = NULL;
	pDataSource = NULL;
}

CGUICollectionView::~CGUICollectionView()
{
	if(cells) delete [] cells;
}

CGUICollectionView* CGUICollectionView::setLayout(CGUICollectionViewLayout* layout)
{
	this->pLayout = layout;
	layout->setCollectionView(this);

	reloadData();

	return this;
}

CGUICollectionViewLayout* CGUICollectionView::getLayout()
{
	return this->pLayout;
}

CGUICollectionView* CGUICollectionView::setDataSource(CGUICollectionViewDataSource* dataSource)
{
	this->pDataSource = dataSource;
	dataSource->setListener(this);

	reloadData();

	return this;
}

CGUICollectionViewDataSource* CGUICollectionView::getDataSource()
{
	return this->pDataSource;
}

void CGUICollectionView::reloadData()
{
	if (!this->pLayout) return;
	if (!this->pDataSource) return;
	
	if(cells) {
		// убираем все ячейки с видимости и запоминаем для реюза
		for(int i = 0;i < cellsCount;i++) {
			if(!cells[i]) continue;
			
			cells[i]->removeFromSuperview();
			
			reusableCells.push_back(cells[i]);
			
			cells[i] = NULL;
		}
	}
	
	int itemsCount = pDataSource->getItemsCount();
	
	if (itemsCount == 0) return;
	
	if(itemsCount != cellsCount) {
		if(cells) delete [] cells;
		cells = new CGUICollectionViewCell*[itemsCount];
		cellsCount = itemsCount;
		
		for(int i = 0;i < cellsCount;i++) cells[i] = NULL;
	}
	
	pLayout->prepareIfNeeded();
	
	setContentSize(pLayout->getContentSize());
	
	setNeedLayout();
}

void CGUICollectionView::insertItem(int itemid)
{
	

}

void CGUICollectionView::moveItem(int fromid, int toid)
{

}

void CGUICollectionView::deleteItem(int itemid)
{

}

CGUICollectionViewCell* CGUICollectionView::getVisibleCells(int* count)
{
	return NULL;
}

void CGUICollectionView::onLayout()
{
	CGUIScrollView::onLayout();

	if(pLayout) {
		pLayout->prepareIfNeeded();
		
		setContentSize(pLayout->getContentSize());
	}
	
	sRectangle<> frame = getFrame();
	frame.position.x = contentOffset.x;
	frame.position.y = contentOffset.y;
	
	setVisibleArea(frame);
}

void CGUICollectionView::onDrawPre(CRenderAbstract* render)
{
	CGUIScrollView::onDrawPre(render);

	
}

void CGUICollectionView::onDataSourceChanged(CGUICollectionViewDataSource* dataSource)
{
	reloadData();
}

void CGUICollectionView::setVisibleArea(sRectangle<> area)
{
	if(!pLayout) return;
	if(!pDataSource) return;
	
	for(int i = 0;i < cellsCount;i++) {
		sRectangle<> itemFrame = pLayout->getItemFrame(i);
		
		if(itemFrame.isIntersect(area)) {
			if(!cells[i]) {
				CGUICollectionViewCell* reusableCell = NULL;
				if(!reusableCells.empty()) {
					reusableCell = reusableCells.back();
					reusableCells.pop_back();
				}
				
				cells[i] = pDataSource->getItemView(i, reusableCell);
				cells[i]->setFrame(itemFrame);
				addSubview(cells[i]);
				
				// ячейка не была реюзнута, она бесполезна
				if(reusableCell != NULL && reusableCell != cells[i]) delete reusableCell;
			}
		}
		else {
			if(cells[i]) {
				cells[i]->removeFromSuperview();
				
				reusableCells.push_back(cells[i]);
				
				cells[i] = NULL;
			}
		}
	}
}
