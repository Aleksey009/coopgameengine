//
//  CGUIView.cpp
//  Render
//
//  Created by Михайлов Алексей on 27.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUIView.h"

#include "../CRenderAbstract.h"

#include "../renderable/CRenderable2dRectangle.h"

#include "../../../third_party/glm/gtx/matrix_transform_2d.hpp"

CGUIView* CGUIView::pFocusedView = NULL;

CGUIView::CGUIView()
{
	pBackground = NULL;
	hidden = false;
	userInteractionEnabled = true;
	
	frame = {0, 0, 0, 0};
	
	pSuperview = NULL;
	memset(insets, 0, sizeof(float)*EDGE_COUNT);
	
	needLayout = true;
	layoutAlignmentMask = 0;
	layoutSizeMask = 0;
	
	tag = NULL;
}

CGUIView::~CGUIView()
{
	// TODO: чистку списка
}

CGUIView* CGUIView::setBackground(CRenderable2dRectangle *background)
{
	pBackground = background;
	
	return this;
}

CRenderable2dRectangle* CGUIView::getBackground()
{
	return pBackground;
}

CGUIView* CGUIView::setBackgroundColor(sColor color)
{
	if(!pBackground) pBackground = createBackgroundRectangle();
	
	pBackground->setAllVertexColor(color.r, color.g, color.b, color.a);
	
	return this;
}

CGUIView* CGUIView::setBackgroundColor(float r, float g, float b, float a)
{
	return setBackgroundColor({r, g, b, a});
}

CGUIView* CGUIView::setBackgroundTexture(CTexture *texture)
{
	if(!pBackground) pBackground = createBackgroundRectangle();
	
	pBackground->setTexture(texture);
	
	return this;
}

CTexture* CGUIView::getBackgroundTexture()
{
	if(!pBackground) return NULL;
	
	return pBackground->getTexture(0);
}

CGUIView* CGUIView::setHidden(bool hidden)
{
	this->hidden = hidden;
	
	return this;
}

bool CGUIView::isHidden()
{
	return this->hidden;
}

CGUIView* CGUIView::setUserInteractionEnabled(bool enabled)
{
	this->userInteractionEnabled = enabled;
	
	return this;
}

bool CGUIView::isUserInteractionEnabled()
{
	return this->userInteractionEnabled;
}

CGUIView* CGUIView::setFrame(sRectangle<int> frame)
{
	this->frame = frame;
	this->frameByParent = this->frame;
	setNeedLayout();
	
	return this;
}

sRectangle<int> CGUIView::getFrame()
{
	return this->frameByParent;
}

CGUIView* CGUIView::setPosition(sPoint<int> position)
{
	this->frame.position = position;
	this->frameByParent = this->frame;
	setNeedLayout();
	
	return this;
}

CGUIView* CGUIView::setSize(sSize<int> size)
{
	this->frame.size = size;
	this->frameByParent = this->frame;
	setNeedLayout();
	
	return this;
}

CGUIView* CGUIView::addSubview(CGUIView *subview)
{
	subviews.add(subview);
	
	subview->pSuperview = this;
	subview->setNeedLayout();
	
	return this;
}

void CGUIView::removeFromSuperview()
{
	if(!pSuperview) return;
	
	pSuperview->subviews.remove(this);
	
	pSuperview = NULL;
}

CGUIView* CGUIView::setInsets(int left, int top, int right, int bottom)
{
	insets[EDGE_LEFT] = left;
	insets[EDGE_TOP] = top;
	insets[EDGE_RIGHT] = right;
	insets[EDGE_BOTTOM] = bottom;
	
	setNeedLayout();
	
	return this;
}

void CGUIView::getInsets(int *left, int *top, int *right, int *bottom)
{
	*left = insets[EDGE_LEFT];
	*top = insets[EDGE_TOP];
	*right = insets[EDGE_RIGHT];
	*bottom = insets[EDGE_BOTTOM];
}

void CGUIView::setNeedLayout()
{
	needLayout = true;
}

CGUIView* CGUIView::setLayoutAlignmentFlags(unsigned long layoutMask)
{
	this->layoutAlignmentMask = layoutMask;
	
	setNeedLayout();
	
	return this;
}

unsigned long CGUIView::getLayoutAlignmentMask()
{
	return this->layoutAlignmentMask;
}

CGUIView* CGUIView::setLayoutSizeFlags(unsigned long layoutMask)
{
	this->layoutSizeMask = layoutMask;
	
	setNeedLayout();
	
	return this;
}

unsigned long CGUIView::getLayoutSizeMask()
{
	return this->layoutSizeMask;
}

void CGUIView::layout()
{
	frameByParent = frame;
	if(pSuperview) {
		sRectangle<int> parentFrame = pSuperview->childFrame();
		
		if(layoutSizeMask & LAYOUT_SIZE_WIDTH_MATH_PARENT) {
			frameByParent.size.width = parentFrame.size.width - frame.size.width;
		}
		if(layoutSizeMask & LAYOUT_SIZE_HEIGHT_MATH_PARENT) {
			frameByParent.size.height = parentFrame.size.height - frame.size.height;
		}
		
		if(layoutAlignmentMask & LAYOUT_ALIGNMENT_RIGHT) {
			frameByParent.position.x = parentFrame.size.width - frameByParent.size.width - frame.position.x;
		}
		if(layoutAlignmentMask & LAYOUT_ALIGNMENT_TOP) {
			frameByParent.position.y = parentFrame.size.height - frameByParent.size.height - frame.position.y;
		}
		if(layoutAlignmentMask & LAYOUT_ALIGNMENT_CENTER_HORIZONTAL) {
			int parentCenter = parentFrame.size.width / 2;
			int frameCenter = frameByParent.size.width / 2;
			
			frameByParent.position.x = parentCenter - frameCenter - frame.position.x;
		}
		if(layoutAlignmentMask & LAYOUT_ALIGNMENT_CENTER_VERTICAL) {
			int parentCenter = parentFrame.size.height / 2;
			int frameCenter = frameByParent.size.height / 2;
			
			frameByParent.position.y = parentCenter - frameCenter - frame.position.y;
		}
		
		pSuperview->correctChildsFrame(&frameByParent);
	}

	onLayout();
	
	needLayout = false;
	
	CLinkedList<CGUIView*>::iterator i = subviews.begin();
	while(i != NULL) {
		i->pData->layout();
		
		i = i->pNext;
	}
}

CGUIView* CGUIView::setTag(void *tag)
{
	this->tag = tag;
	
	return this;
}

void* CGUIView::getTag()
{
	return this->tag;
}

void CGUIView::setFocused()
{
	if(pFocusedView == this) return;
	
	CGUIView* last = pFocusedView;
	
	pFocusedView = this;
	
	if(last != NULL) last->onLostFocus(this);
	this->onFocused(last);
}

void CGUIView::clearFocus()
{
	if(pFocusedView != this) return;
	
	pFocusedView = NULL;
	
	this->onLostFocus(NULL);
}

bool CGUIView::isFocused()
{
	return (pFocusedView == this);
}

void CGUIView::draw(CRenderAbstract* render)
{
	if(hidden) return;
	
	if(needLayout) layout();
	
	onDrawPre(render);
	
	CLinkedList<CGUIView*>::iterator i = subviews.begin();
	if(i != NULL) {
		sRectangle<> iFrame = getFrame();
		render->pushViewportLocal(iFrame.position.x + insets[EDGE_LEFT],
								  iFrame.position.y + insets[EDGE_BOTTOM],
								  iFrame.size.width - (insets[EDGE_LEFT] + insets[EDGE_RIGHT]),
								  iFrame.size.height - (insets[EDGE_TOP] + insets[EDGE_BOTTOM]));
		while(i != NULL) {
			i->pData->draw(render);
			
			i = i->pNext;
		}
		render->popViewport();
	}
	
	onDrawPost(render);
}

void CGUIView::input(unsigned short key, bool pressed)
{
	CLinkedList<CGUIView*>::iterator i = subviews.begin();
	while(i != NULL) {
		i->pData->input(key, pressed);
		
		i = i->pNext;
	}
}

void CGUIView::input(wchar_t character)
{
	CLinkedList<CGUIView*>::iterator i = subviews.begin();
	while(i != NULL) {
		i->pData->input(character);
		
		i = i->pNext;
	}
}

void CGUIView::input(unsigned char button, bool pressed, float x, float y)
{
	sRectangle<> frame = getFrame();

	float correctx = x - (frame.position.x + insets[EDGE_LEFT]);
	float correcty = y - (frame.position.y + insets[EDGE_BOTTOM]);
	
	if (frame.havePoint((int)x, (int)y)) setFocused();
	
	CLinkedList<CGUIView*>::iterator i = subviews.begin();
	while(i != NULL) {
		i->pData->input(button, pressed, correctx, correcty);
		
		i = i->pNext;
	}
}

void CGUIView::input(float x, float y, float dx, float dy)
{
	sRectangle<> frame = getFrame();

	float correctx = x - (frame.position.x + insets[EDGE_LEFT]);
	float correcty = y - (frame.position.y + insets[EDGE_BOTTOM]);
	
	CLinkedList<CGUIView*>::iterator i = subviews.begin();
	while(i != NULL) {
		i->pData->input(correctx, correcty, dx, dy);
		
		i = i->pNext;
	}
}

void CGUIView::onLayout()
{
	if(!pBackground) return;
	
	glm::mat3 transform = matrixFromFrame(getFrame());
	
	pBackground->setTransform(transform);
}

void CGUIView::onDrawPre(CRenderAbstract* render)
{
	if(!pBackground) return;
	
	render->loadBuffers(pBackground);
	render->draw(pBackground);
}

void CGUIView::onDrawPost(CRenderAbstract *render)
{
	
}

void CGUIView::onFocused(CGUIView *lastFocusedView)
{
	
}

void CGUIView::onLostFocus(CGUIView *newFocusedView)
{
	
}

CRenderable2dRectangle* CGUIView::createBackgroundRectangle()
{
	return new CRenderable2dRectangle();
}

sRectangle<int> CGUIView::childFrame()
{
	sRectangle<int> result = this->frameByParent;
	
	result.position.x += insets[EDGE_LEFT];
	result.position.y += insets[EDGE_TOP];
	
	result.size.width -= insets[EDGE_LEFT] + insets[EDGE_RIGHT];
	result.size.height -= insets[EDGE_TOP] + insets[EDGE_BOTTOM];
	
	return result;
}

void CGUIView::correctChildsFrame(sRectangle<>* frame)
{
	
}

glm::mat3 CGUIView::matrixFromFrame(sRectangle<int> frame)
{
	return glm::mat3(frame.size.width, 0.0f, 0.0f,
					 0.0f, frame.size.height, 0.0f,
					 frame.position.x, frame.position.y, 1.0f);
}
