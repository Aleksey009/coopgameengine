//
//  CGUIButtonView.cpp
//  Render
//
//  Created by Михайлов Алексей on 29.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUIButtonView.h"

#include "../CRenderAbstract.h"

#include "../renderable/CRenderable2dRectangle.h"

#include "../../Input/devices/sMouseState.h"

#include "../../../third_party/glm/gtx/matrix_transform_2d.hpp"

CGUIButtonView::CGUIButtonView()
{
	this->pListener = NULL;
	this->pButtonRectangle = new CRenderable2dRectangle();
	
	stateConfig[BUTTON_STATE_IDLE].pTexture = NULL;
	for(int i = 0;i < 4;i++) {
		stateConfig[BUTTON_STATE_IDLE].vertexColor[i][0] = 0.7f;
		stateConfig[BUTTON_STATE_IDLE].vertexColor[i][1] = 0.7f;
		stateConfig[BUTTON_STATE_IDLE].vertexColor[i][2] = 0.7f;
		stateConfig[BUTTON_STATE_IDLE].vertexColor[i][3] = 1.0f;
	}
	
	stateConfig[BUTTON_STATE_HOVER].pTexture = NULL;
	for(int i = 0;i < 4;i++) {
		stateConfig[BUTTON_STATE_HOVER].vertexColor[i][0] = 0.5f;
		stateConfig[BUTTON_STATE_HOVER].vertexColor[i][1] = 0.5f;
		stateConfig[BUTTON_STATE_HOVER].vertexColor[i][2] = 0.5f;
		stateConfig[BUTTON_STATE_HOVER].vertexColor[i][3] = 1.0f;
	}
	
	stateConfig[BUTTON_STATE_PRESSED].pTexture = NULL;
	for(int i = 0;i < 4;i++) {
		stateConfig[BUTTON_STATE_PRESSED].vertexColor[i][0] = 0.3f;
		stateConfig[BUTTON_STATE_PRESSED].vertexColor[i][1] = 0.3f;
		stateConfig[BUTTON_STATE_PRESSED].vertexColor[i][2] = 0.3f;
		stateConfig[BUTTON_STATE_PRESSED].vertexColor[i][3] = 1.0f;
	}
	
	setState(BUTTON_STATE_IDLE);
}

CGUIButtonView::~CGUIButtonView()
{
	if(this->pButtonRectangle) delete this->pButtonRectangle;
}

CGUIButtonView* CGUIButtonView::setButtonStateConfig(eButtonState state, CGUIButtonView::sButtonStateConfig config)
{
	stateConfig[state] = config;
	
	if(state == currentState) setState(state);
	
	return this;
}

CGUIButtonView::sButtonStateConfig CGUIButtonView::getButtonStateConfig(eButtonState state)
{
	return stateConfig[state];
}

void CGUIButtonView::setState(eButtonState state)
{
	currentState = state;
	
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	if(!hidden) {
		render->unloadVertexBuffer(pButtonRectangle, RENDERABLE_2D_BUFFER_COLOR);
	}
	
	sButtonStateConfig conf = stateConfig[currentState];
	
	pButtonRectangle->setTexture(conf.pTexture);
	for(int i = 0;i < 4;i++) {
		pButtonRectangle->setVertexColor(i,
										 conf.vertexColor[i][0],
										 conf.vertexColor[i][1],
										 conf.vertexColor[i][2],
										 conf.vertexColor[i][3]);
	}
	
	if(!hidden) {
		render->loadVertexBuffer(pButtonRectangle, RENDERABLE_2D_BUFFER_COLOR);
	}
}

eButtonState CGUIButtonView::getState()
{
	return currentState;
}

CGUIButtonView* CGUIButtonView::setListener(CGUIButtonView::IGUIButtonViewListener *listener)
{
	this->pListener = listener;
	
	return this;
}

CGUIButtonView::IGUIButtonViewListener* CGUIButtonView::getListener()
{
	return this->pListener;
}

void CGUIButtonView::input(unsigned char button, bool pressed, float x, float y)
{
	if(button == MOUSE_BUTTON_LEFT) {
		sRectangle<int> iFrame = getFrame();
				
		if((x > iFrame.position.x) &&
		   (x < (iFrame.position.x + iFrame.size.width)) &&
		   (y > iFrame.position.y) &&
		   (y < (iFrame.position.y + iFrame.size.height))) {
			
			if(pressed) setState(BUTTON_STATE_PRESSED);
			else {
				if(this->pListener) this->pListener->onButtonPressed(this);
				
				setState(BUTTON_STATE_HOVER);
			}
		}
		else if(!pressed && (getState() == BUTTON_STATE_PRESSED)) {
			setState(BUTTON_STATE_IDLE);
		}
	}
	
	CGUIView::input(button, pressed, x, y);
}

void CGUIButtonView::input(float x, float y, float dx, float dy)
{
	sRectangle<int> iFrame = getFrame();
	
	if((x > iFrame.position.x) &&
	   (x < (iFrame.position.x + iFrame.size.width)) &&
	   (y > iFrame.position.y) &&
	   (y < (iFrame.position.y + iFrame.size.height))) {
		
		if(getState() == BUTTON_STATE_IDLE) setState(BUTTON_STATE_HOVER);
	}
	else if(getState() == BUTTON_STATE_HOVER) {
		setState(BUTTON_STATE_IDLE);
	}
	
	CGUIView::input(x, y, dx, dy);
}

void CGUIButtonView::onLayout()
{
	glm::mat3 transform = matrixFromFrame(getFrame());
	
	pButtonRectangle->setTransform(transform);
}

void CGUIButtonView::onDrawPre(CRenderAbstract *render)
{
	render->loadBuffers(pButtonRectangle);
	render->draw(pButtonRectangle);
}
