//
//  CGUIView.h
//  Render
//
//  Created by Михайлов Алексей on 27.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUIView__
#define __Render__CGUIView__

#include "../../common/defines.h"
#include "../../common/sColor.h"
#include "../../common/sRectangle.h"
#include "../../common/CLinkedList.h"

#include "../../../third_party/glm/glm.hpp"

#include "string.h"

class CRenderAbstract;

enum eEdge
{
	EDGE_LEFT,
	EDGE_TOP,
	EDGE_RIGHT,
	EDGE_BOTTOM,
	EDGE_COUNT
};

enum eLayoutAlignmentFlags {
	/** Элемент интерфейса будет расположен слева с отступом в <заданная позиция x элемента> от левого края родителя */
	LAYOUT_ALIGNMENT_LEFT				= 0x00000000,
	/** Элемент интерфейса будет расположен снизу с отступом в <заданная позиция y элемента> от нижнего края родителя */
	LAYOUT_ALIGNMENT_BOTTOM				= 0x00000000,
	/** Элемент интерфейса будет расположен справа с отступом в <заданная позиция x элемента> от правого края родителя */
	LAYOUT_ALIGNMENT_RIGHT				= 0x00000001,
	/** Элемент интерфейса будет расположен сверху с отступом в <заданная позиция y элемента> от верхнего края родителя */
	LAYOUT_ALIGNMENT_TOP				= 0x00000010,
	/** Элемент интерфейса будет расположен посередине вертикально с отступом в <заданная позиция x элемента> от средней линии родителя */
	LAYOUT_ALIGNMENT_CENTER_VERTICAL	= 0x00000100,
	/** Элемент интерфейса будет расположен посередине горизонтально с отступом в <заданная позиция y элемента> от средней линии родителя */
	LAYOUT_ALIGNMENT_CENTER_HORIZONTAL	= 0x00001000,
};

/** @todo размер на основе дочерних элементов */
enum eLayoutSizeFlags {
	LAYOUT_SIZE_CONSTANT = 0,
	/** Ширина элемента интерфейса будет равна (<ширина родителя> - <заданная ширина элемента>) */
	LAYOUT_SIZE_WIDTH_MATH_PARENT		= 0x00000001,
	/** Высота элемента интерфейса будет равна (<высота родителя> - <заданная высота элемента>) */
	LAYOUT_SIZE_HEIGHT_MATH_PARENT		= 0x00000010,
	/** Ширина элемента интерфейса будет равна (<ширина дочерних элементов> + <заданная ширина элемента> */
	LAYOUT_SIZE_WIDTH_WRAP_CONTENT		= 0x00000100,
	/** Высота элемента интерфейса будет равна (<высота дочерних элементов> + <заданная высота элемента> */
	LAYOUT_SIZE_HEIGHT_WRAP_CONTENT		= 0x00001000,
};

class CRenderable2dRectangle;
class CTexture;

/** Базовый класс всех элементов интерфейса, реализует вложенность интерфейса */
class RENDER_API CGUIView
{
public:
	CGUIView();
	virtual ~CGUIView();
	
	/**
	 Изменяет фон (2d прямоугольник), если NULL - фона нет и ничего не рисуется
	 @param background 2d прямоугольник
	 @return сам себя
	 */
	virtual CGUIView* setBackground(CRenderable2dRectangle* background);
	/**
	 Получает фон
	 @return 2d прямоугольник или NULL
	 */
	virtual CRenderable2dRectangle* getBackground();
	/**
	 Изменяет цвет фона
	 @param color цвет
	 @return сам себя
	 */
	virtual CGUIView* setBackgroundColor(sColor color);
	/**
	 Изменяет цвет фона
	 @param r красный
	 @param g зеленый
	 @param b синий
	 @param a альфа канал
	 @return сам себя
	 */
	virtual CGUIView* setBackgroundColor(float r, float g, float b, float a);
	/**
	 Изменяет текстуру фона
	 @param texture текстура
	 @return сам себя
	 */
	virtual CGUIView* setBackgroundTexture(CTexture* texture);
	/**
	 Получает текстуру фона
	 @return текстура
	 */
	virtual CTexture* getBackgroundTexture();
	
	/**
	 Изменяет видимость элемента
	 @param hidden true - элемент не видим
	 @return сам себя
	 */
	virtual CGUIView* setHidden(bool hidden);
	/**
	 Проверяет видимость элемента
	 @return true - элемент скрыт
	 */
	virtual bool isHidden();
	
	/**
	 Включает/выключает интерактивность
	 @param enabled если true - пользователь может взаимодействовать с элементом
	 @return сам себя
	 */
	virtual CGUIView* setUserInteractionEnabled(bool enabled);
	/**
	 Проверяет включена ли интерактивность
	 @return true - пользователь может взаимодействовать с элементом
	 */
	virtual bool isUserInteractionEnabled();
	
	/**
	 Изменяет положение и размер элемента в координатной системе родителя
	 @param frame положение и размер
	 @return сам себя
	 */
	virtual CGUIView* setFrame(sRectangle<int> frame);
	/**
	 Получает положение и размер элемента в координатной системе родителя
	 @return положение и размер
	 */
	virtual sRectangle<int> getFrame();
	/**
	 Изменяет положение элемента в координатной системе родителя
	 @param position положение
	 @return сам себя
	 */
	virtual CGUIView* setPosition(sPoint<int> position);
	/**
	 Изменяет размер элемента в координатной системе родителя
	 @param size размер
	 @return сам себя
	 */
	virtual CGUIView* setSize(sSize<int> size);
		
	/**
	 Добавляет дочерний элемент интерфейса (элемент будет отрисовываться при отрисовке владельца и измеряться по размеру владельца)
	 @param subview элемент интервейса
	 @return возвращает сам себя
	 */
	virtual CGUIView* addSubview(CGUIView* subview);
	/**
	 Удаляет элемент из списка дочерних элементов родителя
	 */
	virtual void removeFromSuperview();
	/**
	 Изменяет отступы дочерних элементов
	 @param left слева
	 @param top сверху
	 @param right справа
	 @param bottom снизу
	 @return сам себя
	 */
	virtual CGUIView* setInsets(int left, int top, int right, int bottom);
	/**
	 Получает отступы дочерних элементов
	 @param left слева
	 @param top сверху
	 @param right справа
	 @param bottom снизу
	 */
	virtual void getInsets(int* left, int* top, int* right, int* bottom);
	
	/**
	 Устанавливает флаг необходимости лэйаута, произойдет при следующей отрисовке. Все дочерние элементы тоже получат этот флаг
	 */
	virtual void setNeedLayout();
	/**
	 Устанавливает маску выравнивания (eLayoutAlignmentFlags)
	 @param маска комбинируемая флагами eLayoutAlignmentFlags
	 @return возвращает сам себя
	 */
	virtual CGUIView* setLayoutAlignmentFlags(unsigned long layoutMask);
	/**
	 Получает маску выравнивания
	 @return маска комбинируемая флагами eLayoutAlignmentFlags
	 */
	virtual unsigned long getLayoutAlignmentMask();
	/**
	 Устанавливает маску размера (eLayoutSizeFlags)
	 @param маска комбинируемая флагами eLayoutSizeFlags
	 @return возвращает сам себя
	 */
	virtual CGUIView* setLayoutSizeFlags(unsigned long layoutMask);
	/**
	 Получает маску размера
	 @return маска комбинируемая флагами eLayoutSizeFlags
	 */
	virtual unsigned long getLayoutSizeMask();
	
	/**
	 Устанавливает тэг элемента (для идентификации вьюшки)
	 @param tag тэг - представляет собой указатель (можно записать число, либо указатель на сложную конструкцию)
	 @return сам себя
	 */
	virtual CGUIView* setTag(void* tag);
	/**
	 Получает тэг элемента
	 @return тэг
	 */
	virtual void* getTag();
	
	/**
	 Устанавливает фокус на данный элемент
	 */
	virtual void setFocused();
	/**
	 Сбрасывает фокус с данного элемента
	 */
	virtual void clearFocus();
	/**
	 Проверяет установлен ли фокус на данный элемент
	 @return true - данный элемент в фокусе
	 */
	virtual bool isFocused();
	
	/**
	 Отрисовывает элемент и его дочерние элементы и декорации, а так же вызывает проводит пересчет координатных систем дочерних элементов
	 @param render рендер для отрисовки
	 */
	virtual void draw(CRenderAbstract* render);
	
	/**
	 Выполняет обработку событий ввода с клавиатуры
	 @param key клавиша
	 @param pressed флаг события нажатия (если ложь - значит отпустили кнопку)
	 */
	virtual void input(unsigned short key, bool pressed);
	/**
	 Выполняет обработку событий ввода текста с клавиатуры
	 @param character введенный символ
	 */
	virtual void input(wchar_t character);
	/**
	 Выполняет обработку событий ввода с кнопок мыши
	 @param button кнопка мыши
	 @param pressed флаг события нажатия (если ложь - значит отпустили кнопку)
	 @param x координата мыши x
	 @param y координата мыши y
	 */
	virtual void input(unsigned char button, bool pressed, float x, float y);
	/**
	 Выполняет обработку событий движения мыши
	 @param x координата x
	 @param y координата y
	 @param dx изменение координаты x
	 @param dy изменение координаты y
	 */
	virtual void input(float x, float y, float dx, float dy);
	
	/**
	 Событие расположения элемента (по умолчанию выставляет положение фона)
	 */
	virtual void onLayout();
	/**
	 Событие отрисовки элемента перед отрисовкой дочерних (по умолчанию отрисовывает фон)
	 @param render рендер для отрисовки
	 */
	virtual void onDrawPre(CRenderAbstract* render);
	/**
	 Событие отрисовки элемента после отрисовки дочерних (по умолчанию ничего не делает)
	 @param render рендер для отрисовки
	 */
	virtual void onDrawPost(CRenderAbstract* render);
	/**
	 Событие получения фокуса
	 @param lastFocusedView предыдущий элемент который был в фокусе
	 */
	virtual void onFocused(CGUIView* lastFocusedView);
	/**
	 Событие потери фокуса
	 @param newFocusedView новый элемент в фокусе
	 */
	virtual void onLostFocus(CGUIView* newFocusedView);
	
protected:
	/** Вьюшка на которую переключен фокус */
	static CGUIView* pFocusedView;
	
	/**
	 Выполняет расположение дочерних элементов (вызывается автоматически, при установленном флаге needLayout!)
	 */
	virtual void layout();
	/**
	 Создает прямоугольник для отрисовки фона
	 @return 2D прямоугольник
	 */
	virtual CRenderable2dRectangle* createBackgroundRectangle();
	
	virtual sRectangle<int> childFrame();
	virtual void correctChildsFrame(sRectangle<>* frame);
	virtual glm::mat3 matrixFromFrame(sRectangle<int> frame);
	
	/** Прямоугольник фон */
	CRenderable2dRectangle* pBackground;
	/** Флаг скрытости, если элемент скрыт, то он не учавствует в отрисовке */
	bool hidden;
	/** Флаг интерактивности, если выключен - взаимодействие пользователя с элементом не осуществляется */
	bool userInteractionEnabled;
	
	/** Положение и размер элемента */
	sRectangle<int> frame;
	
	/** Связанный список дочерних элементов */
	CLinkedList<CGUIView*> subviews;
	/** Ссылка на родительский элемент */
	CGUIView* pSuperview;
	/** Отсутпы для дочерних элементов */
	int insets[EDGE_COUNT];
	
	/** Флаг определяющий необходимость перерасчета положения элемента и его дочерних элементов */
	bool needLayout;
	/** Маска выравнивания (eLayoutAlignmentFlags) */
	unsigned long layoutAlignmentMask;
	/** Маска размера (eLayoutSizeFlags) */
	unsigned long layoutSizeMask;
	
	/** Тэг элемента, для хранения кастомных данных */
	void* tag;
	
	/** Положение и размер элемента на основе родительских данных */
	sRectangle<int> frameByParent;
};

#endif /* defined(__Render__CGUIView__) */
