//
//  CGUICollectionViewDataSource.cpp
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUICollectionViewDataSource.h"
#include "CGUICollectionView.h"

void CGUICollectionViewDataSource::setListener(IGUICollectionViewDataSourceListener* listener)
{
	this->pListener= listener;
}

void CGUICollectionViewDataSource::notifyDataSourceChanged()
{
	if (pListener) pListener->onDataSourceChanged(this);
}
