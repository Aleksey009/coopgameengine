﻿//
//  CGUICollectionView.h
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUICollectionView__
#define __Render__CGUICollectionView__

#include "CGUIScrollView.h"
#include "CGUICollectionViewDataSource.h"

#include <vector>

class CGUICollectionViewLayout;
class CGUICollectionViewCell;

/** Элемент интерфейса - коллекция */
class RENDER_API CGUICollectionView : public CGUIScrollView, CGUICollectionViewDataSource::IGUICollectionViewDataSourceListener
{
public:
	CGUICollectionView();
	virtual ~CGUICollectionView();

	/**
	 Изменяет расстановщик элементов коллекции
	 @param layout указатель на расстановщик
	 @return сам себя
	*/
	virtual CGUICollectionView* setLayout(CGUICollectionViewLayout* layout);
	/**
	 Получает расстановщик элементов коллекции
	 @return указатель на расстановщик
	*/
	virtual CGUICollectionViewLayout* getLayout();

	/**
	 Изменяет источник данных коллекции
	 @param layout указатель на источник данных
	 @return сам себя
	*/
	virtual CGUICollectionView* setDataSource(CGUICollectionViewDataSource* dataSource);
	/**
	 Получает источник данных коллекции
	 @return указатель на источник данных
	*/
	virtual CGUICollectionViewDataSource* getDataSource();

	/**
	 Обновляет текущие данные коллекции основываясь на источнике данных
	*/
	virtual void reloadData();

	/**
	 Уведомляет коллекцию о добавлении в источник данных нового элемента
	 @param itemid позиция добавленного элемента
	*/
	virtual void insertItem(int itemid);
	/**
	 Уведомляет коллекцию о перемещении элемента в источнике данных
	 @param fromid начальная позиция элемента
	 @param toid конечная позиция элемента
	*/
	virtual void moveItem(int fromid, int toid);
	/**
	 Уведомляет коллекцию о удалении элемента из источника данных
	 @param itemid позиция удаленного элемента
	*/
	virtual void deleteItem(int itemid);

	/**
	 Получает видимые элементы коллекции
	 @param count указатель на переменную, в которую будет записано количество видимых элементов
	 @return указатель на массив видимых элементов
	*/
	virtual CGUICollectionViewCell* getVisibleCells(int* count);

	virtual void onLayout();
	virtual void onDrawPre(CRenderAbstract* render);

	virtual void onDataSourceChanged(CGUICollectionViewDataSource* dataSource);

protected:
	/**
	 Отображает элементы коллекции в заданной области и отправляет те, что не попали в область - в реюз
	 @param area зона в которой должны быть отображены элементы
	 */
	virtual void setVisibleArea(sRectangle<> area);
	
	/** Расстановщик элементов */
	CGUICollectionViewLayout* pLayout;
	/** Источник данных коллекции */
	CGUICollectionViewDataSource* pDataSource;
	/** Массив указателей на ячейки */
	CGUICollectionViewCell** cells;
	/** Количество ячеек в коллекции (может не совпадать с количеством в источнике данных - когда источник обновили, а коллекцию не уведомили) */
	int cellsCount;
	/** Набор ячеек для повторного использования */
	std::vector<CGUICollectionViewCell*> reusableCells;
};

#endif /* defined(__Render__CGUICollectionView__) */
