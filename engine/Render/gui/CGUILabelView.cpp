//
//  CGUILabelView.cpp
//  Render
//
//  Created by Михайлов Алексей on 29.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUILabelView.h"

#include "../renderable/CRenderable2dText.h"

#include "../CRenderAbstract.h"

#include "../../../third_party/glm/gtx/matrix_transform_2d.hpp"

CGUILabelView::CGUILabelView(): CGUIView()
{
	pLabelText = new CRenderable2dText();
	pLabelText->setMaxLines(1);
}

CGUILabelView::~CGUILabelView()
{
	delete pLabelText;
}

CGUILabelView* CGUILabelView::setText(const wchar_t* text)
{
	pLabelText->setText(text);
	
	return this;
}

const wchar_t* CGUILabelView::getText()
{
	return pLabelText->getText().c_str();
}

CGUILabelView* CGUILabelView::setLinesLimit(int lines)
{
	pLabelText->setMaxLines(lines);
	
	return this;
}

int CGUILabelView::getLinesLimit()
{
	return pLabelText->getMaxLines();
}

void CGUILabelView::onLayout()
{
	CGUIView::onLayout();
	
	sRectangle<> frame = getFrame();
	
	sSize<float> textSize = pLabelText->getTextSize();
	
	frame.position.y += (int)textSize.height;
	
	glm::mat3 transform = matrixFromFrame(frame);
	
	pLabelText->setTransform(&(transform[0][0]));
	pLabelText->setPrefferedLineWidth((float)frame.size.width);
}

void CGUILabelView::onDrawPre(CRenderAbstract *render)
{
	CGUIView::onDrawPre(render);
	
	render->draw(pLabelText);
}


