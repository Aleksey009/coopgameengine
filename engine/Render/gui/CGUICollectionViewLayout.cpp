//
//  CGUICollectionViewLayout.cpp
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUICollectionViewLayout.h"
#include "CGUICollectionView.h"

CGUICollectionViewLayout::CGUICollectionViewLayout()
{
	needPrepareLayout = true;
}

CGUICollectionViewLayout::~CGUICollectionViewLayout()
{
	
}

void CGUICollectionViewLayout::setCollectionView(CGUICollectionView* collectionView)
{
	this->pCollectionView = collectionView;
	
	prepare();
}

void CGUICollectionViewLayout::invalidateLayout()
{
	this->needPrepareLayout = true;
}

void CGUICollectionViewLayout::prepareIfNeeded()
{
	if (!this->needPrepareLayout) return;

	prepare();
}
