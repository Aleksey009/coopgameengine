//
//  CGUIManager.h
//  Render
//
//  Created by Михайлов Алексей on 27.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUIManager__
#define __Render__CGUIManager__

#include "../../common/defines.h"

class CGUIView;

/** Менеджер интерфейса пользователя, создает элементы, отрисовывает и передает события ввода */
class RENDER_API CGUIManager
{	
public:
	CGUIManager();
	virtual ~CGUIManager();
	
	/**
	 Проводит первоначальную настройку
	 @param width ширина окна
	 @param height высота окна
	 */
	virtual void init(int width, int height);
	
	/**
	 Добавляет элемент интерфейса для дальнейшей отрисовки и получения событий ввода
	 @param view элемент интерфейса
	 */
	virtual void addView(CGUIView* view);
	/**
	 Изменяет размер холста
	 @param width ширина
	 @param height высота
	 */
	virtual void resize(int width, int height);
	
	/**
	 Передает ввод с клавиатуры интерфейсу
	 @param key клавиша
	 @param pressed флаг события нажатия (если ложь - значит отпустили кнопку)
	 */
	virtual void input(unsigned short key, bool pressed);
	/**
	 Передает ввод текста с клавиатуры интерфейсу
	 @param character введенный символ
	 */
	virtual void input(wchar_t character);
	/**
	 Передает ввод с кнопок мыши интерфейсу
	 @param button кнопка мыши
	 @param pressed флаг события нажатия (если ложь - значит отпустили кнопку)
	 @param x координата мыши x
	 @param y координата мыши y
	 */
	virtual void input(unsigned char button, bool pressed, float x, float y);
	/**
	 Передает ввод с позиции мыши интерфейсу
	 @param x координата x
	 @param y координата y
	 @param dx изменение координаты x
	 @param dy изменение координаты y
	 */
	virtual void input(float x, float y, float dx, float dy);
	/**
	 Отрисовывает интерфейс
	 */
	virtual void draw();
	
private:
	/** Корневой элемент интерфейса (занимает весь экран) */
	CGUIView* pRootView;
};

#endif /* defined(__Render__CGUIManager__) */
