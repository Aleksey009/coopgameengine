//
//  CGUITextfieldView.cpp
//  Render
//
//  Created by Михайлов Алексей on 25.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUITextfieldView.h"

#include "../renderable/CRenderable2dText.h"

#include "../CRenderAbstract.h"

#include "../../Input/CInputManager.h"
#include "../../Input/CKeyboard.h"

CGUITextfieldView::CGUITextfieldView()
{
	this->selection[0] = 0;
	this->selection[1] = 0;
	this->caretPos = 0;
	this->lastCaretFlashTime = 0;
	this->multilined = false;
	
	this->pRenderText = new CRenderable2dText();
	
	setHorizontalScrollBarShowMode(SCROLL_BAR_SHOW_WHEN_SCROLLABLE);
	setVerticalScrollBarShowMode(SCROLL_BAR_SHOW_WHEN_SCROLLABLE);
}

CGUITextfieldView::~CGUITextfieldView()
{
	if(this->pRenderText) delete this->pRenderText;
}

CGUITextfieldView* CGUITextfieldView::setListener(IGUITextfieldViewListener* listener)
{
	this->pListener = listener;
	return this;
}

CGUITextfieldView::IGUITextfieldViewListener* CGUITextfieldView::getListener()
{
	return this->pListener;
}

CGUITextfieldView* CGUITextfieldView::setText(std::wstring text)
{
	this->text = text;
	setNeedLayout();
	
	return this;
}

CGUITextfieldView* CGUITextfieldView::setText(const wchar_t* text)
{
	return setText(std::wstring(text));
}

std::wstring CGUITextfieldView::getText()
{
	return this->text;
}

CGUITextfieldView* CGUITextfieldView::setHintText(std::wstring text)
{
	this->hint = text;
	setNeedLayout();
	
	return this;
}

CGUITextfieldView* CGUITextfieldView::setHintText(const wchar_t* text)
{
	return setHintText(std::wstring(text));
}

std::wstring CGUITextfieldView::getHintText()
{
	return this->hint;
}

CGUITextfieldView* CGUITextfieldView::setReadonly(bool readonly)
{
	this->readonly = readonly;
	setNeedLayout();

	return this;
}

bool CGUITextfieldView::isReadonly()
{
	return this->readonly;
}

CGUITextfieldView* CGUITextfieldView::setMultiline(bool multilined)
{
	this->multilined = multilined;

	return this;
}

bool CGUITextfieldView::isMultiline()
{
	return this->multilined;
}

void CGUITextfieldView::setSelection(size_t startpos, size_t endpos)
{
	this->selection[0] = startpos;
	this->selection[1] = endpos;
}

void CGUITextfieldView::getSelection(size_t* startpos, size_t* endpos)
{
	*startpos = this->selection[0];
	*endpos = this->selection[1];
}

size_t CGUITextfieldView::getSelectionLength()
{
	return this->selection[1] - this->selection[0];
}

void CGUITextfieldView::setCaretPosition(size_t pos)
{
	this->caretPos = pos;
}

size_t CGUITextfieldView::getCaretPosition()
{
	return this->caretPos;
}

void CGUITextfieldView::input(unsigned short key, bool pressed)
{
	CGUIView::input(key, pressed);
	
	if(!isFocused()) return;
	
	if(!pressed) return;
	
	if(key == SDL_SCANCODE_BACKSPACE) {
		if(text.length() > 0) setText(text.substr(0, text.length() - 1));
	}
	if (key == SDL_SCANCODE_RETURN || key == SDL_SCANCODE_KP_ENTER) {
		if (pListener) pListener->onInputCompleted(this);
	}
}

void CGUITextfieldView::input(wchar_t character)
{
	CGUIView::input(character);
	
	if(!isFocused()) return;
	if(readonly) return;
	if(pListener) {
		if(!pListener->onInputCharacter(this, character)) return;
	}

	text += character;
	setText(text);
}

void CGUITextfieldView::input(unsigned char button, bool pressed, float x, float y)
{
	CGUIScrollView::input(button, pressed, x, y);
	
}

void CGUITextfieldView::input(float x, float y, float dx, float dy)
{
	CGUIScrollView::input(x, y, dx, dy);
	
}

void CGUITextfieldView::onLayout()
{
	CGUIScrollView::onLayout();
	
	sRectangle<> tFrame = getFrame();
	
	rebuildText();
	
	sSize<float> textSize = pRenderText->getTextSize();
	
	tFrame.position.x -= contentOffset.x;
	tFrame.position.y += (int)textSize.height - contentOffset.y;
	
	contentSize.width = (int)textSize.width;
	contentSize.height = (int)textSize.height;
	
	glm::mat3 transform = matrixFromFrame(tFrame);
	
	pRenderText->setTransform(&(transform[0][0]));
}

void CGUITextfieldView::onDrawPre(CRenderAbstract* render)
{
	CGUIScrollView::onDrawPre(render);
	
	if(pRenderText) render->draw(pRenderText);
}

void CGUITextfieldView::onDrawPost(CRenderAbstract* render)
{
	CGUIScrollView::onDrawPost(render);
}

void CGUITextfieldView::onFocused(CGUIView* lastFocusedView)
{
	CGUIScrollView::onFocused(lastFocusedView);

	setNeedLayout();
	
	CInputManager::getInstance()->getKeyboard()->startCaptureTextInput();
}

void CGUITextfieldView::onLostFocus(CGUIView* newFocusedView)
{
	CGUIScrollView::onLostFocus(newFocusedView);
	
	setNeedLayout();
	
	CInputManager::getInstance()->getKeyboard()->stopCaptureTextInput();
}

void CGUITextfieldView::rebuildText()
{
	if(!pRenderText) return;
	
	sRectangle<> frame = getFrame();
	
	if (readonly) pRenderText->setText(text);
	else {
		wchar_t cursor = -1;

		if (isFocused()) pRenderText->setText(text + cursor);
		else if (text.length() > 0) pRenderText->setText(text);
		else pRenderText->setText(hint);
	}
	
	if(multilined) {
		pRenderText->setMaxLines(0);
		pRenderText->setPrefferedLineWidth((float)frame.size.width);
	}
	else {
		pRenderText->setMaxLines(1);
		pRenderText->setPrefferedLineWidth(9999);
	}
}
