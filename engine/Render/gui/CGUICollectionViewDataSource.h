//
//  CGUICollectionViewDataSource.h
//  Render
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUICollectionViewDataSource__
#define __Render__CGUICollectionViewDataSource__

#include "../../common/defines.h"
#include "../../common/sSize.h"
#include "../../common/sInsets.h"

class CGUICollectionView;
class CGUICollectionViewCell;

/** Интерфейс источника данных коллекции */
class RENDER_API CGUICollectionViewDataSource
{
public:
	/** Интерфейс слушателя событий источника данных коллекции */
	class IGUICollectionViewDataSourceListener {
	public:
		/** 
		 Событие изменения данных в источнике данных коллекции
		 @param sender источник данных уведомляющий об изменении
		*/
		virtual void onDataSourceChanged(CGUICollectionViewDataSource* dataSource) {}
	};

	/**
	 Изменяет слушателя событий
	 @param listener указатель на слушателя
	*/
	virtual void setListener(IGUICollectionViewDataSourceListener* listener);

	/**
	 Уведомляет слушателя о изменении источника данных
	*/
	virtual void notifyDataSourceChanged();

	/**
	 Получает количество элементов коллекции	 
	 @return количество
	*/
	virtual int getItemsCount() = 0;
	/**
	 Получает вьюшку элемента коллекции
	 @param itemid идентификатор элемента коллекции
	 @param reusableView элемент интерфейса, использованный прежде как вьюшка элемента коллекции
	 @return указатель на элемент интерфейса
	*/
	virtual CGUICollectionViewCell* getItemView(int itemid, CGUICollectionViewCell* reusableView) = 0;
	/**
	 Получает размер элемента коллекции
	 @param itemid идентификатор элемента коллекции
	 @param width ограничение по ширине (0 - не ограничен)
	 @param height ограничение по высоте (0 - не ограничен)
	 @return размер
	 */
	virtual sSize<> getItemSize(int itemid, int width, int height) = 0;
	/**
	 Получает отступы элемента от соседних элементов и границ родителя
	 @param itemid идентификатор элемента коллекции
	 @return отступы
	 */
	virtual sInsets<> getItemInsets(int itemid) = 0;

private:
	/** Слушатель событий */
	IGUICollectionViewDataSourceListener* pListener;
};

#endif /* defined(__Render__CGUICollectionViewDataSource__) */
