//
//  CGUIScrollView.h
//  Render
//
//  Created by Михайлов Алексей on 24.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUIScrollView__
#define __Render__CGUIScrollView__

#include "CGUIView.h"

/** Режимы отображения скроллбара */
enum eScrollBarShowModes {
	/** Скроллбар не будет отображаться */
	SCROLL_BAR_SHOW_NEVER,
	/** Скроллбар будет отображаться всегда */
	SCROLL_BAR_SHOW_ALWAYS,
	/** Скроллбар будет отображаться тогда, когда есть куда скроллить */
	SCROLL_BAR_SHOW_WHEN_SCROLLABLE,
	/** Скроллбар будет отображаться тогда, когда есть куда скроллить и был произведен скролл (либо мышкой навели на область скроллбара) */
	SCROLL_BAR_SHOW_WHEN_SCROLL
};

/** Элемент интерфейса - скролл */
class RENDER_API CGUIScrollView : public CGUIView
{
public:
	CGUIScrollView();
	virtual ~CGUIScrollView();
	
	/**
	 Изменяет размер контента
	 @param size размер контента
	 @return сам себя
	 */
	virtual CGUIScrollView* setContentSize(sSize<> size);
	/**
	 Получает размер контента
	 @return размер контента
	 */
	virtual sSize<> getContentSize();
	
	/**
	 Изменяет положение контента
	 @param offset положение контента
	 @return сам себя
	 */
	virtual CGUIScrollView* setContentOffset(sPoint<> offset);
	/**
	 Получает положение контента
	 @return положение контента
	 */
	virtual sPoint<> getContentOffset();
	
	/**
	 Изменяет состояние возможности скролла
	 @param enabled возможен ли скролл, true - возможен
	 @return сам себя
	 */
	virtual CGUIScrollView* setScrollEnabled(bool enabled);
	/**
	 Получает состояние возможности скролла
	 @return возможен ли скролл
	 */
	virtual bool getScrollEnabled();
	
	/**
	 Изменяет режим отображения горизонтального скроллбара
	 @param mode режим отображения
	 @return сам себя
	 */
	virtual CGUIScrollView* setHorizontalScrollBarShowMode(eScrollBarShowModes mode);
	/**
	 Получает режим отображения горизонтального скроллбара
	 @return режим отображения
	 */
	virtual eScrollBarShowModes getHorizontalScrollBarShowMode();

	/**
	 Изменяет режим отображения вертикального скроллбара
	 @param mode режим отображения
	 @return сам себя
	 */
	virtual CGUIScrollView* setVerticalScrollBarShowMode(eScrollBarShowModes mode);
	/**
	 Получает режим отображения вертикального скроллбара
	 @return режим отображения
	 */
	virtual eScrollBarShowModes getVerticalScrollBarShowMode();
	
	/**
	 Изменяет толщину скроллбаров
	 @param thinkness толщина
	 @return сам себя
	 */
	virtual CGUIScrollView* setScrollBarsThinkness(int thinkness);
	/**
	 Получает толщину скроллбаров
	 @return толщина
	 */
	virtual int getScrollBarsThinkness();
	
	/**
	 Изменяет цвет горизонтального скроллбара
	 @param color цвет
	 @return сам себя
	 */
	virtual CGUIView* setHorizontalScrollBarColor(sColor color);
	/**
	 Изменяет цвет горизонтального скроллбара
	 @param r красный
	 @param g зеленый
	 @param b синий
	 @param a альфа канал
	 @return сам себя
	 */
	virtual CGUIView* setHorizontalScrollBarColor(float r, float g, float b, float a);
	/**
	 Изменяет текстуру горизонтального скроллбара
	 @param texture текстура
	 @return сам себя
	 */
	virtual CGUIView* setHorizontalScrollBarTexture(CTexture* texture);
	/**
	 Получает текстуру горизонтального скроллбара
	 @return текстура
	 */
	virtual CTexture* getHorizontalScrollBarTexture();
	
	/**
	 Изменяет цвет вертикального скроллбара
	 @param color цвет
	 @return сам себя
	 */
	virtual CGUIView* setVerticalScrollBarColor(sColor color);
	/**
	 Изменяет цвет вертикального скроллбара
	 @param r красный
	 @param g зеленый
	 @param b синий
	 @param a альфа канал
	 @return сам себя
	 */
	virtual CGUIView* setVerticalScrollBarColor(float r, float g, float b, float a);
	/**
	 Изменяет текстуру вертикального скроллбара
	 @param texture текстура
	 @return сам себя
	 */
	virtual CGUIView* setVerticalScrollBarTexture(CTexture* texture);
	/**
	 Получает текстуру вертикального скроллбара
	 @return текстура
	 */
	virtual CTexture* getVerticalScrollBarTexture();
	
	virtual void input(unsigned char button, bool pressed, float x, float y);
	virtual void input(float x, float y, float dx, float dy);
	
	virtual void onLayout();
	virtual void onDrawPre(CRenderAbstract* render);
	virtual void onDrawPost(CRenderAbstract* render);
	
protected:
	virtual void correctChildsFrame(sRectangle<>* frame);
	
	virtual void initScrollBars();
	
	virtual sRectangle<> getHorizontalScrollBarFrame();
	virtual sRectangle<> getVerticalScrollBarFrame();
	
	virtual void drawScollBar(eScrollBarShowModes mode, CRenderable2dRectangle* scrollbar, CRenderAbstract* render, float* alpha);
	
	/** Размер контента */
	sSize<> contentSize;
	/** Положение контента на экране */
	sPoint<> contentOffset;
	
	/** Состояние возможности скролла (true - возможен) */
	bool scrollEnabled;
	/** Состояние скролла */
	int scrollState;
	
	/** Режим отображения горизонтального скроллбара */
	eScrollBarShowModes horizontalScrollBarMode;
	/** Режим отображения вертикального скроллбара */
	eScrollBarShowModes verticalScrollBarMode;
	
	/** Толщина скроллбаров */
	int scrollBarsThinkness;
	
	/** Видимость горизонтального скроллбара 0.0-1.0 */
	float horizontalScrollBarAlpha;
	/** Видимость горизонтального скроллбара 0.0-1.0 */
	float verticalScrollBarAlpha;
	
	/** Время последнего скролла */
	long lastScrollTime;
	
	/** Горизонтальный скроллбар */
	CRenderable2dRectangle* pHorizontalScrollBar;
	/** Вертикальный скроллбар */
	CRenderable2dRectangle* pVerticalScrollBar;
};

#endif /* defined(__Render__CGUIScrollView__) */
