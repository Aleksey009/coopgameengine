//
//  CGUICollectionViewListLayout.cpp
//  Render
//
//  Created by Михайлов Алексей on 06.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUICollectionViewListLayout.h"

#include "CGUICollectionView.h"

CGUICollectionViewListLayout::CGUICollectionViewListLayout()
{
	itemsFrame = NULL;
	contentSize.width = 0;
	contentSize.height = 0;
}

CGUICollectionViewListLayout::~CGUICollectionViewListLayout()
{
	if(itemsFrame) delete [] itemsFrame;
}

sSize<> CGUICollectionViewListLayout::getContentSize()
{
	prepareIfNeeded();
	
	return contentSize;
}

sRectangle<> CGUICollectionViewListLayout::getItemFrame(int itemid)
{
	prepareIfNeeded();
	
	return itemsFrame[itemid];
}

void CGUICollectionViewListLayout::prepare()
{
	if(itemsFrame) delete [] itemsFrame;
		
	int currenty = 0;
	CGUICollectionViewDataSource* dataSource = pCollectionView->getDataSource();
	
	if(dataSource == NULL) return;
	
	int itemsCount = dataSource->getItemsCount();
	sRectangle<> frame = pCollectionView->getFrame();
	
	if(itemsCount == 0) return;
	
	itemsFrame = new sRectangle<>[itemsCount];
	for(int i = 0;i < itemsCount;i++) {
		sInsets<> insets = dataSource->getItemInsets(i);
		
		int width = frame.size.width - (insets.left + insets.right);
		int height = 0;
		
		sSize<> size = dataSource->getItemSize(i, width, height);
		
		itemsFrame[i].position.x = insets.left;
		itemsFrame[i].position.y = currenty + insets.top;
		itemsFrame[i].size.width = size.width;
		itemsFrame[i].size.height = size.height;
		
		currenty = itemsFrame[i].position.y + itemsFrame[i].size.height + insets.bottom;
	}
	
	contentSize.width = frame.size.width;
	contentSize.height = currenty;
}
