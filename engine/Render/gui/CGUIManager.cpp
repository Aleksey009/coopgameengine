//
//  CGUIManager.cpp
//  Render
//
//  Created by Михайлов Алексей on 27.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CGUIManager.h"
#include "CGUIView.h"

#include "../CRenderAbstract.h"

CGUIManager::CGUIManager()
{
	this->pRootView = new CGUIView();
}

CGUIManager::~CGUIManager()
{
	if(this->pRootView) delete this->pRootView;
}

void CGUIManager::init(int width, int height)
{
	resize(width, height);
}

void CGUIManager::addView(CGUIView *view)
{
	if(this->pRootView) this->pRootView->addSubview(view);
}

void CGUIManager::resize(int width, int height)
{	
	if(this->pRootView) this->pRootView->setFrame({{0, 0},{width, height}});
}

void CGUIManager::input(unsigned short key, bool pressed)
{
	if(this->pRootView) this->pRootView->input(key, pressed);
}

void CGUIManager::input(wchar_t character)
{
	if(this->pRootView) this->pRootView->input(character);
}

void CGUIManager::input(unsigned char button, bool pressed, float x, float y)
{
	if(this->pRootView) this->pRootView->input(button, pressed, x, y);
}

void CGUIManager::input(float x, float y, float dx, float dy)
{
	if(this->pRootView) this->pRootView->input(x, y, dx, dy);
}

void CGUIManager::draw()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	if(this->pRootView) this->pRootView->draw(render);
}
