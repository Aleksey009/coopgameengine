//
//  CGUICollectionViewListLayout.h
//  Render
//
//  Created by Михайлов Алексей on 06.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUICollectionViewListLayout__
#define __Render__CGUICollectionViewListLayout__

#include "../../common/defines.h"

#include "CGUICollectionViewLayout.h"

/** Расстановщик коллекции - список вертикальный */
class RENDER_API CGUICollectionViewListLayout: public CGUICollectionViewLayout
{
public:
	CGUICollectionViewListLayout();
	virtual ~CGUICollectionViewListLayout();
	
	virtual sSize<> getContentSize();
	virtual sRectangle<> getItemFrame(int itemid);
	virtual void prepare();
	
protected:
	/** Массив фреймов элементов */
	sRectangle<>* itemsFrame;
	/** Размер контента */
	sSize<> contentSize;
};

#endif /* defined(__Render__CGUICollectionViewListLayout__) */
