//
//  CGUIButtonView.h
//  Render
//
//  Created by Михайлов Алексей on 29.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CGUIButtonView__
#define __Render__CGUIButtonView__

#include "CGUIView.h"

enum eButtonState
{
	BUTTON_STATE_IDLE,
	BUTTON_STATE_HOVER,
	BUTTON_STATE_PRESSED,
	BUTTON_STATE_COUNT
};

/** Элемент интерфейса - кнопка */
class CGUIButtonView: public CGUIView
{
public:
	/** Интерфейс слушателя событий кнопки */
	class IGUIButtonViewListener {
	public:
		/**
		 Событие нажатия на кнопку
		 @param sender отправитель события (кнопка, которую нажали)
		 */
		virtual void onButtonPressed(CGUIButtonView* sender) {}
	};
	/** Структура конфигурации состояния кнопки */
	struct sButtonStateConfig
	{
		/** Текстура используемая в данном состоянии */
		CTexture* pTexture;
		/** Цвет вершин в данном состоянии */
		float vertexColor[4][4];
	};
	
	CGUIButtonView();
	virtual ~CGUIButtonView();
	
	/**
	 Устанавливает конфигурацию определенного состояния кнопки
	 @param state состояние кнопки
	 @param config новая конфигурация
	 @return сам себя
	 */
	virtual CGUIButtonView* setButtonStateConfig(eButtonState state, sButtonStateConfig config);
	/**
	 Получает конфигурацию определенного состояния кнопки
	 @param state состояние кнопки
	 @return конфигурация
	 */
	virtual sButtonStateConfig getButtonStateConfig(eButtonState state);
	/**
	 Меняет текущее состояние
	 @param state новое состояние
	 */
	virtual void setState(eButtonState state);
	/**
	 Получает текущее состояние
	 @return состояние
	 */
	virtual eButtonState getState();
	/**
	 Устанавливает объект-слушатель событий
	 @param listener слушатель
	 @return сам себя
	 */
	virtual CGUIButtonView* setListener(IGUIButtonViewListener* listener);
	/**
	 Получает текущий объект-слушатель событий
	 @return слушатель
	 */
	virtual IGUIButtonViewListener* getListener();
	
	virtual void input(unsigned char button, bool pressed, float x, float y);
	virtual void input(float x, float y, float dx, float dy);
	
	virtual void onLayout();
	virtual void onDrawPre(CRenderAbstract* render);
	
protected:
	/** Объект слушатель событий */
	IGUIButtonViewListener* pListener;
	/** 2д прямоугольник кнопки */
	CRenderable2dRectangle* pButtonRectangle;
	/** Массив состояний кнопки */
	sButtonStateConfig stateConfig[BUTTON_STATE_COUNT];
	/** Текущее состояние кнопки */
	eButtonState currentState;
};

#endif /* defined(__Render__CGUIButtonView__) */
