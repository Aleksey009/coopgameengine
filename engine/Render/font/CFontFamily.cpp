//
//  CFontFamily.cpp
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CFontFamily.h"

#include "../../../third_party/freetype-gl/texture-font.h"

CFontFamily::CFontFamily(texture_font_t* font)
{
	this->pFont = font;
}

CFontFamily::~CFontFamily()
{
	delete this->pFont;
}

char* CFontFamily::getName()
{
	return this->pFont->filename;
}

float CFontFamily::getSize()
{
	return this->pFont->size;
}

texture_font_t* CFontFamily::getFontFamily()
{
	return this->pFont;
}
