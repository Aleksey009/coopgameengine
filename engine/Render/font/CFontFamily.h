//
//  CFontFamily.h
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CFontFamily__
#define __Render__CFontFamily__

#include "../../common/defines.h"

struct texture_font_t;

class RENDER_API CFontFamily
{
public:
	CFontFamily(texture_font_t* font);
	virtual ~CFontFamily();
	
	virtual char* getName();
	virtual float getSize();
	
	// freetype-gl lib
	virtual texture_font_t* getFontFamily();
	
private:
	// freetype-gl lib
	texture_font_t* pFont;
};

#endif /* defined(__Render__CFontFamily__) */
