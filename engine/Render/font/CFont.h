//
//  CFont.h
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CFont__
#define __Render__CFont__

#include "../../common/defines.h"

#include "../../../third_party/freetype-gl/vec234.h"

class CFontFamily;
struct markup_t;

class RENDER_API CFont
{
public:
	CFont(CFontFamily* family);
	virtual ~CFont();
	
	virtual CFont* setSize(float size);
	virtual float getSize();
	
	virtual CFont* setBold(bool state);
	virtual bool getBold();
	
	virtual CFont* setItalic(bool state);
	virtual bool getItalic();
	
	virtual CFont* setUnderline(bool state);
	virtual bool getUnderline();
	
	virtual CFont* setOverline(bool state);
	virtual bool getOverline();
	
	virtual CFont* setStrike(bool state);
	virtual bool getStrike();
	
	virtual CFont* setForegroundColor(vec4 color);
	virtual vec4 getForegroundColor();

	virtual CFont* setBackgroundColor(vec4 color);
	virtual vec4 getBackgroundColor();
	
	virtual CFont* setUnderlineColor(vec4 color);
	virtual vec4 getUnderlineColor();
	
	virtual CFont* setOverlineColor(vec4 color);
	virtual vec4 getOverlineColor();
	
	virtual CFont* setStrikeColor(vec4 color);
	virtual vec4 getStrikeColor();
	
	// freetype-gl lib
	virtual markup_t* getMarkup();
	
private:
	CFontFamily* pFontFamily;
	// freetype-gl lib
	markup_t* pMarkup;
};

#endif /* defined(__Render__CFont__) */
