//
//  CFontManager.h
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CFontManager__
#define __Render__CFontManager__

#include "../../common/defines.h"

#include <string.h>
#include <map>

class CFont;
class CFontFamily;
struct texture_atlas_t;

class RENDER_API CFontManager
{
public:
	CFontManager();
	virtual ~CFontManager();
	
	virtual CFont* build(const char* font_family, float size);
	
	// freetype-gl
	virtual texture_atlas_t* getAtlas();
	
private:
	struct cache_key {
		char font_family[64];
		float size;
		
		friend bool operator== (const cache_key& lhs, const cache_key& rhs) {
			return (strcmp(lhs.font_family, rhs.font_family) == 0) && (lhs.size == rhs.size);
		}
		friend bool operator< (const cache_key& lhs, const cache_key& rhs) {
			return (strcmp(lhs.font_family, rhs.font_family) == -1) && (lhs.size < rhs.size);
		}
		friend bool operator> (const cache_key& lhs, const cache_key& rhs) {
			return (strcmp(lhs.font_family, rhs.font_family) == 1) && (lhs.size > rhs.size);
		}
	};
	
	std::map<cache_key, CFontFamily*> *pFontFamilyCache;
	
	// freetype-gl
	texture_atlas_t* pAtlas;
};

#endif /* defined(__Render__CFontManager__) */
