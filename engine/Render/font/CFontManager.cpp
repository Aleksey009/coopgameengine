//
//  CFontManager.cpp
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CFontManager.h"
#include "CFont.h"
#include "CFontFamily.h"

#include "../../../third_party/freetype-gl/texture-font.h"

CFontManager::CFontManager()
{
	this->pFontFamilyCache = new std::map<cache_key, CFontFamily*>();
	this->pAtlas = texture_atlas_new( 512, 512, 1 );
}

CFontManager::~CFontManager()
{
	delete this->pFontFamilyCache;
	delete this->pAtlas;
}

CFont* CFontManager::build(const char* font_family, float size)
{
	cache_key key;
	CFontFamily* family;
	
	strcpy(key.font_family, font_family);
	key.size = size;
	
	if(pFontFamilyCache->find(key) != pFontFamilyCache->end()) {
		family = pFontFamilyCache->at(key);
	}
	else {
		family = new CFontFamily(texture_font_new_from_file(pAtlas, size, font_family));
		
		pFontFamilyCache->insert(std::pair<cache_key, CFontFamily*>(key, family));
	}
	
	return new CFont(family);
}

texture_atlas_t* CFontManager::getAtlas()
{
	return this->pAtlas;
}