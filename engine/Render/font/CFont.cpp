//
//  CFont.cpp
//  Render
//
//  Created by Михайлов Алексей on 25.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CFont.h"
#include "CFontFamily.h"

#include "../../../third_party/freetype-gl/markup.h"

CFont::CFont(CFontFamily* family)
{
	this->pMarkup = new markup_t;
	
	this->pMarkup->font = family->getFontFamily();
	this->pMarkup->family = family->getName();
}

CFont::~CFont()
{
	delete this->pMarkup;
}

CFont* CFont::setSize(float size)
{
	this->pMarkup->size = size;
	
	return this;
}

float CFont::getSize()
{
	return this->pMarkup->size;
}

CFont* CFont::setBold(bool state)
{
	this->pMarkup->bold = state;
	
	return this;
}

bool CFont::getBold()
{
	return (this->pMarkup->bold != 0);
}

CFont* CFont::setItalic(bool state)
{
	this->pMarkup->italic = state;
	
	return this;
}

bool CFont::getItalic()
{
	return (this->pMarkup->italic != 0);
}

CFont* CFont::setUnderline(bool state)
{
	this->pMarkup->underline = state;
	
	return this;
}

bool CFont::getUnderline()
{
	return (this->pMarkup->underline != 0);
}

CFont* CFont::setOverline(bool state)
{
	this->pMarkup->overline = state;
	
	return this;
}

bool CFont::getOverline()
{
	return (this->pMarkup->overline != 0);
}

CFont* CFont::setStrike(bool state)
{
	this->pMarkup->strikethrough = state;
	
	return this;
}

bool CFont::getStrike()
{
	return (this->pMarkup->strikethrough != 0);
}

CFont* CFont::setForegroundColor(vec4 color)
{
	this->pMarkup->foreground_color = color;
	
	return this;
}

vec4 CFont::getForegroundColor()
{
	return this->pMarkup->foreground_color;
}

CFont* CFont::setBackgroundColor(vec4 color)
{
	this->pMarkup->background_color = color;
	
	return this;
}

vec4 CFont::getBackgroundColor()
{
	return this->pMarkup->background_color;
}

CFont* CFont::setUnderlineColor(vec4 color)
{
	this->pMarkup->underline_color = color;
	
	return this;
}

vec4 CFont::getUnderlineColor()
{
	return this->pMarkup->underline_color;
}

CFont* CFont::setOverlineColor(vec4 color)
{
	this->pMarkup->overline_color = color;
	
	return this;
}

vec4 CFont::getOverlineColor()
{
	return this->pMarkup->overline_color;
}

CFont* CFont::setStrikeColor(vec4 color)
{
	this->pMarkup->strikethrough_color = color;

	return this;
}

vec4 CFont::getStrikeColor()
{
	return this->pMarkup->strikethrough_color;
}

markup_t* CFont::getMarkup()
{
	return this->pMarkup;
}
