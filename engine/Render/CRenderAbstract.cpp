//
//  CRenderAbstract.cpp
//  Render
//
//  Created by Михайлов Алексей on 24.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderAbstract.h"

#include "OpenGL/CRenderOpenGL.h"

CRenderAbstract* CRenderAbstract::pInstance = NULL;

CRenderAbstract* CRenderAbstract::getInstance() {
	if(pInstance == NULL) pInstance = new CRenderOpenGL();
	return pInstance;
}