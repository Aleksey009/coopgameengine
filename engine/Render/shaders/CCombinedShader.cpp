//
//  CCombinedShader.cpp
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CCombinedShader.h"

CCombinedShader::CCombinedShader(CShader** shaders, int count)
{
	SHADER_VARIABLES_COUNT_TYPE inputsCount = 0;
	SHADER_VARIABLES_COUNT_TYPE outputsCount = 0;
	SHADER_VARIABLES_COUNT_TYPE uniformsCount = 0;
	size_t codeLength = 0;
	size_t len;
	
	for(int i = 0;i < count;i++) {
		inputsCount += shaders[i]->getInputVariablesCount();
		outputsCount += shaders[i]->getOutputVariablesCount();
		uniformsCount += shaders[i]->getUniformVariablesCount();
		shaders[i]->getCode(&len);
		codeLength += len;
	}
	
	CShader::sVariable* inputs = new CShader::sVariable[inputsCount];
	CShader::sVariable* outputs = new CShader::sVariable[outputsCount];
	CShader::sVariable* uniforms = new CShader::sVariable[uniformsCount];
	
	SHADER_VARIABLES_COUNT_TYPE addedInputCount = 0;
	SHADER_VARIABLES_COUNT_TYPE addedOutputCount = 0;
	SHADER_VARIABLES_COUNT_TYPE addedUniformCount = 0;
	for(int i = 0;i < count;i++) {
		SHADER_VARIABLES_COUNT_TYPE vcount;
		
		vcount = shaders[i]->getInputVariablesCount();
		for(SHADER_VARIABLES_COUNT_TYPE j = 0;j < vcount;j++) {
			addMissingVariable(inputs, addedInputCount, shaders[i]->getInputVariable(j));
		}
		
		vcount = shaders[i]->getOutputVariablesCount();
		for(SHADER_VARIABLES_COUNT_TYPE j = 0;j < vcount;j++) {
			addMissingVariable(outputs, addedOutputCount, shaders[i]->getOutputVariable(j));
		}
		
		vcount = shaders[i]->getUniformVariablesCount();
		for(SHADER_VARIABLES_COUNT_TYPE j = 0;j < vcount;j++) {
			addMissingVariable(uniforms, addedUniformCount, shaders[i]->getUniformVariable(j));
		}
	}
	
	sourceCodeLength = codeLength + ((addedInputCount + addedOutputCount + addedUniformCount) * 64) + 128;
	sourceCode = new char[sourceCodeLength];
	memset(sourceCode, 0, sourceCodeLength);
	
	sprintf(sourceCode, "#version 330 core\n");
	
	// собираем заголовок
	for(SHADER_VARIABLES_COUNT_TYPE i = 0;i < addedInputCount;i++) {
		addVariableDeclarationCode(sourceCode, "in", inputs[i]);
	}
	for(SHADER_VARIABLES_COUNT_TYPE i = 0;i < addedOutputCount;i++) {
		addVariableDeclarationCode(sourceCode, "out", outputs[i]);
	}
	for(SHADER_VARIABLES_COUNT_TYPE i = 0;i < addedUniformCount;i++) {
		addVariableDeclarationCode(sourceCode, "uniform", uniforms[i]);
	}
	
	sprintf(sourceCode, "%svoid main() {\n", sourceCode);
	
	bool* addedShaderCode = new bool[count];
	for(int i = 0;i < count;i++) addedShaderCode[i] = false;
	
	bool* outputsUsed = NULL;
	if(addedOutputCount > 0) {
		outputsUsed = new bool[addedOutputCount];
		for(SHADER_VARIABLES_COUNT_TYPE i = 0;i < addedOutputCount;i++) outputsUsed[i] = false;
	}
	
	int addedShadersCount = 0;
	
	while(addedShadersCount != count) {
		bool added = false;
		
		for(int i = 0;i < count;i++) {
			if(addedShaderCode[i]) continue;
			
			SHADER_VARIABLES_COUNT_TYPE externsCount = shaders[i]->getExternVariablesCount();
			
			if(externsCount > 0) {
				SHADER_VARIABLES_COUNT_TYPE addedExternVariables = 0;
				
				for(SHADER_VARIABLES_COUNT_TYPE j = 0;j < externsCount;j++) {
					bool externAdded = false;
					CShader::sVariable externVariable = shaders[i]->getExternVariable(j);
					
					for(SHADER_VARIABLES_COUNT_TYPE k = 0;k < addedOutputCount;k++) {
						if(!outputsUsed[k]) continue;
						if(strcmp(outputs[k].name, externVariable.name)) continue;
						
						if(strcmp(outputs[k].type, externVariable.type)) {
							printf("EXTERN VARIABLE HAVE INCORRECT TYPE!\n");
						}
						
						externAdded = true;
						break;
					}
					
					if(externAdded) {
						addedExternVariables++;
					}
				}
				
				// если не все внешние переменные определены - пропускаем данную часть кода
				if(addedExternVariables != externsCount) continue;
			}
			
			SHADER_VARIABLES_COUNT_TYPE ocount = shaders[i]->getOutputVariablesCount();
			for(SHADER_VARIABLES_COUNT_TYPE j = 0;j < ocount;j++) {
				CShader::sVariable output = shaders[i]->getOutputVariable(j);
				
				for(SHADER_VARIABLES_COUNT_TYPE k = 0;k < addedOutputCount;k++) {
					if(strcmp(output.name, outputs[k].name)) continue;
					
					outputsUsed[k] = true;
					break;
				}
			}
			
			const char* shaderCode = shaders[i]->getCode(&len);
			sprintf(sourceCode, "%s%s\n", sourceCode, shaderCode);
			
			addedShaderCode[i] = true;

			addedShadersCount++;
			added = true;
		}
		
		if(!added) {
			printf("CANT BUILD SHADER!\n");
			break;
		}
	}
	
	sprintf(sourceCode, "%s}\n", sourceCode);
	
	if(inputs) delete [] inputs;
	if(outputs) delete [] outputs;
	if(uniforms) delete [] uniforms;
	if(addedShaderCode) delete [] addedShaderCode;
	if(outputsUsed) delete [] outputsUsed;
}

CCombinedShader::~CCombinedShader()
{
	if(sourceCode) delete [] sourceCode;
}

void CCombinedShader::addMissingVariable(CShader::sVariable* variables,
										  SHADER_VARIABLES_COUNT_TYPE& variablesCount,
										  CShader::sVariable variable)
{
	for(SHADER_VARIABLES_COUNT_TYPE j = 0;j < variablesCount;j++) {
		if(!strcmp(variable.name, variables[j].name)) {
			if(strcmp(variable.type, variables[j].type)) {
				printf("INCORRECT TYPES IN SHADERS: [%s vs %s]\n", variable.type, variables[j].type);
			}
			if(variable.location != variables[j].location) {
				printf("INCORRECT LOCATION IN SHADERS: [%d vs %d]\n", variable.location, variables[j].location);
			}
			
			return;
		}
	}
	
	variables[variablesCount] = variable;
	variablesCount++;
}

void CCombinedShader::addVariableDeclarationCode(char* code,
												 const char* prefix,
												 CShader::sVariable variable)
{
	if(variable.location != -1) sprintf(code, "%slayout(location = %d) %s %s %s;\n", code, variable.location, prefix, variable.type, variable.name);
	else sprintf(code, "%s%s %s %s;\n", code, prefix, variable.type, variable.name);
}

const char* CCombinedShader::getSourceCode(size_t* length)
{
	if(length) *length = sourceCodeLength;
	return sourceCode;
}
