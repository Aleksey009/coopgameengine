//
//  CShader.cpp
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CShader.h"

CShader::CShader(pugi::xml_node* shaderNode)
{
	pugi::xml_object_range<pugi::xml_named_node_iterator> xmlInputs = shaderNode->children("input");
	loadShaderVariables(&inputs, &xmlInputs);
	
	pugi::xml_object_range<pugi::xml_named_node_iterator> xmlOutputs = shaderNode->children("output");
	loadShaderVariables(&outputs, &xmlOutputs);
	
	pugi::xml_object_range<pugi::xml_named_node_iterator> xmlUniforms = shaderNode->children("uniform");
	loadShaderVariables(&uniforms, &xmlUniforms);
	
	pugi::xml_object_range<pugi::xml_named_node_iterator> xmlExterns = shaderNode->children("extern");
	loadShaderVariables(&externs, &xmlExterns);
	
	const char* value = shaderNode->child("code").text().get();
	codeLength = strlen(value) + 1;
	
	if(codeLength > 0) {
		code = new char[codeLength];
		strcpy(code, value);
		code[codeLength - 1] = 0;
	}
	else {
		code = NULL;
		printf("[%s] HAVENT CODE SHADER BLOCK. ITS TRUE?\n", shaderNode->name());
	}
}

CShader::~CShader()
{
	if(code) delete [] code;
}

SHADER_VARIABLES_COUNT_TYPE CShader::getInputVariablesCount()
{
	return inputs.count;
}

CShader::sVariable CShader::getInputVariable(int id)
{
	return inputs.array[id];
}

SHADER_VARIABLES_COUNT_TYPE CShader::getOutputVariablesCount()
{
	return outputs.count;
}

CShader::sVariable CShader::getOutputVariable(int id)
{
	return outputs.array[id];
}

SHADER_VARIABLES_COUNT_TYPE CShader::getUniformVariablesCount()
{
	return uniforms.count;
}

CShader::sVariable CShader::getUniformVariable(int id)
{
	return uniforms.array[id];
}

SHADER_VARIABLES_COUNT_TYPE CShader::getExternVariablesCount()
{
	return externs.count;
}

CShader::sVariable CShader::getExternVariable(int id)
{
	return externs.array[id];
}

const char* CShader::getCode(size_t* len)
{
	if(len) *len = codeLength;
	return code;
}

void CShader::loadShaderVariables(sVariables* pVariables,
								  pugi::xml_object_range<pugi::xml_named_node_iterator>* iterator)
{
	SHADER_VARIABLES_COUNT_TYPE counter;
	
	counter = 0;
	for(pugi::xml_named_node_iterator i = iterator->begin();i != iterator->end();i++) counter++;
	pVariables->array = new sVariable[counter];
	pVariables->count = counter;
	
	counter = 0;
	for(pugi::xml_named_node_iterator i = iterator->begin();i != iterator->end();i++) {
		loadShaderVariable(&(pVariables->array[counter]), &(*i));
		counter++;
	}
}

void CShader::loadShaderVariable(sVariable* pVariable, pugi::xml_node* node)
{
	pugi::xml_attribute type = node->attribute("type");
	if(type) {
		strcpy(pVariable->type, type.as_string());
	}
	
	pugi::xml_attribute name = node->attribute("name");
	if(name) {
		strcpy(pVariable->name, name.as_string());
	}
	
	pugi::xml_attribute location = node->attribute("location");
	if(location) {
		pVariable->location = location.as_int();
	}
	else {
		pVariable->location = -1;
	}
}