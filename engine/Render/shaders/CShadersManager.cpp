//
//  CShadersManager.cpp
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CShadersManager.h"
#include "CShaderProgram.h"

#include "../../../third_party/pugixml/pugixml.hpp"

#include <string.h>

CShadersManager::CShadersManager(CRenderAbstract* render)
{
	pRender = render;
}

CShadersManager::~CShadersManager()
{

}

CShaderProgram* CShadersManager::loadShaderProgram(const char* name, std::istream& xmlStream)
{
	CShaderProgram* result = NULL;
	
	CLinkedList<shaderHolder*>::iterator i = shaderPrograms.begin();
	while(i != NULL) {
		if(!strcmp(name, i->pData->shadername)) {
			result = i->pData->pShaderProgram;
		}
		
		i = i->pNext;
	}
	
	if(!result) {
		result = new CShaderProgram(xmlStream);
		
		shaderHolder* newHolder = new shaderHolder;
		strcpy(newHolder->shadername, name);
		newHolder->pShaderProgram = result;
		
		shaderPrograms.add(newHolder);
	}
	
	return result;
}

CCombinedShaderProgram* CShadersManager::loadCombinedShaderProgram(CShaderProgram** programs, int count)
{
	CLinkedList<combinedShaderHolder*>::iterator i = combinedShaderPrograms.begin();
	while(i != NULL) {
		if(i->pData->count == count) {
			bool allCorrect = true;
			for(int j = 0;j < i->pData->count;j++) {
				if(i->pData->pShaderPrograms[j] == programs[j]) continue;
				
				allCorrect = false;
				break;
			}
			
			if(allCorrect) {
				return i->pData->pCombinedShaderProgram;
			}
		}
		
		i = i->pNext;
	}
	
	return NULL;
}
