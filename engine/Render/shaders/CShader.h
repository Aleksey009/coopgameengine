//
//  CShader.h
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CShader__
#define __Render__CShader__

#include "../../common/defines.h"

#include "../../../third_party/pugixml/pugixml.hpp"

#include <string.h>

typedef unsigned char SHADER_VARIABLES_COUNT_TYPE;

/** Класс шейдера */
class RENDER_API CShader
{
public:
	/** Структура содержащая данные о переменной шейдера */
	struct sVariable {
		/** Тип переменной шейдера (vec3, mat4, sampler2D, ...) */
		char type[16];
		/** Имя переменной шейдера */
		char name[32];
		/** Локация переменной шейдера (если не задана = -1) */
		int location;
	};
	
	/**
	 Конструктор, использующий данные из XML для создания шейдера
	 @param shaderNode элемент XML содержащий описание шейдера (блоки input, output, uniform, code)
	 */
	CShader(pugi::xml_node* shaderNode);
	virtual ~CShader();
	
	/**
	 Получает количество входящих переменных шейдера
	 @return количество
	 */
	virtual SHADER_VARIABLES_COUNT_TYPE getInputVariablesCount();
	/**
	 Получает данные входящей переменной шейдера
	 @param id индекс переменной
	 @return структура данных переменной
	 */
	virtual sVariable getInputVariable(int id);
	/**
	 Получает количество исходящих переменных шейдера
	 @return количество
	 */
	virtual SHADER_VARIABLES_COUNT_TYPE getOutputVariablesCount();
	/**
	 Получает данные исходящей переменной шейдера
	 @param id индекс переменной
	 @return структура данных переменной
	 */
	virtual sVariable getOutputVariable(int id);
	/**
	 Получает количество переменных униформ шейдера
	 @return количество
	 */
	virtual SHADER_VARIABLES_COUNT_TYPE getUniformVariablesCount();
	/**
	 Получает данные переменной униформы шейдера
	 @param id индекс переменной
	 @return структура данных переменной
	 */
	virtual sVariable getUniformVariable(int id);
	/**
	 Получает количество внешних переменных шейдера
	 @return количество
	 */
	virtual SHADER_VARIABLES_COUNT_TYPE getExternVariablesCount();
	/**
	 Получает данные внешней переменной шейдера
	 @param id индекс переменной
	 @return структура данных переменной
	 */
	virtual sVariable getExternVariable(int id);
	/**
	 Получает код шейдера
	 @param len выходящий параметр - длина кода
	 @return указатель на текст кода
	 */
	virtual const char* getCode(size_t* len);
	
	
protected:
	/** Структура содержащая данные о наборе переменных */
	struct sVariables {
		sVariables() {
			array = NULL;
			count = 0;
		}
		
		~sVariables() {
			if(array) delete [] array;
		}
		
		/** Массив переменных */
		sVariable* array;
		/** Количество переменных в массиве */
		SHADER_VARIABLES_COUNT_TYPE count;
	};
	
	/**
	 Выполняет загрузку переменных шейдера из XML документа в массив переменных
	 @param pVariables массив переменных, в который будет идти загрузка
	 @param iterator итератор переменных XML документа
	 */
	virtual void loadShaderVariables(sVariables* pVariables,
									 pugi::xml_object_range<pugi::xml_named_node_iterator>* iterator);
	/**
	 Выполняет загрузку переменной из XML узла в структуру переменной
	 @param pVariable структура переменной, в которую будет идти загрузка
	 @param node узел XML документа
	 */
	virtual void loadShaderVariable(sVariable* pVariable,
									pugi::xml_node* node);
	
	/** Входящие данные шейдера */
	sVariables inputs;
	/** Исходящие данные шейдера */
	sVariables outputs;
	/** Униформы шейдера */
	sVariables uniforms;
	/** Внешние данные шейдера (переменные получаемые от других шейдеров) */
	sVariables externs;
	/** Код шейдера */
	char* code;
	/** Длина кода шейдера */
	size_t codeLength;
};

#endif /* defined(__Render__CShader__) */
