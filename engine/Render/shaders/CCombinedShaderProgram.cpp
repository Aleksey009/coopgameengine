//
//  CCombinedShaderProgram.cpp
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CCombinedShaderProgram.h"
#include "CCombinedShader.h"
#include "CShaderProgram.h"

CCombinedShaderProgram::CCombinedShaderProgram(CShaderProgram** programs, int count)
{
	pVertexShader = NULL;
	pGeometryShader = NULL;
	pFragmentShader = NULL;
	
	int vCount = 0;
	int gCount = 0;
	int fCount = 0;
	
	for(int i = 0;i < count;i++) {
		if(programs[i]->getVertexShader()) vCount++;
		if(programs[i]->getGeometryShader()) gCount++;
		if(programs[i]->getFragmentShader()) fCount++;
	}
	
	CShader** vShaders = new CShader*[vCount];
	CShader** gShaders = new CShader*[gCount];
	CShader** fShaders = new CShader*[fCount];
	
	int addedVShaders = 0;
	int addedGShaders = 0;
	int addedFShaders = 0;
	for(int i = 0;i < count;i++) {
		if(programs[i]->getVertexShader()) {
			vShaders[addedVShaders++] = programs[i]->getVertexShader();
		}
		if(programs[i]->getGeometryShader()) {
			gShaders[addedGShaders++] = programs[i]->getGeometryShader();
		}
		if(programs[i]->getFragmentShader()) {
			fShaders[addedFShaders++] = programs[i]->getFragmentShader();
		}
	}
	
	if(vCount > 0) pVertexShader = new CCombinedShader(vShaders, vCount);
	if(gCount > 0) pGeometryShader = new CCombinedShader(gShaders, gCount);
	if(fCount > 0) pFragmentShader = new CCombinedShader(fShaders, fCount);
	
	delete [] vShaders;
	delete [] gShaders;
	delete [] fShaders;
}

CCombinedShaderProgram::~CCombinedShaderProgram()
{
	if(pVertexShader) delete pVertexShader;
	if(pGeometryShader) delete pGeometryShader;
	if(pFragmentShader) delete pFragmentShader;
}
