//
//  CCombinedShader.h
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CCombinedShader__
#define __Render__CCombinedShader__

#include "../../common/defines.h"

#include "CShader.h"

/** Класс комбинированного шейдера (собранного из нескольких шейдеров) */
class RENDER_API CCombinedShader
{
public:
	/**
	 Конструктор комбинированного шейдера, собирающий из нескольких шейдеров один
	 @param shaders массив шейдеров
	 @param count количество шейдеров в массиве
	 */
	CCombinedShader(CShader** shaders, int count);
	virtual ~CCombinedShader();
	
	/**
	 Получает текст исходного кода шейдера
	 @param length длина исходного кода - выходной параметр
	 @return исходный код шейдера
	 */
	virtual const char* getSourceCode(size_t* length);
	
protected:
	/**
	 Добавляет переменную в массив переменных, если ее еще нет
	 @param variables массив переменных
	 @param variablesCount количество переменных в массиве
	 @param variable переменная для добавления
	 */
	virtual void addMissingVariable(CShader::sVariable* variables,
									SHADER_VARIABLES_COUNT_TYPE& variablesCount,
									CShader::sVariable variable);
	/**
	 Добавляет объявление переменной в текст исходного кода
	 @param code текст исходного кода, к которому допишется объявление
	 @param prefix префикс (in, out, uniform)
	 @param variable переменная, объявление которой надо добавить
	 */
	virtual void addVariableDeclarationCode(char* code, const char* prefix, CShader::sVariable variable);
	
	/** Текст исходного кода */
	char* sourceCode;
	/** Длина исходного кода */
	size_t sourceCodeLength;
};

#endif /* defined(__Render__CCombinedShader__) */
