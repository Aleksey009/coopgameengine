//
//  CCombinedShaderProgram.h
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CCombinedShaderProgram__
#define __Render__CCombinedShaderProgram__

#include "../../common/defines.h"

class CShaderProgram;
class CCombinedShader;

/** Класс скомбинированных шейдерных программ (составляется из нескольких шейдерных программ в одну) */
class RENDER_API CCombinedShaderProgram
{
public:
	/**
	 Конструктор комбинированной шейдерной программы, собирающий из нескольких шейдерных программ одну
	 @param shaders массив шейдерных программ
	 @param count количество программ в массиве
	 */
	CCombinedShaderProgram(CShaderProgram** programs, int count);
	virtual ~CCombinedShaderProgram();
	
	/**
	 Начинает использование шейдерной программы
	 */
	virtual void use() = 0;
	
	
protected:
	/** Скомбинированный вершинный шейдер */
	CCombinedShader* pVertexShader;
	/** Скомбинированный геометрический шейдер */
	CCombinedShader* pGeometryShader;
	/** Скомбинированный фрагментный шейдер */
	CCombinedShader* pFragmentShader;
};

#endif /* defined(__Render__CCombinedShaderProgram__) */
