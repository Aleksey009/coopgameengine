//
//  CShaderProgram.h
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CShaderProgram__
#define __Render__CShaderProgram__

#include "../../common/defines.h"

#include <istream>

class CShader;

/** Класс шейдерных программ (объединяют несколько шейдеров) */
class RENDER_API CShaderProgram
{
public:
	/**
	 Конструктор, создающий шейдерную программу по данным XML документа
	 @param xmlDocument поток с XML документом
	 */
	CShaderProgram(std::istream& xmlDocument);
	virtual ~CShaderProgram();
	
	/**
	 Получает вершинный шейдер
	 @return вершинный шейдер
	 */
	virtual CShader* getVertexShader();
	/**
	 Получает геометрический шейдер
	 @return геометрический шейдер
	 */
	virtual CShader* getGeometryShader();
	/**
	 Получает фрагментный шейдер
	 @return фрагментный шейдер
	 */
	virtual CShader* getFragmentShader();
	
protected:
	/** Вершинный шейдер */
	CShader* pVertexShader;
	/** Геометрический шейдер */
	CShader* pGeometryShader;
	/** Фрагментный шейдер */
	CShader* pFragmentShader;
};

#endif /* defined(__Render__CShaderProgram__) */
