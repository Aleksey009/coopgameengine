//
//  CShaderProgram.cpp
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CShaderProgram.h"
#include "CShader.h"

#include "../../../third_party/pugixml/pugixml.hpp"

CShaderProgram::CShaderProgram(std::istream& xmlDocument)
{
	pVertexShader = NULL;
	pGeometryShader = NULL;
	pFragmentShader = NULL;
	
	pugi::xml_document doc;
	doc.load(xmlDocument);
	
	pugi::xml_node program = doc.child("program");
	pugi::xml_node shader;
	
	shader = program.child("vertex");
	if(shader) pVertexShader = new CShader(&shader);
	
	shader = program.child("geometry");
	if(shader) pGeometryShader = new CShader(&shader);
	
	shader = program.child("fragment");
	if(shader) pFragmentShader = new CShader(&shader);
}

CShaderProgram::~CShaderProgram()
{
	if(pVertexShader) delete pVertexShader;
	if(pGeometryShader) delete pGeometryShader;
	if(pFragmentShader) delete pFragmentShader;
}

CShader* CShaderProgram::getVertexShader()
{
	return pVertexShader;
}

CShader* CShaderProgram::getGeometryShader()
{
	return pGeometryShader;
}

CShader* CShaderProgram::getFragmentShader()
{
	return pFragmentShader;
}
