//
//  CShadersManager.h
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __CoopGameEngine__CShadersManager__
#define __CoopGameEngine__CShadersManager__

#include "../../common/defines.h"
#include "../../common/CLinkedList.h"

#include <istream>

class CRenderAbstract;
class CShaderProgram;
class CCombinedShaderProgram;

class RENDER_API CShadersManager
{
public:
	CShadersManager(CRenderAbstract* render);
	virtual ~CShadersManager();
	
	/**
	 Загружает шейдерную программу из xml документа
	 @param name название шейдера (уникальный идентификатор, для определения повторяющихся шейдеров)
	 @param xmlStream поток с xml документом
	 @return объект шейдерной программы
	 */
	virtual CShaderProgram* loadShaderProgram(const char* name, std::istream& xmlStream);
	/**
	 Загружает сборную шейдерную программу (состоящую из неопределенного количества шейдерных программ)
	 @param programs массив шейдерных программ
	 @param count количество шейдерных программ в массиве
	 @return объект шейдерной программы
	 */
	virtual CCombinedShaderProgram* loadCombinedShaderProgram(CShaderProgram** programs, int count);
	
protected:
	struct shaderHolder {
		char shadername[32];
		CShaderProgram* pShaderProgram;
	};
	struct combinedShaderHolder {
		CShaderProgram** pShaderPrograms;
		int count;
		CCombinedShaderProgram* pCombinedShaderProgram;
	};
	
	CRenderAbstract* pRender;
	CLinkedList<shaderHolder*> shaderPrograms;
	CLinkedList<combinedShaderHolder*> combinedShaderPrograms;
};

#endif /* defined(__CoopGameEngine__CShadersManager__) */
