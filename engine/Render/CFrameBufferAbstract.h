//
//  CFrameBufferAbstract.h
//  Render
//
//  Created by Михайлов Алексей on 13.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CFrameBufferAbstract__
#define __Render__CFrameBufferAbstract__

#include "../common/defines.h"

class CTexture;
class CRenderBufferAbstract;

/** Базовый класс фрейм буфера */
class RENDER_API CFrameBufferAbstract
{
public:
	CFrameBufferAbstract() {}
	virtual ~CFrameBufferAbstract() {}
	
	/**
	 Устанавливает текстуру в которую будет проводиться рендеринг цвета
	 @param attachmentid номер текстуры содержащей цвет
	 @param texture текстура в которую будет вестись вывод
	 */
	virtual void setColorOutputTexture(int attachmentid, CTexture* texture) = 0;
	/**
	 Устанавливает текстуру в которую будет проводиться рендеринг глубины
	 @param texture текстура в которую будет вестись вывод
	 */
	virtual void setDepthOutputTexture(CTexture* texture) = 0;
	/**
	 Устанавливает текстуру в которую будет проводиться рендеринг трафарета
	 @param texture текстура в которую будет вестись вывод
	 */
	virtual void setStencilOutputTexture(CTexture* texture) = 0;
	/**
	 Устанавливает рендер буфер в который будет проводиться рендеринг глубины
	 @param buffer буфер для вывода
	 */
	virtual void setDepthOutputBuffer(CRenderBufferAbstract* buffer) = 0;
	/**
	 Устанавливает буфер в который будет проводиться рендеринг трафарета
	 @param buffer буфер для вывода
	 */
	virtual void setStencilOutputBuffer(CRenderBufferAbstract* buffer) = 0;
	/**
	 Закрепляет буфер для использования
	 */
	virtual void bind() = 0;
	/**
	 Отменяет использование буфера
	 */
	virtual void unbind() = 0;
};

#endif /* defined(__Render__CFrameBufferAbstract__) */
