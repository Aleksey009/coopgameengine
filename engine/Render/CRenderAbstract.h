//
//  CRenderAbstract.h
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef Render_CRenderAbstract_h
#define Render_CRenderAbstract_h

#include "../common/defines.h"

#include "CRenderBufferAbstract.h"

class CCamera;

class CShadersManager;
class CTextureManager;
class CFontManager;
class CGUIManager;

class IRenderable;
class CRenderable2dText;

class CRenderPhaseAbstract;

class CFrameBufferAbstract;

class CShaderProgram;

enum eRenderFlags
{
	RENDER_FLAG_DEPTH_TEST	= 0x00000001,
	RENDER_FLAG_BLEND		= 0x00000010
};

/** Класс отрисовщик */
class RENDER_API CRenderAbstract
{
public:
	/**
	 Получить экземпляр рендера
	 @return экземпляр рендера (OpenGL)
	 */
	static CRenderAbstract* getInstance();
	
	/**
	  Получить менеджер шейдеров
	 @return менеджер шейдеров
	 */
	virtual CShadersManager* shadersManager() = 0;
	/**
	 Получить менеджер текстур
	 @return менеджер текстур
	 */
	virtual CTextureManager* textureManager() = 0;
	/**
	 Получить менеджер шрифтов
	 @return менеджер шрифтов
	 */
	virtual CFontManager* fontManager() = 0;
	/**
	 Получить менеджер пользовательского интерфейса
	 @return менеджер интерфейса
	 */
	virtual CGUIManager* guiManager() = 0;
	
	/**
	 Проводит инициализацию рендера
	 @param data не используется (NULL)
	 */
	virtual void init(void* data) = 0;
	
	/**
	 Проводит изменение размеров области отрисовки
	 @param width ширина
	 @param height высота
	 */
	virtual void resize(int width, int height) = 0;
	
	/**
	 Начинает отрисовку нового фрейма
	 */
	virtual void startDrawing() = 0;
	/**
	 Заканчивает отрисовку нового фрейма
	 */
	virtual void endDrawing() = 0;
	/**
	 Возвращает состояние отрисовки
	 @return true - отрисовка начата
	 */
	virtual bool isDrawing() = 0;
	
	/** 
	 Изменяет текущую камеру используемую для отрисовки
	 @param camera камера для отрисовки
	*/
	virtual void setCamera(CCamera* camera) = 0;
	/**
	 Получает текущую камеру используемую для отрисовки
	 @return камера для отрисовки
	*/
	virtual CCamera* getCamera() = 0;
	/**
	 Получает текущую матрицу мира (проекция и камера)
	 @return матрица 4x4
	 */
	virtual float* getViewProjectionMatrix() = 0;
	
	/**
	 Проводит отрисовку объекта
	 @param renderable объект, содержащий данные для отрисовки
	 */
	virtual void draw(IRenderable* renderable) = 0;
	/**
	 Проводит отрисовку текста
	 @param text объект, содержащий данные для отрисовки текста
	 */
	virtual void draw(CRenderable2dText* text) = 0;
	
	/**
	 Проводит выполнение набора фаз отрисовки
	 @param phases массив указателей на фазы отрисовки (фазы будут выполняться в том же порядке, что в массиве)
	 @param len длина массива с фазами отрисовки
	 */
	virtual void render(CRenderPhaseAbstract** phases, int len) = 0;
	
	/**
	 Создает новый фрейм буфер
	 @return фрейм буфер
	 */
	virtual CFrameBufferAbstract* createFrameBuffer() = 0;
	/**
	 Создает новый рендер буфер
	 @param width ширина буфера
	 @param height высота буфера
	 @param type тип буфера
	 @return рендер буфер
	 */
	virtual CRenderBufferAbstract* createRenderBuffer(int width, int height, eRenderBufferType type) = 0;
	
	/**
	 Добавляет шейдерную программу в набор для отрисовки
	 @param pShaderProgram указатель на объект шейдерной программы
	 */
	virtual void pushShaderProgram(CShaderProgram* pShaderProgram) = 0;
	/**
	 Убирает последнюю шейдерную программу из набора для отрисовки
	 */
	virtual void popShaderProgram() = 0;
	
	/**
	 Загружает в память видеокарты данные буферов объекта
	 @param renderable объект, содержащий данные для отрисовки
	 */
	virtual void loadBuffers(IRenderable* renderable) = 0;
	/**
	 Выгружает из памяти видеокарты данные буферов объекта
	 @param renderable объект, содержащий данные для отрисовки
	 */
	virtual void unloadBuffers(IRenderable* renderable) = 0;
	
	/**
	 Загружает в память видеокарты данные вершинного буфера объекта
	 @param renderable объект, содержащий данные для отрисовки
	 @param bufferid индекс вершинного буфера
	 */
	virtual void loadVertexBuffer(IRenderable* renderable, unsigned char bufferid) = 0;
	/**
	 Выгружает из памяти видеокарты данные вершинного буфера объекта
	 @param renderable объект, содержащий данные для отрисовки
	 @param bufferid индекс вершинного буфера
	 */
	virtual void unloadVertexBuffer(IRenderable* renderable, unsigned char bufferid) = 0;
	/**
	 Проверяет загружены ли данные вершинного буфера объекта в память видеокарты
	 @param renderable объект, содержащий данные для отрисовки
	 @param bufferid индекс вершинного буфера
	 @return true - если данные загружены
	 */
	virtual bool isVertexBufferLoaded(IRenderable* renderable, unsigned char bufferid) = 0;
	
	/**
	 Загружает в память видеокарты данные буфера индексов объекта
	 @param renderable объект, содержащий данные для отрисовки
	 */
	virtual void loadElementsBuffer(IRenderable* renderable) = 0;
	/**
	 Выгружает из памяти видеокарты данные буфера индексов объекта
	 @param renderable объект, содержащий данные для отрисовки
	 */
	virtual void unloadElementsBuffer(IRenderable* renderable) = 0;
	/**
	 Проверяет загружены ли данные буфера индексов объекта в память видеокарты
	 @param renderable объект, содержащий данные для отрисовки
	 @return true - если данные загружены
	 */
	virtual bool isElementsBufferLoaded(IRenderable* renderable) = 0;
	
	/**
	 Изменяет текущую область рисования, запоминая предыдущую
	 @param x начало по оси x
	 @param y начало по оси y
	 @param width ширина
	 @param height высота
	 */
	virtual void pushViewport(int x, int y, int width, int height) = 0;
	/**
	 Изменяет текущую область рисования основываясь на предыдущей, запоминая предыдущую
	 @param x начало по оси x, начиная от начала предыдущей области
	 @param y начало по оси y, начиная от начала предыдущей области
	 @param width ширина (ограничена размерами предыдущей области)
	 @param height высота (ограничена размерами предыдущей области)
	 */
	virtual void pushViewportLocal(int x, int y, int width, int height) = 0;
	/**
	 Возвращает область рисования из памяти
	 */
	virtual void popViewport() = 0;
	/**
	 Очищяет область рисования
	 */
	virtual void clearViewport() = 0;
	/**
	 Очищяет область рисования с заданным цветом
	 @param r красный
	 @param g зеленый
	 @param b синий
	 @param a прозрачность
	 */
	virtual void clearViewport(float r, float g, float b, float a) = 0;
	/**
	 Получает текущую область рисования
	 @param x начало по оси x
	 @param y начало по оси y
	 @param width ширина
	 @param height высота
	 */
	virtual void getViewport(int* x, int* y, int* width, int* height) = 0;
	/**
	 Получает матрицу координатной системы вьюпорта
	 Если установлен вьюпорт 0,0,640,480 то координатная система будет 0-640 и 0-480 с началом в левом нижнем углу вьюпорта (0,0)
	 Если установлен вьюпорт 200,10,100,100 то координатная система будет 0-100 и 0-100 с началом в левом нижнем углу вьюпорта (200,10)
	 @param dest массив 3х3 в который будет записана матрица
	 */
	virtual void getViewportMatrix2D(float* dest) = 0;
	
	/**
	 Включает определенные флаги из набора eRenderFlags
	 @param flag флаг
	 */
	virtual void enable(eRenderFlags flag) = 0;
	/**
	 Выключает определенные флаги из набора eRenderFlags
	 @param flag флаг
	 */
	virtual void disable(eRenderFlags flag) = 0;
	
private:
	static CRenderAbstract* pInstance;
};

#endif
