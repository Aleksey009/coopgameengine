//
//  CFreeCamera.h
//  WorldEditor
//
//  Created by Михайлов Алексей on 23.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __WorldEditor__CFreeCamera__
#define __WorldEditor__CFreeCamera__

#include "CCamera.h"

#include "../../../third_party/glm/gtx/quaternion.hpp"

class RENDER_API CFreeCamera : public CCamera
{
public:
	CFreeCamera();
	CFreeCamera(glm::mat4 projection, glm::mat4 view);
	CFreeCamera(glm::mat4 projection, glm::vec3 position, glm::vec3 lookAtPosition);
	CFreeCamera(glm::vec3 position, glm::vec3 lookAtPosition);
	
	virtual void rotateCamera(float x, float y);
	
	virtual void setPosition(glm::vec3 position);
	virtual glm::vec3 getPosition();
	
	virtual void setRotation(glm::quat rotation);
	virtual glm::quat getRotation();
	
	virtual void setLookAt(glm::vec3 position);
	
	virtual void moveLocal(glm::vec3 change);
	virtual void moveGlobal(glm::vec3 change);

private:
	void updateDataByViewMatrix();
	void updateViewMatrixByData();
	
	glm::vec3 position;
	glm::quat rotation;
};

#endif /* defined(__WorldEditor__CFreeCamera__) */
