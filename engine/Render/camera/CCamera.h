//
//  CCamera.h
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __CoopGameEngine__CCamera__
#define __CoopGameEngine__CCamera__

#include "../../common/defines.h"

#define GLM_FORCE_RADIANS

#include "../../../third_party/glm/matrix.hpp"

class RENDER_API CCamera
{
public:
	CCamera();
	CCamera(glm::mat4 projection, glm::mat4 view);
	
	virtual ~CCamera();
	
	virtual void setProjectionMatrix(glm::mat4 projection);
	virtual glm::mat4 getProjectionMatrix();
	virtual void setViewMatrix(glm::mat4 view);
	virtual glm::mat4 getViewMatrix();
	
	virtual void setPosition(glm::vec3 position);
	virtual glm::vec3 getPosition();
	
	virtual void setLookAt(glm::vec3 position);
	
protected:
	glm::mat4 fProjection;
	glm::mat4 fView;
};

#endif /* defined(__CoopGameEngine__CCamera__) */
