//
//  CFreeCamera.cpp
//  WorldEditor
//
//  Created by Михайлов Алексей on 23.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CFreeCamera.h"

#include "../../../third_party/glm/gtc/matrix_transform.hpp"

CFreeCamera::CFreeCamera(): CCamera()
{
	updateDataByViewMatrix();
}

CFreeCamera::CFreeCamera(glm::mat4 projection, glm::mat4 view): CCamera(projection, view)
{
	updateDataByViewMatrix();
}

CFreeCamera::CFreeCamera(glm::mat4 projection, glm::vec3 position, glm::vec3 lookAtPosition)
{
	fProjection = projection;
	
	setPosition(position);
	setLookAt(lookAtPosition);
}

CFreeCamera::CFreeCamera(glm::vec3 position, glm::vec3 lookAtPosition)
{
	fProjection = glm::mat4(1.0f);
	
	setPosition(position);
	setLookAt(lookAtPosition);
}

void CFreeCamera::rotateCamera(float x, float y)
{
	glm::quat rot = glm::angleAxis(x, glm::vec3(0, 1.0f, 0)) * rotation;
	rot = glm::angleAxis(y, glm::vec3(1.0f, 0, 0)) * rot;
	
	setRotation(rot);
	
	updateDataByViewMatrix();
}

void CFreeCamera::setPosition(glm::vec3 position)
{
	this->position = position;
	
	updateViewMatrixByData();
}

glm::vec3 CFreeCamera::getPosition()
{
	return position;
}

void CFreeCamera::setRotation(glm::quat rotation)
{
	this->rotation = rotation;
	
	updateViewMatrixByData();
}

glm::quat CFreeCamera::getRotation()
{
	return rotation;
}

void CFreeCamera::setLookAt(glm::vec3 position)
{
	CCamera::setLookAt(position);
	
	updateDataByViewMatrix();
}

void CFreeCamera::updateDataByViewMatrix()
{
	rotation = glm::toQuat(fView);
	
	glm::vec3 p(fView[3][0], fView[3][1], fView[3][2]);
	glm::quat rot = rotation;
	
	rot.x *= -1.f;
	rot.y *= -1.f;
	rot.z *= -1.f;
	
	position = glm::rotate(rot, p);
}

void CFreeCamera::updateViewMatrixByData()
{
	glm::vec3 p = glm::rotate(rotation, position);
	
	fView = glm::translate(glm::mat4(1.0f), p) * glm::toMat4(rotation);
}

void CFreeCamera::moveLocal(glm::vec3 change)
{
	fView[3][0] += change.x;
	fView[3][1] += change.y;
	fView[3][2] += change.z;
	
	updateDataByViewMatrix();
}

void CFreeCamera::moveGlobal(glm::vec3 change)
{
	position += change;
	
	updateViewMatrixByData();
}
