//
//  CCamera.cpp
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CCamera.h"

#include "../../../third_party/glm/gtc/matrix_transform.hpp"

#include <stdio.h>

CCamera::CCamera()
{
	fProjection = glm::mat4(1.0f);
	fView = glm::mat4(1.0f);
}

CCamera::CCamera(glm::mat4 projection, glm::mat4 view): fProjection(projection), fView(view)
{
}

CCamera::~CCamera()
{
	
}

void CCamera::setProjectionMatrix(glm::mat4 projection)
{
	fProjection = projection;
}

glm::mat4 CCamera::getProjectionMatrix()
{
	return fProjection;
}

void CCamera::setViewMatrix(glm::mat4 view)
{
	fView = view;
}

glm::mat4 CCamera::getViewMatrix()
{
	return fView;
}

void CCamera::setPosition(glm::vec3 position)
{
	fView[3][0] = position.x;
	fView[3][1] = position.y;
	fView[3][2] = position.z;
}

glm::vec3 CCamera::getPosition()
{
	return glm::vec3(fView[3][0], fView[3][1], fView[3][2]);
}

void CCamera::setLookAt(glm::vec3 position)
{
	fView = glm::lookAt(getPosition(), position, glm::vec3(0.0f, 1.0f, 0.0f));
}
