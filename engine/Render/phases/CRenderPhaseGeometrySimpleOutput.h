//
//  CRenderPhaseGeometrySimpleOutput.h
//  Render
//
//  Created by Михайлов Алексей on 14.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderPhaseGeometrySimpleOutput__
#define __Render__CRenderPhaseGeometrySimpleOutput__

#include "../CRenderPhaseAbstract.h"

class CRenderPhaseGeometry;
class CShaderProgram;
class CRenderableFrame;

class RENDER_API CRenderPhaseGeometrySimpleOutput: public CRenderPhaseAbstract
{
public:
public:
	CRenderPhaseGeometrySimpleOutput(int width, int height);
	virtual ~CRenderPhaseGeometrySimpleOutput();
	
	/**
	 Устанавливает шейдерную программу, используемую для вывода результата по G-buffer'у
	 @param debugProgram шейдерная программа простого вывода текстуры
	 @param resultProgram шейдерная программа объединения текстур G-buffer'а
	 */
	virtual void setShaderPrograms(CShaderProgram* debugProgram, CShaderProgram* resultProgram);
	
	/**
	 Выставляет фазу рендеринга геометрии, для получения буферов отрисовки
	 @param phase фаза рендеринга геометрии
	 */
	virtual void setRenderPhaseGeometry(CRenderPhaseGeometry* phase);
	
	virtual void onStartDrawing();
	virtual void onDrawing();
	virtual void onEndDrawing();
	
protected:
	/** Шейдерная программа отладки */
	CShaderProgram* pDebugShaderProgram;
	/** Шейдерная программа вывода результата */
	CShaderProgram* pResultShaderProgram;
	/** Ширина буферов */
	int width;
	/** Высота буферов */
	int height;
	/** Указатель на фазу рендеринга геометрии */
	CRenderPhaseGeometry* pPhaseGeometry;
	/** Фрейм отрисовки */
	CRenderableFrame* pFrame;
};

#endif /* defined(__Render__CRenderPhaseGeometrySimpleOutput__) */
