//
//  CRenderPhaseGuiOutput.cpp
//  Render
//
//  Created by Михайлов Алексей on 14.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderPhaseGuiOutput.h"
#include "../CRenderAbstract.h"
#include "../gui/CGUIManager.h"

CRenderPhaseGuiOutput::CRenderPhaseGuiOutput(int width, int height)
{
	
}

CRenderPhaseGuiOutput::~CRenderPhaseGuiOutput()
{
	
}

void CRenderPhaseGuiOutput::onStartDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	render->enable(RENDER_FLAG_BLEND);
}

void CRenderPhaseGuiOutput::onDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	render->guiManager()->draw();
}

void CRenderPhaseGuiOutput::onEndDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	render->disable(RENDER_FLAG_BLEND);
}
