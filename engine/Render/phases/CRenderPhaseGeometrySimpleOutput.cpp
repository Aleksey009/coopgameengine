//
//  CRenderPhaseGeometrySimpleOutput.cpp
//  Render
//
//  Created by Михайлов Алексей on 14.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderPhaseGeometrySimpleOutput.h"
#include "CRenderPhaseGeometry.h"
#include "../shaders/CShaderProgram.h"
#include "../CRenderAbstract.h"
#include "../renderable/CRenderableFrame.h"

CRenderPhaseGeometrySimpleOutput::CRenderPhaseGeometrySimpleOutput(int width, int height)
{
	pDebugShaderProgram = NULL;
	pResultShaderProgram = NULL;
	
	pFrame = new CRenderableFrame();
}

CRenderPhaseGeometrySimpleOutput::~CRenderPhaseGeometrySimpleOutput()
{
	if(pDebugShaderProgram) delete pDebugShaderProgram;
	if(pResultShaderProgram) delete pResultShaderProgram;

	if(pFrame) delete pFrame;
}

void CRenderPhaseGeometrySimpleOutput::setShaderPrograms(CShaderProgram *debugProgram, CShaderProgram *resultProgram)
{
	pDebugShaderProgram = debugProgram;
	pResultShaderProgram = resultProgram;
}

void CRenderPhaseGeometrySimpleOutput::setRenderPhaseGeometry(CRenderPhaseGeometry* phase)
{
	this->pPhaseGeometry = phase;
}

void CRenderPhaseGeometrySimpleOutput::onStartDrawing()
{
	
}

void CRenderPhaseGeometrySimpleOutput::onDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	render->loadBuffers(pFrame);
	
	int x,y,w,h;
	render->getViewport(&x, &y, &w, &h);
	
	if(pDebugShaderProgram) {
		render->pushShaderProgram(pDebugShaderProgram);
		
		render->clearViewport(0.8f, 0.8f, 0.8f, 1.0f);
		
		int width = (w/2) - 1;
		int height = (h/2) - 1;
		
		// albedo
		render->pushViewport(x, y, width, height);
		render->clearViewport();
		
		pFrame->clearTextures();
		pFrame->pushTexture("uTexture", pPhaseGeometry->getAlbedoTexture());
		render->draw(pFrame);
		
		render->popViewport();
		
		// normals
		render->pushViewport(x, y + height + 2, width, height);
		render->clearViewport();
		
		pFrame->clearTextures();
		pFrame->pushTexture("uTexture", pPhaseGeometry->getNormalsTexture());
		render->draw(pFrame);
		
		render->popViewport();
		
		// depth
		render->pushViewport(x + width + 2, y, width, height);
		render->clearViewport();
		
		pFrame->clearTextures();
		pFrame->pushTexture("uTexture", pPhaseGeometry->getDepthTexture());
		render->draw(pFrame);
		
		render->popViewport();
		
		render->popShaderProgram();
		
		x += width + 2;
		y += height + 2;
		w = width;
		h = height;
	}
	
	if(pResultShaderProgram) {
		render->pushShaderProgram(pResultShaderProgram);
		render->pushViewport(x, y, w, h);
		render->clearViewport();
		
		pFrame->clearTextures();
		pFrame->pushTexture("uAlbedoTexture", pPhaseGeometry->getAlbedoTexture());
		pFrame->pushTexture("uNormalsTexture", pPhaseGeometry->getNormalsTexture());
		pFrame->pushTexture("uDepthTexture", pPhaseGeometry->getDepthTexture());
		render->draw(pFrame);
		
		render->popViewport();
		render->popShaderProgram();
	}
}

void CRenderPhaseGeometrySimpleOutput::onEndDrawing()
{
	
}
