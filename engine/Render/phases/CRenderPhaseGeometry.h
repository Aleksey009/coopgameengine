//
//  CRenderPhaseGeometry.h
//  Render
//
//  Created by Михайлов Алексей on 12.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderPhaseGeometry__
#define __Render__CRenderPhaseGeometry__

#include "../CRenderPhaseAbstract.h"

class CScene;
class CTexture;
class CFrameBufferAbstract;
class CShaderProgram;

/** Фаза отрисовки геометрии в g-buffer */
class RENDER_API CRenderPhaseGeometry: public CRenderPhaseAbstract
{
public:
	CRenderPhaseGeometry(int width, int height);
	virtual ~CRenderPhaseGeometry();
	
	/**
	 Устанавливает шейдерную программу, используемую для вывода в G-buffer
	 @param shaderProgram шейдерная программа
	 */
	virtual void setShaderProgram(CShaderProgram* shaderProgram);
	
	/**
	 Выставляет сцену используемую для отрисовки
	 @param scene сцена
	 */
	virtual void setScene(CScene* scene);
	
	/**
	 Получает текстуру с позициями вершин
	 @return указатель на текстуру
	 */
	virtual CTexture* getPositionTexture();
	/**
	 Получает текстуру с цветом геометрии
	 @return указатель на текстуру
	 */
	virtual CTexture* getAlbedoTexture();
	/**
	 Получает текстуру с нормалями
	 @return указатель на текстуру
	 */
	virtual CTexture* getNormalsTexture();
	/**
	 Получает текстуру с глубиной
	 @return указатель на текстуру
	 */
	virtual CTexture* getDepthTexture();
	/**
	 Получает текстуру с трафаретом
	 @return указатель на текстуру
	 */
	virtual CTexture* getStencilTexture();
	
	virtual void onStartDrawing();
	virtual void onDrawing();
	virtual void onEndDrawing();
	
protected:
	/** Шейдерная программа фазы */
	CShaderProgram* pShaderProgram;
	/** Ширина буферов */
	int width;
	/** Высота буферов */
	int height;
	/** Указатель на сцену используемую для отрисовки геометрии */
	CScene* pScene;
	/** Указатель на текстуру содержащую позиции */
	CTexture* pPositionTexture;
	/** Указатель на текстуру содержащую цвет геометрии */
	CTexture* pAlbedoTexture;
	/** Указатель на текстуру содержащую нормали */
	CTexture* pNormalsTexture;
	/** Указатель на текстуру содержащую глубину */
	CTexture* pDepthTexture;
	/** Указатель на текстуру содержащую трафарет */
	CTexture* pStencilTexture;
	/** Фрейм буфер фазы */
	CFrameBufferAbstract* pFrameBuffer;
};

#endif /* defined(__Render__CRenderPhaseGeometry__) */
