//
//  CRenderPhaseGuiOutput.h
//  Render
//
//  Created by Михайлов Алексей on 14.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderPhaseGuiOutput__
#define __Render__CRenderPhaseGuiOutput__

#include "../CRenderPhaseAbstract.h"

class CRenderPhaseGeometry;

class RENDER_API CRenderPhaseGuiOutput: public CRenderPhaseAbstract
{
public:
public:
	CRenderPhaseGuiOutput(int width, int height);
	virtual ~CRenderPhaseGuiOutput();
	
	virtual void onStartDrawing();
	virtual void onDrawing();
	virtual void onEndDrawing();
	
protected:
	/** Ширина буферов */
	int width;
	/** Высота буферов */
	int height;
};


#endif /* defined(__Render__CRenderPhaseGuiOutput__) */
