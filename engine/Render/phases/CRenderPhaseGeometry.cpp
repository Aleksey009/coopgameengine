//
//  CRenderPhaseGeometry.cpp
//  Render
//
//  Created by Михайлов Алексей on 12.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderPhaseGeometry.h"
#include "../CScene.h"
#include "../CRenderAbstract.h"
#include "../texture/CTexture.h"
#include "../CFrameBufferAbstract.h"
#include "../texture/CTextureManager.h"
#include "../shaders/CShadersManager.h"
#include "../shaders/CShaderProgram.h"

#include <fstream>

CRenderPhaseGeometry::CRenderPhaseGeometry(int width, int height)
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	this->pShaderProgram = NULL;
	
	this->width = width;
	this->height = height;
	
	this->pScene = NULL;
	//this->pPositionTexture = render->textureManager()->createTexture(width, height, TEXTURE_FORMAT_RGBA, NULL);
	this->pAlbedoTexture = render->textureManager()->createTexture(width, height, TEXTURE_FORMAT_RGBA, NULL);
	this->pNormalsTexture = render->textureManager()->createTexture(width, height, TEXTURE_FORMAT_RGBA, NULL);
	this->pDepthTexture = render->textureManager()->createTexture(width, height, TEXTURE_FORMAT_DEPTH, NULL);
	//this->pStencilTexture = render->textureManager()->createTexture(width, height, TEXTURE_FORMAT_STENCIL, NULL);
	
	//this->pPositionTexture->incCounter();
	this->pAlbedoTexture->incCounter();
	this->pNormalsTexture->incCounter();
	this->pDepthTexture->incCounter();
	//this->pStencilTexture->incCounter();
	
	this->pFrameBuffer = render->createFrameBuffer();
	
	//this->pFrameBuffer->setColorOutputTexture(0, this->pPositionTexture);
	this->pFrameBuffer->setColorOutputTexture(0, this->pAlbedoTexture);
	this->pFrameBuffer->setColorOutputTexture(1, this->pNormalsTexture);
	this->pFrameBuffer->setDepthOutputTexture(this->pDepthTexture);
	//this->pFrameBuffer->setStencilOutputTexture(this->pStencilTexture);
}

CRenderPhaseGeometry::~CRenderPhaseGeometry()
{
	if(this->pShaderProgram) delete this->pShaderProgram;
	
	if(this->pPositionTexture) delete this->pPositionTexture;
	if(this->pAlbedoTexture) delete this->pAlbedoTexture;
	if(this->pNormalsTexture) delete this->pNormalsTexture;
	if(this->pDepthTexture) delete this->pDepthTexture;
	//if(this->pStencilTexture) delete this->pStencilTexture;

	if(this->pFrameBuffer) delete this->pFrameBuffer;
}

void CRenderPhaseGeometry::setShaderProgram(CShaderProgram* shaderProgram)
{
	pShaderProgram = shaderProgram;
}

void CRenderPhaseGeometry::setScene(CScene* scene)
{
	pScene = scene;
}

CTexture* CRenderPhaseGeometry::getPositionTexture()
{
	return pPositionTexture;
}

CTexture* CRenderPhaseGeometry::getAlbedoTexture()
{
	return pAlbedoTexture;
}

CTexture* CRenderPhaseGeometry::getNormalsTexture()
{
	return pNormalsTexture;
}

CTexture* CRenderPhaseGeometry::getDepthTexture()
{
	return pDepthTexture;
}

CTexture* CRenderPhaseGeometry::getStencilTexture()
{
	return pStencilTexture;
}

void CRenderPhaseGeometry::onStartDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	this->pFrameBuffer->bind();
	
	render->clearViewport();
	render->enable(RENDER_FLAG_DEPTH_TEST);
	render->enable(RENDER_FLAG_BLEND);
	
	if(pShaderProgram) render->pushShaderProgram(pShaderProgram);
}

void CRenderPhaseGeometry::onDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();

	pScene->draw(render);
}

void CRenderPhaseGeometry::onEndDrawing()
{
	CRenderAbstract* render = CRenderAbstract::getInstance();
	
	if(pShaderProgram) render->popShaderProgram();
	
	render->disable(RENDER_FLAG_DEPTH_TEST);
	render->disable(RENDER_FLAG_BLEND);
	
	this->pFrameBuffer->unbind();
}

