//
//  CRenderOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __CoopGameEngine__CRenderOpenGL__
#define __CoopGameEngine__CRenderOpenGL__

#include "../../common/defines.h"

#include "../CRenderAbstract.h"

#include "opengl.h"

#include <map>
#include <stack>
#include <vector>

#define GLM_FORCE_RADIANS

#include "../../../third_party/glm/glm.hpp"

class CTexture;

class RENDER_API CRenderOpenGL : public CRenderAbstract
{
public:
	CRenderOpenGL();
	virtual ~CRenderOpenGL();
	
	virtual CShadersManager* shadersManager();
	virtual CTextureManager* textureManager();
	virtual CFontManager* fontManager();
	virtual CGUIManager* guiManager();
	
	virtual void init(void* data);

	virtual void resize(int width, int height);
	
	virtual void startDrawing();
	virtual void endDrawing();
	virtual bool isDrawing();
	
	virtual void setCamera(CCamera* camera);
	virtual CCamera* getCamera();
	virtual float* getViewProjectionMatrix();
	
	// draw
	virtual void draw(IRenderable* renderable);
	virtual void draw(CRenderable2dText* text);
	
	virtual void render(CRenderPhaseAbstract** phases, int len);
	
	virtual CFrameBufferAbstract* createFrameBuffer();
	virtual CRenderBufferAbstract* createRenderBuffer(int width, int height, eRenderBufferType type);
	
	virtual void pushShaderProgram(CShaderProgram* pShaderProgram);
	virtual void popShaderProgram();
	
	// buffers
	virtual void loadBuffers(IRenderable* renderable);
	virtual void unloadBuffers(IRenderable* renderable);
	
	// vertex buffers
	virtual void loadVertexBuffer(IRenderable* renderable, unsigned char bufferid);
	virtual void unloadVertexBuffer(IRenderable* renderable, unsigned char bufferid);
	virtual bool isVertexBufferLoaded(IRenderable* renderable, unsigned char bufferid);
	
	// elements buffer
	virtual void loadElementsBuffer(IRenderable* renderable);
	virtual void unloadElementsBuffer(IRenderable* renderable);
	virtual bool isElementsBufferLoaded(IRenderable* renderable);
	
	virtual void pushViewport(int x, int y, int width, int height);
	virtual void pushViewportLocal(int x, int y, int width, int height);
	virtual void popViewport();
	virtual void clearViewport();
	virtual void clearViewport(float r, float g, float b, float a);
	virtual void getViewport(int* x, int* y, int* width, int* height);
	virtual void getViewportMatrix2D(float* dest);
	
	virtual void enable(eRenderFlags flag);
	virtual void disable(eRenderFlags flag);
	
private:
	struct sViewport {
		int x;
		int y;
		int width;
		int height;
	};
	
	void loadBuffer(std::map<void*, GLuint> *bufferMap, void* buffer, unsigned long size, GLenum target);
	void unloadBuffer(std::map<void*, GLuint> *bufferMap, void* buffer);
	bool isBufferLoaded(std::map<void*, GLuint> *bufferMap, void* buffer);
	
	CShadersManager* pShadersManager;
	CTextureManager* pTextureManager;
	CFontManager* pFontManager;
	CGUIManager* pGUIManager;
	
	bool drawing;
	CCamera* pCamera;
	glm::mat4 ViewProjection;
	
	std::map<void*, GLuint> *vertexBuffer;
	std::map<void*, GLuint> *elementsBuffer;
	std::map<CTexture*, GLuint> *texturesBuffer;
	
	std::stack<sViewport> viewportStack;
	
	std::vector<CShaderProgram*> usedShaderPrograms;
};

#endif /* defined(__CoopGameEngine__CRenderOpenGL__) */
