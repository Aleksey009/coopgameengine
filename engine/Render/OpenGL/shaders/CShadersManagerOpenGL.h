//
//  CShadersManagerOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CShadersManagerOpenGL__
#define __Render__CShadersManagerOpenGL__

#include "../../shaders/CShadersManager.h"

class CShaderAbstract;

class CShadersManagerOpenGL: public CShadersManager
{
public:
	CShadersManagerOpenGL(CRenderAbstract* render);
	
	virtual CCombinedShaderProgram* loadCombinedShaderProgram(CShaderProgram** programs, int count);
};

#endif /* defined(__Render__CShadersManagerOpenGL__) */
