//
//  CCombinedShaderProgramOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CCombinedShaderProgramOpenGL.h"
#include "../../shaders/CCombinedShader.h"

#include "../opengl.h"

#include <stdio.h>
#include <vector>

CCombinedShaderProgramOpenGL::CCombinedShaderProgramOpenGL(CShaderProgram** programs, int count): CCombinedShaderProgram(programs, count)
{
	programId = 0xFFFFFFFF;
	vertexShaderId = 0xFFFFFFFF;
	geometryShaderId = 0xFFFFFFFF;
	fragmentShaderId = 0xFFFFFFFF;
	
	compileProgram();
}

CCombinedShaderProgramOpenGL::~CCombinedShaderProgramOpenGL()
{
	glDeleteProgram(programId); CHECK_OGL();
	glDeleteShader(vertexShaderId); CHECK_OGL();
	glDeleteShader(geometryShaderId); CHECK_OGL();
	glDeleteShader(fragmentShaderId); CHECK_OGL();
}

void CCombinedShaderProgramOpenGL::use()
{
	glUseProgram(programId);
}

unsigned int CCombinedShaderProgramOpenGL::getProgramId()
{
	return programId;
}

void CCombinedShaderProgramOpenGL::compileProgram()
{
	printf("Compile combined shader program:\n");
	
	programId = glCreateProgram(); CHECK_OGL();
	
	if(pVertexShader) {
		vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
		
		compileShader(vertexShaderId, pVertexShader->getSourceCode(NULL));
	
		glAttachShader(programId, vertexShaderId); CHECK_OGL();
	}
	
	if(pGeometryShader) {
		geometryShaderId = glCreateShader(GL_GEOMETRY_SHADER);
		
		compileShader(geometryShaderId, pGeometryShader->getSourceCode(NULL));
		
		glAttachShader(programId, geometryShaderId); CHECK_OGL();
	}
	
	if(pFragmentShader) {
		fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
		
		compileShader(fragmentShaderId, pFragmentShader->getSourceCode(NULL));
		
		glAttachShader(programId, fragmentShaderId); CHECK_OGL();
	}
	
	glLinkProgram(programId); CHECK_OGL();
	
	GLint Result = GL_FALSE;
    int InfoLogLength;
	
    glGetProgramiv(programId, GL_LINK_STATUS, &Result); CHECK_OGL();
	glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &InfoLogLength); CHECK_OGL();
    
	std::vector<char> ProgramErrorMessage(InfoLogLength);
    glGetProgramInfoLog(programId, InfoLogLength, NULL, &ProgramErrorMessage[0]); CHECK_OGL();
    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);
}

void CCombinedShaderProgramOpenGL::compileShader(unsigned int shaderId, const char* sourceCode)
{
	GLint Result = GL_FALSE;
	int InfoLogLength;
	
	printf("Compile shader [%s]:\n", sourceCode);
	glShaderSource(shaderId, 1, &sourceCode , NULL); CHECK_OGL();
	glCompileShader(shaderId); CHECK_OGL();
	
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &Result); CHECK_OGL();
	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &InfoLogLength); CHECK_OGL();
	std::vector<char> errorMessage(InfoLogLength);
	glGetShaderInfoLog(shaderId, InfoLogLength, NULL, &errorMessage[0]); CHECK_OGL();
	fprintf(stdout, "%s\n", &errorMessage[0]);
}
