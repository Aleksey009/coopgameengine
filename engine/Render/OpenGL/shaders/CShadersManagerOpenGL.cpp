//
//  CShadersManagerOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CShadersManagerOpenGL.h"
#include "CCombinedShaderProgramOpenGL.h"

CShadersManagerOpenGL::CShadersManagerOpenGL(CRenderAbstract* render): CShadersManager(render)
{
	
}

CCombinedShaderProgram* CShadersManagerOpenGL::loadCombinedShaderProgram(CShaderProgram** programs, int count)
{
	CCombinedShaderProgram* result = CShadersManager::loadCombinedShaderProgram(programs, count);
	
	if(!result) {
		result = new CCombinedShaderProgramOpenGL(programs, count);
		
		combinedShaderHolder* newHolder = new combinedShaderHolder;
		newHolder->count = count;
		newHolder->pShaderPrograms = new CShaderProgram*[count];
		for(int i = 0;i < count;i++) newHolder->pShaderPrograms[i] = programs[i];
		newHolder->pCombinedShaderProgram = result;
		
		combinedShaderPrograms.add(newHolder);
	}
	
	return result;
}
