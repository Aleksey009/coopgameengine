//
//  CCombinedShaderProgramOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 21.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CCombinedShaderProgramOpenGL__
#define __Render__CCombinedShaderProgramOpenGL__

#include "../../shaders/CCombinedShaderProgram.h"

class RENDER_API CCombinedShaderProgramOpenGL: public CCombinedShaderProgram
{
public:
	CCombinedShaderProgramOpenGL(CShaderProgram** programs, int count);
	virtual ~CCombinedShaderProgramOpenGL();
	
	virtual void use();
	
	/**
	 Получает идентификатор шейдерной программы
	 @return идентификатор
	 */
	virtual unsigned int getProgramId();
	
protected:
	/**
	 Проводит компиляцию шейдерной программы
	 */
	virtual void compileProgram();
	/**
	 Проводит компиляцию шейдера
	 @param shaderId идентификатор шейдера
	 @param sourceCode исходный код шейдера
	 */
	virtual void compileShader(unsigned int shaderId, const char* sourceCode);
	
	/** Идентификатор программы на видеокарте */
	unsigned int programId;
	/** Идентификатор вершинного шейдера на видеокарте */
	unsigned int vertexShaderId;
	/** Идентификатор геометрического шейдера на видеокарте */
	unsigned int geometryShaderId;
	/** Идентификатор фрагментного шейдера на видеокарте */
	unsigned int fragmentShaderId;
};

#endif /* defined(__Render__CCombinedShaderProgramOpenGL__) */
