//
//  CFrameBufferOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 13.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CFrameBufferOpenGL.h"
#include "CRenderBufferOpenGL.h"
#include "texture/CTextureOpenGL.h"
#include "opengl.h"

#include <stdio.h>

CFrameBufferOpenGL::CFrameBufferOpenGL(): CFrameBufferAbstract()
{
	glGenFramebuffers(1, &framebufferId);
}

CFrameBufferOpenGL::~CFrameBufferOpenGL()
{
	glDeleteFramebuffers(1, &framebufferId);
}

void CFrameBufferOpenGL::setColorOutputTexture(int attachmentid, CTexture* texture)
{
	CTextureOpenGL* togl = static_cast<CTextureOpenGL*>(texture);
	GLuint tid = togl->getId();
	
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId); CHECK_OGL();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachmentid, GL_TEXTURE_2D, tid, 0); CHECK_OGL();
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		printf("framebuffer broken - color texture");
	}
	else {
		usedBuffers.push_back(GL_COLOR_ATTACHMENT0 + attachmentid);
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_OGL();
}

void CFrameBufferOpenGL::setDepthOutputTexture(CTexture* texture)
{
	CTextureOpenGL* togl = static_cast<CTextureOpenGL*>(texture);
	GLuint tid = togl->getId();
	
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId); CHECK_OGL();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tid, 0); CHECK_OGL();
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		printf("framebuffer broken - depth texture\n");
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_OGL();
}

void CFrameBufferOpenGL::setStencilOutputTexture(CTexture* texture)
{
	CTextureOpenGL* togl = static_cast<CTextureOpenGL*>(texture);
	GLuint tid = togl->getId();
	
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId); CHECK_OGL();
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, tid, 0); CHECK_OGL();
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		printf("framebuffer broken - stencil texture\n");
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_OGL();
}

void CFrameBufferOpenGL::setDepthOutputBuffer(CRenderBufferAbstract *buffer)
{
	CRenderBufferOpenGL* bogl = static_cast<CRenderBufferOpenGL*>(buffer);
	GLuint bid = bogl->getId();
	
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, bid); CHECK_OGL();
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		printf("framebuffer broken - depth buffer\n");
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_OGL();
}

void CFrameBufferOpenGL::setStencilOutputBuffer(CRenderBufferAbstract *buffer)
{
	CRenderBufferOpenGL* bogl = static_cast<CRenderBufferOpenGL*>(buffer);
	GLuint bid = bogl->getId();
	
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, bid); CHECK_OGL();
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		printf("framebuffer broken - stencil buffer\n");
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_OGL();
}

void CFrameBufferOpenGL::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferId); CHECK_OGL();
	glDrawBuffers((GLsizei)usedBuffers.size(), (GLenum*)usedBuffers.data()); CHECK_OGL();
}

void CFrameBufferOpenGL::unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0); CHECK_OGL();
	//glDrawBuffer(GL_COLOR_ATTACHMENT0); CHECK_OGL();
}
