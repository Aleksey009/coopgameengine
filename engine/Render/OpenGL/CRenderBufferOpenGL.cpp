//
//  CRenderBufferOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 22.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderBufferOpenGL.h"

#include "opengl.h"

CRenderBufferOpenGL::CRenderBufferOpenGL(int width, int height, eRenderBufferType type)
{
	GLenum format;
	switch(type) {
		case RENDER_BUFFER_DEPTH:
			format = GL_DEPTH_COMPONENT;
			break;
		case RENDER_BUFFER_STENCIL:
			format = GL_STENCIL_BUFFER_BIT;
			break;
	}
	
	glGenRenderbuffers(1, &bufferId);
	
	glBindRenderbuffer(GL_RENDERBUFFER, bufferId); CHECK_OGL();
	glRenderbufferStorage(GL_RENDERBUFFER, format, width, height); CHECK_OGL();
	glBindRenderbuffer(GL_RENDERBUFFER, 0); CHECK_OGL();
}

CRenderBufferOpenGL::~CRenderBufferOpenGL()
{
	glDeleteRenderbuffers(1, &bufferId);
	
}

unsigned int CRenderBufferOpenGL::getId()
{
	return bufferId;
}

void CRenderBufferOpenGL::bind()
{
	glBindRenderbuffer(GL_RENDERBUFFER, bufferId); CHECK_OGL();
}

void CRenderBufferOpenGL::unbind()
{
	glBindRenderbuffer(GL_RENDERBUFFER, 0); CHECK_OGL();
}
