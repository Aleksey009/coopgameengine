//
//  CTextureOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 09.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CTextureOpenGL__
#define __Render__CTextureOpenGL__

#include "../../texture/CTexture.h"

class CTextureOpenGL: public CTexture
{
public:
	CTextureOpenGL(int width, int height, eTextureFormat format, sPixel* pixels);
	virtual ~CTextureOpenGL();
	
	virtual void loadToGPU();
	virtual void unloadFromGPU();
	virtual bool isLoadedIntoGPU();
	
	/**
	 Получает идентификатор текстуры
	 @return идентификатор текстуры в видеопамяти
	 */
	virtual unsigned int getId();
	
	virtual void bind();
	virtual void unbind();
	
private:
	virtual CTexture* createTexture(int width, int height, eTextureFormat format, sPixel* pixels);
	
	unsigned int textureId;
};

#endif /* defined(__Render__CTextureOpenGL__) */
