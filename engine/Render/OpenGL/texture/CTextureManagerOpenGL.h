//
//  CTextureManagerOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 10.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CTextureManagerOpenGL__
#define __Render__CTextureManagerOpenGL__

#include "../../texture/CTextureManager.h"

class CTextureManagerOpenGL: public CTextureManager
{
public:
	CTextureManagerOpenGL();

protected:
	virtual CTexture* createTexture(int width, int height, eTextureFormat format, CTexture::sPixel* pixels);
};

#endif /* defined(__Render__CTextureManagerOpenGL__) */
