//
//  CTextureOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 09.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CTextureOpenGL.h"

#include "../opengl.h"

CTextureOpenGL::CTextureOpenGL(int width, int height, eTextureFormat format, sPixel* pixels): CTexture(width, height, format, pixels)
{
	textureId = 0xFFFFFFFF;
}

CTextureOpenGL::~CTextureOpenGL()
{
	if(isLoadedIntoGPU()) unloadFromGPU();
}

void CTextureOpenGL::loadToGPU()
{
	if(this->pTextureMap) {
		this->pTextureMap->loadToGPU();
		return;
	}
	
	if(isLoadedIntoGPU()) return;
	
	GLenum oformat;
	GLenum iformat;
	switch (format) {
		case TEXTURE_FORMAT_RGBA:
			iformat = GL_RGBA;
			oformat = iformat;
			break;
		case TEXTURE_FORMAT_DEPTH:
			iformat = GL_DEPTH_COMPONENT32;
			oformat = GL_DEPTH_COMPONENT;
			break;
		case TEXTURE_FORMAT_STENCIL:
			iformat = GL_STENCIL_INDEX1;
			oformat = iformat;
			break;
	}
	
	glGenTextures(1, &textureId); CHECK_OGL();
	glBindTexture(GL_TEXTURE_2D, textureId); CHECK_OGL();
	
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_OGL();
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_OGL();
	
	glTexImage2D(GL_TEXTURE_2D, 0, iformat, this->width, this->height, 0, oformat, GL_UNSIGNED_BYTE, this->pixels); CHECK_OGL();
}

void CTextureOpenGL::unloadFromGPU()
{
	if(this->pTextureMap) {
		this->pTextureMap->unloadFromGPU();
		return;
	}
	
	if(!isLoadedIntoGPU()) return;
	
	glDeleteTextures(1, &textureId); CHECK_OGL();
}

bool CTextureOpenGL::isLoadedIntoGPU()
{
	if(this->pTextureMap) {
		return this->pTextureMap->isLoadedIntoGPU();
	}
	
	return (textureId != 0xFFFFFFFF);
}

unsigned int CTextureOpenGL::getId()
{
	return textureId;
}

void CTextureOpenGL::bind()
{
	if(this->pTextureMap) {
		this->pTextureMap->bind();
		return;
	}
	
	glBindTexture(GL_TEXTURE_2D, textureId);
}

void CTextureOpenGL::unbind()
{
	if(this->pTextureMap) {
		this->pTextureMap->unbind();
		return;
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);
}

CTexture* CTextureOpenGL::createTexture(int width, int height, eTextureFormat format, sPixel* pixels)
{
	return new CTextureOpenGL(width, height, format, pixels);
}
