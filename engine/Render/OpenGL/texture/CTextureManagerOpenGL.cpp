//
//  CTextureManagerOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 10.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CTextureManagerOpenGL.h"
#include "CTextureOpenGL.h"

CTextureManagerOpenGL::CTextureManagerOpenGL(): CTextureManager()
{
	
}

CTexture* CTextureManagerOpenGL::createTexture(int width, int height, eTextureFormat format, CTexture::sPixel *pixels)
{
	return new CTextureOpenGL(width, height, format, pixels);
}
