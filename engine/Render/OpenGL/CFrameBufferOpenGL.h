//
//  CFrameBufferOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 13.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CFrameBufferOpenGL__
#define __Render__CFrameBufferOpenGL__

#include "../../common/defines.h"
#include "../CFrameBufferAbstract.h"

#include <vector>

class CRenderBufferAbstract;

/** Класс буфера фрейма */
class RENDER_API CFrameBufferOpenGL: public CFrameBufferAbstract
{
public:
	CFrameBufferOpenGL();
	virtual ~CFrameBufferOpenGL();
	
	virtual void setColorOutputTexture(int attachmentid, CTexture* texture);
	virtual void setDepthOutputTexture(CTexture* texture);
	virtual void setStencilOutputTexture(CTexture* texture);
	virtual void setDepthOutputBuffer(CRenderBufferAbstract* buffer);
	virtual void setStencilOutputBuffer(CRenderBufferAbstract* buffer);
	
	virtual void bind();
	virtual void unbind();
	
protected:
	/** Идентификатор фреймбуфера в видюхе */
	unsigned int framebufferId;
	/** Список буферов для отрисовки */
	std::vector<unsigned int> usedBuffers;
};

#endif /* defined(__Render__CFrameBufferOpenGL__) */
