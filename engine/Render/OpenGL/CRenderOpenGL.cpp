//
//  CRenderOpenGL.cpp
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CRenderOpenGL.h"
#include "./shaders/CShaderOpenGL.h"
#include "./shaders/CCombinedShaderProgramOpenGL.h"
#include "./shaders/CShadersManagerOpenGL.h"
#include "./texture/CTextureManagerOpenGL.h"

#include "../camera/CCamera.h"
#include "../font/CFontManager.h"
#include "../gui/CGUIManager.h"

#include "../renderable/CRenderable2d.h"
#include "../renderable/CRenderable2dText.h"

#include "../CRenderPhaseAbstract.h"

#include "CFrameBufferOpenGL.h"
#include "CRenderBufferOpenGL.h"

#include <stdio.h>
#include <vector>
#include <math.h>
#include <stack>

#include "opengl.h"

#define GLM_FORCE_RADIANS

#include "../../../third_party/glm/gtc/type_ptr.hpp"
#include "../../../third_party/glm/gtc/matrix_transform.hpp"
#include "../../../third_party/glm/gtx/quaternion.hpp"
#include "../../../third_party/freetype-gl/texture-atlas.h"
#include "../../../third_party/freetype-gl/vertex-buffer.h"

void openglErrorCheck(const char* file, int line) {
	GLenum err;
	int ernum = 1;
	
	do {
		err = glGetError();
		if(err == 0) break;
		
		printf("file %s line %d error #%d %d\n", file, line, ernum, err);
		ernum++;
	}
	while(err != 0);
}

glm::mat4 makeMat4(float px, float py, float pz, float rx, float ry, float rz, float rw)
{
	glm::mat4 rot = glm::toMat4(glm::quat(rx, ry, rz, rw));
	glm::mat4 trans = glm::translate(glm::mat4(1.0f), glm::vec3(px, py, pz));
	
	return rot * trans;
}

void debugmat4(char* msg, float* mat)
{
	printf("%s:\n", msg);
	for(int i = 0;i < 4;i++) {
		for(int j = 0;j < 4;j++) printf("%f ", mat[i*4 + j]);
		printf("\n");
	}
}

CRenderOpenGL::CRenderOpenGL()
{
	this->vertexBuffer = new std::map<void*, GLuint>();
	this->elementsBuffer = new std::map<void*, GLuint>();
	this->texturesBuffer = new std::map<CTexture*, GLuint>();
	
	this->pShadersManager = new CShadersManagerOpenGL(this);
	this->pTextureManager = new CTextureManagerOpenGL();
	this->pFontManager = new CFontManager();
	this->pGUIManager = new CGUIManager();
	
	pCamera = NULL;
}

CRenderOpenGL::~CRenderOpenGL()
{
	delete this->vertexBuffer;
	delete this->elementsBuffer;
	delete this->texturesBuffer;
	
	delete this->pShadersManager;
	delete this->pTextureManager;
	delete this->pFontManager;
	delete this->pGUIManager;
}

CShadersManager* CRenderOpenGL::shadersManager()
{
	return pShadersManager;
}

CTextureManager* CRenderOpenGL::textureManager()
{
	return pTextureManager;
}

CFontManager* CRenderOpenGL::fontManager()
{
	return pFontManager;
}

CGUIManager* CRenderOpenGL::guiManager()
{
	return pGUIManager;
}

void CRenderOpenGL::init(void *data)
{
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID); CHECK_OGL();
	glBindVertexArray(VertexArrayID); CHECK_OGL();
	
	glEnable(GL_SCISSOR_TEST); CHECK_OGL();
	glEnable(GL_TEXTURE_2D); CHECK_OGL();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); CHECK_OGL();
	
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	
	this->pGUIManager->init(m_viewport[2], m_viewport[3]);
}

void CRenderOpenGL::resize(int width, int height)
{
	glViewport(0, 0, width, height); CHECK_OGL();
	
	this->pGUIManager->resize(width, height);
}

void CRenderOpenGL::startDrawing()
{
	this->drawing = true;
	
	if(pCamera) ViewProjection = pCamera->getProjectionMatrix() * pCamera->getViewMatrix();
	else ViewProjection = glm::mat4(1.0f);
}

void CRenderOpenGL::endDrawing()
{
	glFlush();
	
	this->drawing = false;
}

bool CRenderOpenGL::isDrawing()
{
	return this->drawing;
}

void CRenderOpenGL::setCamera(CCamera* camera)
{
	this->pCamera = camera;
}

CCamera* CRenderOpenGL::getCamera()
{
	return this->pCamera;
}

float* CRenderOpenGL::getViewProjectionMatrix()
{
	return &(ViewProjection[0][0]);
}

void CRenderOpenGL::draw(IRenderable* renderable)
{
	unsigned char count;
	const char* name;
	eUniformType uniformType;
	void* uniformData;
	unsigned long bufferSize;
	void* bufferData;
	GLuint bufferId;
	GLuint location;
	unsigned char vertexSize;
	unsigned int vertexCount;
	GLuint enabledArrays[10];
	unsigned char enabledArraysSize = 0;
	GLenum drawMode = GL_TRIANGLES;
	bool byElements = false;
	CTexture* texture;
	
	if(!isDrawing()) return;
	
	renderable->onPreDraw(this);
	
	CCombinedShaderProgram* pProgram = pShadersManager->loadCombinedShaderProgram(&(usedShaderPrograms[0]), (int)usedShaderPrograms.size());
	CCombinedShaderProgramOpenGL* pProgramOpenGL = static_cast<CCombinedShaderProgramOpenGL*>(pProgram);
	
	pProgramOpenGL->use();
	GLuint programId = pProgramOpenGL->getProgramId();
	
	// uniforms
	count = renderable->getUniformsCount();
	for(unsigned char i = 0;i < count;i++) {
		name = renderable->getUniformName(i);
		uniformType = renderable->getUniformType(i);
		uniformData = renderable->getUniformData(i);
		
		location = glGetUniformLocation(programId, name); CHECK_OGL();
		
		switch(uniformType) {
			case UNIFORM_TYPE_INT_1:
				glUniform1iv(location, 1, (GLint*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_INT_2:
				glUniform2iv(location, 1, (GLint*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_INT_3:
				glUniform3iv(location, 1, (GLint*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_INT_4:
				glUniform4iv(location, 1, (GLint*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_FLOAT_1:
				glUniform1fv(location, 1, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_FLOAT_2:
				glUniform2fv(location, 1, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_FLOAT_3:
				glUniform3fv(location, 1, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_FLOAT_4:
				glUniform4fv(location, 1, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_MATRIX_2:
				glUniformMatrix2fv(location, 1, GL_FALSE, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_MATRIX_3:
				glUniformMatrix3fv(location, 1, GL_FALSE, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_MATRIX_4:
				glUniformMatrix4fv(location, 1, GL_FALSE, (GLfloat*)uniformData); CHECK_OGL();
				break;
			case UNIFORM_TYPE_UNKNOWN:
				printf("UNKNOWN UNIFORM!\n");
				break;
		}
	}
	
	// textures
	count = renderable->getTexturesCount();
	for (unsigned char i = 0; i < count; i++) {
		name = renderable->getTextureName(i);
		texture = renderable->getTexture(i);
		
		location = glGetUniformLocation(programId, name); CHECK_OGL();
		
		glActiveTexture(GL_TEXTURE0 + i); CHECK_OGL();
		texture->bind();
		glUniform1i(location, i); CHECK_OGL();
	}
		
	// vertex buffers
	count = renderable->getVertexBuffersCount();
	for(unsigned char i = 0;i < count;i++) {
		name = renderable->getVertexBufferName(i);
		bufferSize = renderable->getVertexBufferSize(i);
		bufferData = renderable->getVertexBufferData(i);
		vertexSize = renderable->getVertexBufferDimensions(i);
		
		bufferId = this->vertexBuffer->at(bufferData);
		glBindBuffer(GL_ARRAY_BUFFER, bufferId); CHECK_OGL();
		
		location = glGetAttribLocation(programId, name); CHECK_OGL();
		glEnableVertexAttribArray(location); CHECK_OGL();
		
		glVertexAttribPointer(location,
							  vertexSize,
							  GL_FLOAT,
							  GL_FALSE,
							  0,
							  NULL); CHECK_OGL();
		
		enabledArrays[enabledArraysSize] = location;
		enabledArraysSize++;
	}
	
	// elements buffer
	bufferSize = renderable->getElementsBufferSize();
	bufferData = renderable->getElementsBufferData();
	if(bufferData != NULL) {
		bufferId = this->elementsBuffer->at(bufferData);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId); CHECK_OGL();
		
		byElements = true;
	}

	// draw
	vertexCount = renderable->getVertexCount();
	switch(renderable->getDrawMode()) {
		case DRAW_POINTS:
			drawMode = GL_POINTS;
			break;
		case DRAW_LINES:
			drawMode = GL_LINES;
			break;
		case DRAW_TRIANGLES:
			drawMode = GL_TRIANGLES;
			break;
		default:
			printf("ERROR!\n");
			break;
	}
	
	if(byElements) {
		glDrawElements(drawMode, vertexCount, GL_UNSIGNED_INT, 0); CHECK_OGL();
	}
	else {
		glDrawArrays(drawMode, 0, vertexCount); CHECK_OGL();
	}
	
	
	for(unsigned char i = 0;i < enabledArraysSize;i++) {
		glDisableVertexAttribArray(enabledArrays[i]); CHECK_OGL();
	}
	
	renderable->onPostDraw(this);
	
	glUseProgram(0); CHECK_OGL();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_OGL();
	glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_OGL();
}

void CRenderOpenGL::draw(CRenderable2dText *text)
{
	GLuint location;
	GLuint programId;
	float coordSystem[3][3];
	float transform[3][3];
	
	if(!isDrawing()) return;
	
	text->onPreDraw(this);
	
	CCombinedShaderProgram* pProgram = pShadersManager->loadCombinedShaderProgram(usedShaderPrograms.data(), (int)usedShaderPrograms.size());
	CCombinedShaderProgramOpenGL* pProgramOpenGL = static_cast<CCombinedShaderProgramOpenGL*>(pProgram);
	
	pProgramOpenGL->use();
	programId = pProgramOpenGL->getProgramId();
	
	getViewportMatrix2D(&(coordSystem[0][0]));
	memcpy(transform, text->getTransform(), sizeof(float)*(3*3));
	transform[0][0] = 1;
	transform[1][1] = 1;
	
	texture_atlas_t* atlas = this->pFontManager->getAtlas();
	
	glActiveTexture(GL_TEXTURE0); CHECK_OGL();
	glBindTexture( GL_TEXTURE_2D, atlas->id); CHECK_OGL();
	
	location = glGetUniformLocation(programId, "uTexture2d"); CHECK_OGL();
	glUniform1i(location, 0); CHECK_OGL();
	
	location = glGetUniformLocation(programId, "uCoordSystem"); CHECK_OGL();
	glUniformMatrix3fv(location, 1, GL_FALSE, &(coordSystem[0][0])); CHECK_OGL();
	
	location = glGetUniformLocation(programId, "uTransform"); CHECK_OGL();
	glUniformMatrix3fv(location, 1, GL_FALSE, &(transform[0][0])); CHECK_OGL();
	
	vertex_buffer_render(text->getVertexBuffer(), GL_TRIANGLES); CHECK_OGL();

	
	text->onPostDraw(this);
	
	glUseProgram(0); CHECK_OGL();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_OGL();
	glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_OGL();
	glBindTexture( GL_TEXTURE_2D, 0); CHECK_OGL();
}

void CRenderOpenGL::render(CRenderPhaseAbstract **phases, int len)
{
	startDrawing();
	clearViewport();
	
	for(int i = 0;i < len;i++) {
		CRenderPhaseAbstract* phase = phases[i];
		
		phase->onStartDrawing();
		phase->onDrawing();
		phase->onEndDrawing();
	}
	
	endDrawing();
}

CFrameBufferAbstract* CRenderOpenGL::createFrameBuffer()
{
	return new CFrameBufferOpenGL();
}

CRenderBufferAbstract* CRenderOpenGL::createRenderBuffer(int width, int height, eRenderBufferType type)
{
	return new CRenderBufferOpenGL(width, height, type);
}

void CRenderOpenGL::pushShaderProgram(CShaderProgram* pShaderProgram)
{
	usedShaderPrograms.push_back(pShaderProgram);
}

void CRenderOpenGL::popShaderProgram()
{
	usedShaderPrograms.pop_back();
}

void CRenderOpenGL::loadBuffers(IRenderable* renderable)
{
	// vertex buffers
	unsigned char count = renderable->getVertexBuffersCount();
	for(unsigned char i = 0;i < count;i++) loadVertexBuffer(renderable, i);
	// elements buffers
	loadElementsBuffer(renderable);
}

void CRenderOpenGL::unloadBuffers(IRenderable* renderable)
{
	// vertex buffers
	unsigned char count = renderable->getVertexBuffersCount();
	for(unsigned char i = 0;i < count;i++) unloadVertexBuffer(renderable, i);
	// elements buffers
	unloadElementsBuffer(renderable);
}

void CRenderOpenGL::loadVertexBuffer(IRenderable* renderable, unsigned char bufferid)
{
	void* buffer = renderable->getVertexBufferData(bufferid);
	unsigned long size = renderable->getVertexBufferSize(bufferid);
	
	if(buffer) loadBuffer(this->vertexBuffer, buffer, size, GL_ARRAY_BUFFER);
}

void CRenderOpenGL::unloadVertexBuffer(IRenderable* renderable, unsigned char bufferid)
{
	void* buffer = renderable->getVertexBufferData(bufferid);
	
	if(buffer) unloadBuffer(this->vertexBuffer, buffer);
}

bool CRenderOpenGL::isVertexBufferLoaded(IRenderable* renderable, unsigned char bufferid)
{
	void* buffer = renderable->getVertexBufferData(bufferid);
	
	if(buffer) return isBufferLoaded(this->vertexBuffer, buffer);
	else return false;
}

void CRenderOpenGL::loadElementsBuffer(IRenderable* renderable)
{
	void* buffer = renderable->getElementsBufferData();
	unsigned long size = renderable->getElementsBufferSize();
	
	if(buffer) loadBuffer(this->elementsBuffer, buffer, size, GL_ELEMENT_ARRAY_BUFFER);
}

void CRenderOpenGL::unloadElementsBuffer(IRenderable* renderable)
{
	void* buffer = renderable->getElementsBufferData();
	
	if(buffer) unloadBuffer(this->elementsBuffer, buffer);
}

bool CRenderOpenGL::isElementsBufferLoaded(IRenderable* renderable)
{
	void* buffer = renderable->getElementsBufferData();
	
	if(buffer) return isBufferLoaded(this->elementsBuffer, buffer);
	else return false;
}

void CRenderOpenGL::loadBuffer(std::map<void*, GLuint> *bufferMap, void* buffer, unsigned long size, GLenum target)
{
	GLuint bufferId;
	
	if(isBufferLoaded(bufferMap, buffer)) return;
	
	glGenBuffers(1, &bufferId); CHECK_OGL();
	glBindBuffer(target, bufferId); CHECK_OGL();
	glBufferData(target, size, buffer, GL_STATIC_DRAW); CHECK_OGL();
	
	bufferMap->insert(std::pair<void*, GLuint>(buffer, bufferId));
}

void CRenderOpenGL::unloadBuffer(std::map<void*, GLuint> *bufferMap, void* buffer)
{
	GLuint bufferId;

	if(!isBufferLoaded(bufferMap, buffer)) return;
	
	bufferId = bufferMap->at(buffer);
	
	glDeleteBuffers(1, &bufferId); CHECK_OGL();
	
	bufferMap->erase(buffer);
}

bool CRenderOpenGL::isBufferLoaded(std::map<void*, GLuint> *bufferMap, void* buffer)
{
	return bufferMap->find(buffer) != bufferMap->end();
}

void CRenderOpenGL::pushViewport(int x, int y, int width, int height)
{
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	
	viewportStack.push({m_viewport[0], m_viewport[1], m_viewport[2], m_viewport[3]});
	
	glViewport(x, y, width, height);
	glScissor(x, y, width, height);
}

void CRenderOpenGL::pushViewportLocal(int x, int y, int width, int height)
{
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	
	viewportStack.push({m_viewport[0], m_viewport[1], m_viewport[2], m_viewport[3]});
	
	int localx = m_viewport[0] + x;
	int localy = m_viewport[1] + y;
	int localwidth = (m_viewport[2] - x);
	if(localwidth > width) localwidth = width;
	int localheight = (m_viewport[3] - y);
	if(localheight > height) localheight = height;
	
	glViewport(localx, localy, localwidth, localheight);
	glScissor(localx, localy, localwidth, localheight);
}

void CRenderOpenGL::popViewport()
{
	sViewport vp = viewportStack.top();
	viewportStack.pop();
	
	glViewport(vp.x, vp.y, vp.width, vp.height);
	glScissor(vp.x, vp.y, vp.width, vp.height);
}

void CRenderOpenGL::getViewport(int* x, int* y, int* width, int* height)
{
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	
	*x = m_viewport[0];
	*y = m_viewport[1];
	*width = m_viewport[2];
	*height = m_viewport[3];
}

void CRenderOpenGL::clearViewport()
{
	clearViewport(0.0f, 0.0f, 0.0f, 1.0f);
}

void CRenderOpenGL::clearViewport(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a); CHECK_OGL();
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); CHECK_OGL();
}

void CRenderOpenGL::getViewportMatrix2D(float* dest)
{
	GLint m_viewport[4];
	glGetIntegerv( GL_VIEWPORT, m_viewport );
	
	dest[0*3 + 0] = 2.0f/m_viewport[2];
	dest[0*3 + 1] = 0.0f;
	dest[0*3 + 2] = 0.0f;
	dest[1*3 + 0] = 0.0f;
	dest[1*3 + 1] = 2.0f/m_viewport[3];
	dest[1*3 + 2] = 0.0f;
	dest[2*3 + 0] = -1.0f;
	dest[2*3 + 1] = -1.0f;
	dest[2*3 + 2] = 1.0f;
}

void CRenderOpenGL::enable(eRenderFlags flag)
{
	switch(flag) {
		case RENDER_FLAG_DEPTH_TEST:
			glEnable(GL_DEPTH_TEST);
			break;
		case RENDER_FLAG_BLEND:
			glEnable(GL_BLEND);
			break;
	}
	
	CHECK_OGL();
}

void CRenderOpenGL::disable(eRenderFlags flag)
{
	switch(flag) {
		case RENDER_FLAG_DEPTH_TEST:
			glDisable(GL_DEPTH_TEST);
			break;
		case RENDER_FLAG_BLEND:
			glDisable(GL_BLEND);
			break;
	}
	
	CHECK_OGL();
}
