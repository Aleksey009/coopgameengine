//
//  CRenderBufferOpenGL.h
//  Render
//
//  Created by Михайлов Алексей on 22.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CRenderBufferOpenGL__
#define __Render__CRenderBufferOpenGL__

#include "../../common/defines.h"

#include "../CRenderBufferAbstract.h"

class RENDER_API CRenderBufferOpenGL: public CRenderBufferAbstract
{
public:
	CRenderBufferOpenGL(int width, int height, eRenderBufferType type);
	virtual ~CRenderBufferOpenGL();
	
	/**
	 Получает идентификатор буфера на видеокарте
	 @return идентификатор
	 */
	virtual unsigned int getId();
	
	virtual void bind();
	virtual void unbind();
	
protected:
	/** Идентификатор буфера */
	unsigned int bufferId;
};

#endif /* defined(__Render__CRenderBufferOpenGL__) */
