//
//  opengl.h
//  Render
//
//  Created by Михайлов Алексей on 20.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __CoopGameEngine__opengl__
#define __CoopGameEngine__opengl__

#define CHECK_OGL() openglErrorCheck(__FILE__, __LINE__)

#ifdef _WIN32

#include "../../../third_party/gl/glew.h"

#endif

#ifdef __APPLE__

#include <OpenGL/gl3.h>

#endif

#ifdef __linux__

#include <GL/glew.h>

#endif

void openglErrorCheck(const char* file, int line);

#endif /* defined(__CoopGameEngine__opengl__) */
