//
//  CRenderBufferAbstract.h
//  Render
//
//  Created by Михайлов Алексей on 22.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef Render_CRenderBufferAbstract_h
#define Render_CRenderBufferAbstract_h

#include "../common/defines.h"

enum eRenderBufferType {
	RENDER_BUFFER_DEPTH,
	RENDER_BUFFER_STENCIL
};

/** Базовый класс рендер буфера */
class RENDER_API CRenderBufferAbstract
{
public:
	virtual ~CRenderBufferAbstract() {}
	
	/**
	 Закрепляет буфер для использования
	 */
	virtual void bind() = 0;
	/**
	 Отменяет использование буфера
	 */
	virtual void unbind() = 0;
};

#endif
