//
//  CScene.h
//  Render
//
//  Created by Михайлов Алексей on 12.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Render__CScene__
#define __Render__CScene__

#include "../common/defines.h"
#include "../common/CLinkedList.h"

class IRenderable;
class CRenderAbstract;

/** 
 Класс сцены, занимается следующими задачами:
	хранит весь список 3д объектов и 3д эффектов
	хранит активную камеру
	решает что должно быть отображено в кадре, а что нет
	решает какие данные нужно загрузить в видео память, а какие выгрузить
	подает на отрисовку все необходимые объекты и эффекты
 */
class RENDER_API CScene
{
public:
	CScene();
	virtual ~CScene();
	
	/**
	 Добавляет объект в список
	 @param object объект, который нужно добавить
	 */
	virtual void addObject(IRenderable* object);
	/**
	 Удаляет объект из списка
	 @param object объект, который нужно удалить
	 */
	virtual void removeObject(IRenderable* object);
	
	/**
	 Подает на отрисовку все видимые объекты
	 @param render объект рендер, который будет отрисовывать все
	 */
	virtual void draw(CRenderAbstract* render);
	
protected:
	/** Список объектов сцены */
	CLinkedList<IRenderable*> objects;
};

#endif /* defined(__Render__CScene__) */
