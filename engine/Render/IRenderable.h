//
//  IRenderable.h
//  Render
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef Render_IRenderable_h
#define Render_IRenderable_h

#include "../common/defines.h"

#include "CRenderAbstract.h"

#include <stddef.h>

/** Тип униформы */
enum eUniformType {
	/** Неизвестный тип униформы */
	UNIFORM_TYPE_UNKNOWN,
	/** Тип униформы - целое число */
	UNIFORM_TYPE_INT_1,
	/** Тип униформы - 2 целых числа (массив) */
	UNIFORM_TYPE_INT_2,
	/** Тип униформы - 3 целых числа (массив) */
	UNIFORM_TYPE_INT_3,
	/** Тип униформы - 4 целых числа (массив) */
	UNIFORM_TYPE_INT_4,
	/** Тип униформы - вещественное число */
	UNIFORM_TYPE_FLOAT_1,
	/** Тип униформы - 2 вещественных числа (массив) */
	UNIFORM_TYPE_FLOAT_2,
	/** Тип униформы - 3 вещественных числа (массив) */
	UNIFORM_TYPE_FLOAT_3,
	/** Тип униформы - 4 вещественных числа (массив) */
	UNIFORM_TYPE_FLOAT_4,
	/** Тип униформы - матрица вещественных чисел 2х2 */
	UNIFORM_TYPE_MATRIX_2,
	/** Тип униформы - матрица вещественных чисел 3х3 */
	UNIFORM_TYPE_MATRIX_3,
	/** Тип униформы - матрица вещественных чисел 4х4 */
	UNIFORM_TYPE_MATRIX_4
};

/** Режим отрисовки */
enum eDrawMode {
	/** Режим отрисовки - неопределен */
	DRAW_UNKNOWN,
	/** Режим отрисовки - точки */
	DRAW_POINTS,
	/** Режим отрисовки - линии */
	DRAW_LINES,
	/** Режим отрисовки - треугольники */
	DRAW_TRIANGLES
};

class CTexture;

/** Интерфейс рисуемого объекта */
class RENDER_API IRenderable
{
public:
	/**
	 Событие вызываемое до начала отрисовки объекта
	 В данном событии добавляют шейдеры для отрисовки
	 @param render отрисовщик
	 */
	virtual void onPreDraw(CRenderAbstract* render) {}
	/**
	 Событие вызываемое после окончания отрисовки объекта
	 В данном событии убирают шейдеры для отрисовки
	 @param render отрисовщик
	 */
	virtual void onPostDraw(CRenderAbstract* render) {}

	/**
	 Получает количество текстур
	 @return количество
	*/
	virtual unsigned char	getTexturesCount() { return 0; }
	/**
	 Получает название текстуры в шейдерной программе
	 @param textureid идентификатор текстуры
	 @return название
	*/
	virtual const char*		getTextureName(unsigned char textureid) { return NULL; }
	/**
	 Получает текстуру
	 @param textureid идентификатор текстуры
	 @return текстура
	*/
	virtual CTexture*		getTexture(unsigned char textureid) {return NULL;}

	/**
	 Получает количество униформ
	 @return количество
	*/
	virtual unsigned char	getUniformsCount() {return 0;}
	/**
	 Получает название униформы в шейдерной программе
	 @param uniformid идентификатор униформы
	 @return название
	*/
	virtual const char*		getUniformName(unsigned char uniformid) { return NULL; }
	/**
	 Получает тип униформы в шейдерной программе
	 @param uniformid идентификатор униформы
	 @return тип
	*/
	virtual eUniformType	getUniformType(unsigned char uniformid) { return UNIFORM_TYPE_UNKNOWN; }
	/**
	 Получает указатель на данные униформы
	 @param uniformid идентификатор униформы
	 @return указатель
	*/
	virtual void*			getUniformData(unsigned char uniformid) {return NULL;}

	/**
	 Получает количество буферов вершин
	 @return количество
	*/
	virtual unsigned char	getVertexBuffersCount() { return 0; }
	/**
	 Получает название буфера вершин в шейдерной программе
	 @param bufferid идентификатор буфера
	 @return название
	*/
	virtual const char*		getVertexBufferName(unsigned char bufferid) { return NULL; }
	/**
	 Получает размер буфера вершин
	 @param bufferid идентификатор буфера
	 @return размер в байтах
	*/
	virtual unsigned long	getVertexBufferSize(unsigned char bufferid) { return 0; }
	/**
	 Получает указатель на данные буфера вершин
	 @param bufferid идентификатор буфера
	 @return указатель
	*/
	virtual void*			getVertexBufferData(unsigned char bufferid) { return NULL; }
	/**
	 Получает количество измерений буфера вершин
	 @param bufferid идентификатор буфера
	 @return количество измерений (1 - x , 2 - x,y , 3 - x,y,z , ...)
	*/
	virtual unsigned char	getVertexBufferDimensions(unsigned char bufferid) {return 0;}
	
	/** 
	 Получает количество вершин
	 @return количество вершин
	*/
	virtual unsigned int	getVertexCount() {return 0;}
	
	/**
	 Получает размер буфера индексов
	 @return размер в байтах
	*/
	virtual unsigned long	getElementsBufferSize() { return 0; }
	/**
	 Получает указатель на данные буфера индексов
	 @return указатель
	*/
	virtual void*			getElementsBufferData() {return NULL;}
	
	/**
	 Получает режим отрисовки объекта
	 @return режим отрисовки
	*/
	virtual eDrawMode		getDrawMode() {return DRAW_UNKNOWN;};
};

#endif
