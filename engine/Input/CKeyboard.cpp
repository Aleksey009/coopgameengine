//
//  CKeyboard.cpp
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#define _CRT_SECURE_NO_WARNINGS

#include "CKeyboard.h"

#include "./events/sKeyboardListenerEvent.h"

#include "../../third_party/sdl/SDL.h"

#include <string.h>
#include <wchar.h>
#include <locale>

CKeyboard::CKeyboard()
{
	int num;
	const Uint8* keys_state = SDL_GetKeyboardState(&num);

	memset(&state, 0, sizeof(sKeyboardState));

	for (int i = 0; i < num; i++) state.keys[i] = (keys_state[i] != 0);
	
#ifndef _WIN32
	std::locale::global(std::locale("ru_RU.UTF-8"));
#else
	setlocale(LC_ALL, "russian");
#endif
}

void CKeyboard::handleEvent(SDL_Event* e)
{
	wchar_t ws;
	int key = e->key.keysym.scancode;
	
	switch (e->type) {
		case SDL_KEYDOWN:
			state.keys[key] = true;
			notifyKeyDown(key);
			break;
			
		case SDL_KEYUP:
			state.keys[key] = false;
			notifyKeyUp(key);
			break;
			
		case SDL_TEXTINPUT:
			mbtowc(&ws, e->text.text, 2);
			notifyTextInput(ws);
			break;
			
		case SDL_TEXTEDITING:
			printf("editing: [%s] %d %d\n", e->edit.text, e->edit.start, e->edit.length);
			break;
	}
}

sKeyboardState CKeyboard::getState()
{
	return state;
}

void CKeyboard::startCaptureTextInput()
{
	SDL_StartTextInput();
}

void CKeyboard::stopCaptureTextInput()
{
	SDL_StopTextInput();
}

void CKeyboard::notify(IKeyboardListener* listener, void* info)
{
	sKeyboardListenerEvent* e = (sKeyboardListenerEvent*)info;

	switch (e->eventType) {
		case KEYBOARD_BUTTON_DOWN:
			listener->onKeyboardButtonDown(state, e->key);
			break;
		
		case KEYBOARD_BUTTON_UP:
			listener->onKeyboardButtonUp(state, e->key);
			break;
			
		case KEYBOARD_TEXT_INPUT:
			listener->onKeyboardTextInput(state, e->character);
			break;
	}
}

void CKeyboard::notifyKeyDown(unsigned short key)
{
	sKeyboardListenerEvent e;
	e.eventType = KEYBOARD_BUTTON_DOWN;
	e.key = key;

	notifyAll(&e);
}

void CKeyboard::notifyKeyUp(unsigned short key)
{
	sKeyboardListenerEvent e;
	e.eventType = KEYBOARD_BUTTON_UP;
	e.key = key;

	notifyAll(&e);
}

void CKeyboard::notifyTextInput(wchar_t character)
{
	sKeyboardListenerEvent e;
	e.eventType = KEYBOARD_TEXT_INPUT;
	e.character = character;
	
	notifyAll(&e);
}
