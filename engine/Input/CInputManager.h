//
//  CInputManager.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__CInputManager__
#define __Input__CInputManager__

#include "../common/CSingleton.h"

#include "CKeyboard.h"
#include "CMouse.h"

class INPUT_API CInputManager : public CSingleton<CInputManager>
{
	friend class CSingleton<CInputManager>;

public:
	void init(SDL_Window* window);
	
	void handleEvent(SDL_Event* e);

	void windowRect(int* x, int* y, int* width, int* height);
	
	CKeyboard* getKeyboard();
	CMouse* getMouse();

private:
	SDL_Window* pWindow;
	
	CKeyboard* pKeyboard;
	CMouse* pMouse;
};

#endif /* defined(__Input__CInputManager__) */
