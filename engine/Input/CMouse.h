//
//  CMouse.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__CMouse__
#define __Input__CMouse__

#include "../common/CSpeaker.h"
#include "../common/defines.h"

#include "./interfaces/IMouseListener.h"

#include "../../third_party/sdl/SDL_events.h"

class INPUT_API CMouse : public CSpeaker<IMouseListener>
{
public:
	CMouse(SDL_Window* window);

	void handleEvent(SDL_Event* e);
	
	void setPosition(int x, int y);
	void setCursorVisibility(bool visibility);
	sMouseState getState();

private:
	void notify(IMouseListener* listener, void* info);

protected:
	void notifyButtonDown(eMouseButton button);
	void notifyButtonUp(eMouseButton button);
	void notifyMove();
	void notifyWheelScroll();

	SDL_Window* pWindow;
	
	sMouseState state;
};

#endif /* defined(__Input__CMouse__) */
