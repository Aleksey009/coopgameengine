//
//  CMouse.cpp
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CMouse.h"

#include "./events/sMouseListenerEvent.h"

#include <string.h>

CMouse::CMouse(SDL_Window* window)
{
	memset(&state, 0, sizeof(sMouseState));

	int x, y;
	Uint32 buttons = SDL_GetMouseState(&x, &y);

	state.x = x;
	state.y = y;
	state.button_left = (buttons & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;
	state.button_middle = (buttons & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
	state.button_right = (buttons & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;

	pWindow = window;
}

void CMouse::handleEvent(SDL_Event* e)
{
	bool pressed;

	switch (e->type) {
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		switch (e->button.button) {
		case SDL_BUTTON_LEFT:
			pressed = (e->type == SDL_MOUSEBUTTONDOWN);

			state.button_left = pressed;
			if (pressed) notifyButtonDown(MOUSE_BUTTON_LEFT);
			else notifyButtonUp(MOUSE_BUTTON_LEFT);
			break;
		case SDL_BUTTON_MIDDLE:
			pressed = (e->type == SDL_MOUSEBUTTONDOWN);

			state.button_middle = pressed;
			if (pressed) notifyButtonDown(MOUSE_BUTTON_MIDDLE);
			else notifyButtonUp(MOUSE_BUTTON_MIDDLE);
			break;
		case SDL_BUTTON_RIGHT:
			pressed = (e->type == SDL_MOUSEBUTTONDOWN);

			state.button_right = pressed;
			if (pressed) notifyButtonDown(MOUSE_BUTTON_RIGHT);
			else notifyButtonUp(MOUSE_BUTTON_RIGHT);
			break;
		}
		break;

	case SDL_MOUSEMOTION:
		int w,h;
		SDL_GetWindowSize(pWindow, &w, &h);
		state.x = e->motion.x;
		state.y = h - e->motion.y;
		state.xrel = e->motion.xrel;
		state.yrel = -e->motion.yrel;
		notifyMove();
		break;

	case SDL_MOUSEWHEEL:
		state.wheel = e->wheel.y;
		notifyWheelScroll();
		break;
	}
}

void CMouse::setPosition(int x, int y)
{	
	SDL_WarpMouseInWindow(pWindow, x, y);
}

void CMouse::setCursorVisibility(bool visibility)
{
	SDL_ShowCursor(visibility);
}

sMouseState CMouse::getState()
{
	return state;
}

void CMouse::notify(IMouseListener* listener, void* info)
{
	sMouseListenerEvent* e = (sMouseListenerEvent*)info;

	switch (e->eventType) {
		case MOUSE_BUTTON_DOWN: 
			listener->onMouseButtonDown(state, e->button);
			break;
		case MOUSE_BUTTON_UP: 
			listener->onMouseButtonUp(state, e->button);
			break;
		case MOUSE_MOVE:
			listener->onMouseMove(state);
			break;
		case MOUSE_WHEEL_SCROLL:
			listener->onMouseWheelScroll(state);
			break;
	}
}

void CMouse::notifyButtonDown(eMouseButton button)
{
	sMouseListenerEvent e;
	e.eventType = MOUSE_BUTTON_DOWN;
	e.button = button;

	notifyAll(&e);
}

void CMouse::notifyButtonUp(eMouseButton button)
{
	sMouseListenerEvent e;
	e.eventType = MOUSE_BUTTON_UP;
	e.button = button;

	notifyAll(&e);
}

void CMouse::notifyMove()
{
	sMouseListenerEvent e;
	e.eventType = MOUSE_MOVE;

	notifyAll(&e);
}

void CMouse::notifyWheelScroll()
{
	sMouseListenerEvent e;
	e.eventType = MOUSE_WHEEL_SCROLL;

	notifyAll(&e);
}