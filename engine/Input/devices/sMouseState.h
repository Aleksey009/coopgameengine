//
//  sMouseState.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__sMouseState__
#define __Input__sMouseState__

enum eMouseButton
{
	MOUSE_BUTTON_LEFT,
	MOUSE_BUTTON_RIGHT,
	MOUSE_BUTTON_MIDDLE,
};

struct sMouseState
{
	// buttons
	bool button_left;
	bool button_right;
	bool button_middle;
	// absolute
	int x;
	int y;
	// relative
	int xrel;
	int yrel;
	// wheel
	int wheel;
};

#endif /* defined(__Input__sMouseState__) */
