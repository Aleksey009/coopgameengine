//
//  sKeyboardState.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__sKeyboardState__
#define __Input__sKeyboardState__

#include "../../../third_party/sdl/SDL_keycode.h"

struct sKeyboardState
{
	bool keys[SDL_NUM_SCANCODES];
};

#endif /* defined(__Input__sKeyboardState__) */
