//
//  sMouseListenerEvent.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__sMouseListenerEvent__
#define __Input__sMouseListenerEvent__

#include "../devices/sMouseState.h"

enum {
	MOUSE_BUTTON_DOWN,
	MOUSE_BUTTON_UP,
	MOUSE_MOVE,
	MOUSE_WHEEL_SCROLL
};

struct sMouseListenerEvent
{
	int eventType;
	eMouseButton button;
};

#endif /* defined(__Input__sMouseListenerEvent__) */
