//
//  sKeyboardListenerEvent.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__sKeyboardListenerEvent__
#define __Input__sKeyboardListenerEvent__

#include <string.h>

enum {
	KEYBOARD_BUTTON_DOWN,
	KEYBOARD_BUTTON_UP,
	KEYBOARD_TEXT_INPUT
};

struct sKeyboardListenerEvent
{
	int eventType;
	unsigned short key;
	wchar_t character;
};

#endif /* defined(__Input__sKeyboardListenerEvent__) */
