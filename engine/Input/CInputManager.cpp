//
//  CInputManager.cpp
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CInputManager.h"

#include "../../third_party/sdl/SDL.h"

void CInputManager::init(SDL_Window* window)
{	
	pWindow = window;
}

void CInputManager::handleEvent(SDL_Event* e)
{
	switch (e->type) {
		case SDL_KEYDOWN:
		case SDL_KEYUP:
		case SDL_TEXTINPUT:
		case SDL_TEXTEDITING:
			if (pKeyboard) pKeyboard->handleEvent(e);
			break;

		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEMOTION:
		case SDL_MOUSEWHEEL:
			if (pMouse) pMouse->handleEvent(e);
			break;
	}
}

void CInputManager::windowRect(int* x, int* y, int* width, int* height)
{
	SDL_GetWindowPosition(pWindow, x, y);
	SDL_GetWindowSize(pWindow, width, height);
}

CKeyboard* CInputManager::getKeyboard()
{
	if (pKeyboard == NULL) pKeyboard = new CKeyboard();
	return pKeyboard;
}

CMouse* CInputManager::getMouse()
{
	if (pMouse == NULL) pMouse = new CMouse(pWindow);
	return pMouse;
}
