//
//  IMouseListener.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__IMouseListener__
#define __Input__IMouseListener__

#include "../devices/sMouseState.h"

class IMouseListener
{
public:
	virtual void onMouseButtonDown(sMouseState state, eMouseButton button) {}
	virtual void onMouseButtonUp(sMouseState state, eMouseButton button) {}
	virtual void onMouseMove(sMouseState state) {}
	virtual void onMouseWheelScroll(sMouseState state) {}
};

#endif /* defined(__Input__IMouseListener__) */
