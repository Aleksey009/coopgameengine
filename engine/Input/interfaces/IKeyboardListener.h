//
//  IKeyboardListener.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__IKeyboardListener__
#define __Input__IKeyboardListener__

#include "../devices/sKeyboardState.h"

#include <string>

class IKeyboardListener
{
public:
	virtual void onKeyboardButtonDown(sKeyboardState state, unsigned short button) {}
	virtual void onKeyboardButtonUp(sKeyboardState state, unsigned short button) {}
	virtual void onKeyboardTextInput(sKeyboardState state, wchar_t character) {}
};

#endif /* defined(__Input__IKeyboardListener__) */
