//
//  CKeyboard.h
//  Input
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Input__CKeyboard__
#define __Input__CKeyboard__

#include "../common/CSpeaker.h"
#include "../common/defines.h"

#include "./interfaces/IKeyboardListener.h"

#include "../../third_party/sdl/SDL_events.h"

/** Класс ввода с клавиатуры */
class INPUT_API CKeyboard : public CSpeaker<IKeyboardListener>
{
public:
	CKeyboard();

	/**
	 Обработка события ввода
	 @param e событие библиотеки SDL
	 */
	virtual void handleEvent(SDL_Event* e);
	/**
	 Получает текущее состояние
	 @return состояние
	 */
	virtual sKeyboardState getState();
	
	/**
	 Начинает захват текстового ввода
	 */
	virtual void startCaptureTextInput();
	/**
	 Прекращает захват текстового ввода
	 */
	virtual void stopCaptureTextInput();
	
private:
	virtual void notify(IKeyboardListener* listener, void* info);

protected:
	virtual void notifyKeyDown(unsigned short key);
	virtual void notifyKeyUp(unsigned short key);
	virtual void notifyTextInput(wchar_t character);

	sKeyboardState state;
};

#endif /* defined(__Input__CKeyboard__) */
