//
//  CFactory.h
//  Core
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __Core__CFactory__
#define __Core__CFactory__

#include "../common/defines.h"

#include "../common/CSingleton.h"

/** Фабрика классов, создает требуемые классы */
class CORE_API CFactory : public CSingleton<CFactory>
{
	friend class CSingleton<CFactory>;

	CFactory();
	virtual ~CFactory();
public:
	
	
private:
};

#endif /* defined(__Core__CFactory__) */
