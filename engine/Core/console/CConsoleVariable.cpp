//
//  CConsoleVariable.cpp
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#include "CConsoleVariable.h"
#include "CConsole.h"

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <wchar.h>

CConsoleVariable<int> intV(NULL, L"hidden");
CConsoleVariable<float> floatV(NULL, L"hidden");

template <> void CConsoleVariable<int>::handle(CConsole *console, int argc, wchar_t* argv[]) {
	if (argc == 0) {
		console->log(L"%d", *data);
	} else {
		*data = (int)wcstol(argv[0], NULL, 10);
	}
}

template <> void CConsoleVariable<float>::handle(CConsole *console, int argc, wchar_t* argv[]) {
	if (argc == 0) {
		console->log(L"%f", *data);
	} else {
		*data = wcstof(argv[0], NULL);
	}
}

template <> void CConsoleVariable<std::wstring>::handle(CConsole *console, int argc, wchar_t* argv[]) {
	if (argc == 0) {
		console->log(L"%ls", data);
	} else {
		std::wstring s;

		for (int i = 0; i < argc; i++) {
			s += argv[i];
			s += L" ";
		}

		s.erase(s.end() - 1);

		*data = s;
	}
}