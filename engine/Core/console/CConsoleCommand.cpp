//
//  CConsoleCommand.cpp
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#include "CConsoleCommand.h"

CConsoleCommand::CConsoleCommand(std::function<void(CConsole *, int, wchar_t**)> func, std::wstring desc) : CConsoleSymbol(desc)
{
	name = func;
}

void CConsoleCommand::handle(CConsole *console, int argc, wchar_t *argv[]) {
	name(console, argc, argv);
}