//
//  CConsoleSymbol.h
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#ifndef __Core__CConsoleSymbol__
#define __Core__CConsoleSymbol__

#include "../../common/defines.h"

#include <string>

class CConsole;

/** Базовый класс хранимых консолью "символов" - переменные и команды */
class CORE_API CConsoleSymbol
{
public:
	/**
	 @param desc описание "символа"
	*/
	CConsoleSymbol(std::wstring desc) : description(desc) {}

	/**
	Получает описание "символа"
	@return описание
	*/
	virtual std::wstring getDescription() { return description; }
	/** 
	 Обрабатывает введенную с консоли строку
	 @param argc количество аргументов
	 @param argv массив аргументов
	*/
	virtual void handle(CConsole *console, int argc, wchar_t *argv[]) = 0;

protected:
	/** Описание символа */
	std::wstring description;
};

#endif /* defined(__Core__CConsoleSymbol__) */