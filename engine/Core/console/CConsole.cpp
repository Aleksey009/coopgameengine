//
//  CConsole.cpp
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#define _CRT_SECURE_NO_WARNINGS

#include "CConsole.h"

#include <wchar.h>
#include <stdarg.h>

CConsole* CConsole::registerSymbol(std::wstring name, CConsoleSymbol *symbol) {
	symbols.insert(std::make_pair(name, symbol));
	return this;
}

bool CConsole::unregisterSymbol(std::wstring name) {
	if (symbols.find(name) == symbols.end()) {
		return false;
	}

	symbols.erase(name);
	return true;
}

bool CConsole::getSymbolDescription(std::wstring name, std::wstring &dest) {
	if (symbols.find(name) == symbols.end()) {
		return false;
	}

	dest = symbols[name]->getDescription();
	return true;
}

bool CConsole::execute(std::wstring cmd) {
	// trim string
	cmd.erase(0, cmd.find_first_not_of(' '));
	cmd.erase(cmd.find_last_not_of(' ') + 1);

	if (cmd.empty()) {
		return false;
	}

	int argc = 0;
	wchar_t **argv = NULL;

	std::wstring params;
	std::size_t found = cmd.find_first_of(L" ");

	if (found != std::wstring::npos) {
		params = cmd.substr(found + 1);
		cmd = cmd.substr(0, found);
	}

	if (symbols.find(cmd) == symbols.end()) {
		log(L"%ls: command not found.\n", cmd.c_str());
		return false;
	}

	if (params.length() > 0) {
		// get arguments count
		argc = 1;

		for (size_t i = params.length(); i > 0; i--) {
			if (params[i] == L' ') {
				argc++;
			}
		}

		// create array
		argv = new wchar_t *[argc];
		for (int i = 0; i < argc; i++) {
			argv[i] = new wchar_t[CONSOLE_PARAM_MAX_LEN];
		}

		// parse params string into array
		size_t pos = 0;
		int current_num = 0;
		while ((pos = params.find(' ')) != std::string::npos) {
			wcscpy(argv[current_num], params.substr(0, pos).c_str());
			argv[current_num][CONSOLE_PARAM_MAX_LEN - 1] = L'\0';
			params.erase(0, pos + 1);
			current_num++;
		}
		wcscpy(argv[current_num], params.c_str());
		argv[current_num][CONSOLE_PARAM_MAX_LEN - 1] = L'\0';
	}

	symbols[cmd]->handle(this, argc, argv);
	return true;
}

void CConsole::addLogString(std::wstring str)
{
	for (int i = CONSOLE_LOG_MAX_CELLS - 1; i > 0; i--) {
		logArray[i] = logArray[i - 1];
	}

	logArray[0] = str;

	logCount++;

	if (logCount >= CONSOLE_LOG_MAX_CELLS) {
		logCount = CONSOLE_LOG_MAX_CELLS;
	}

	notifyAll(NULL);
}

std::wstring CConsole::getLogString(int num)
{
	if (num < 0 || num >= CONSOLE_LOG_MAX_CELLS) {
		return L"";
	}
	return logArray[num];
}

int CConsole::getLogStringCount()
{
	return logCount;
}

void CConsole::log(const wchar_t *format, ...)
{
	wchar_t temp[64];
	va_list args;

	va_start(args, format);

	vswprintf(temp, 64, format, args);
	addLogString(temp);

	va_end(args);
}

void CConsole::notify(IConsoleListener* listener, void* info)
{
	listener->onConsoleLogChanged(this);
}