//
//  IConsoleListener.h
//  Console
//
//  Created by Марочкин Сергей on 12.09.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#ifndef __Input__IConsoleListener__
#define __Input__IConsoleListener__

class CConsole;

class IConsoleListener
{
public:
	virtual void onConsoleLogChanged(CConsole* sender) {}
};

#endif /* defined(__Input__IConsoleListener__) */ 
