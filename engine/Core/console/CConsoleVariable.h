//
//  CConsoleVariable.h
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#ifndef __Core__CConsoleVariable__
#define __Core__CConsoleVariable__

#include "../../common/defines.h"

#include "CConsoleSymbol.h"

/** Переменная консоли */
template <class T> class CORE_API CConsoleVariable: public CConsoleSymbol {
public:
	/** 
	 @param var указатель на переменную
	 @param desc описание переменной
	*/
	CConsoleVariable(T *var, std::wstring desc): CConsoleSymbol(desc) {
		data = var;
	}

	virtual void handle(CConsole *console, int argc, wchar_t *argv[]);

protected:
	/** Указатель на переменную */
	T *data;
};

#endif /* defined(__Core__CConsoleVariable__) */