//
//  CConsoleCommand.h
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#ifndef __Core__CConsoleCommand__
#define __Core__CConsoleCommand__

#include "../../common/defines.h"

#include "CConsoleSymbol.h" 

#include <functional>

/** Команда консоли */
class CORE_API CConsoleCommand: public CConsoleSymbol {
public:
	/**
	 @param func вызываемая командой функция
	 @param desc описание команды
	*/
	CConsoleCommand(std::function<void(CConsole *, int, wchar_t**)> func, std::wstring desc);
	
	virtual void handle(CConsole *console, int argc, wchar_t *argv[]);

protected:
	/** Вызываемая командой функция */
	std::function<void(CConsole *, int, wchar_t**)> name;
};

#endif /* defined(__Core__CConsoleCommand__) */