//
//  CConsole.h
//  Core
//
//  Created by Марочкин Сергей on 26.08.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#ifndef __Core__CConsole__
#define __Core__CConsole__

#include "../../common/CSpeaker.h"
#include "../../common/defines.h"

#include "IConsoleListener.h"

#include "CConsoleSymbol.h"

#include <map>
#include <string>
#include <cstring>
#include <iostream>

/** Максимальная длина параметра консоли */
#define CONSOLE_PARAM_MAX_LEN	16
/** Максимальное количество ячеек лога */
#define CONSOLE_LOG_MAX_CELLS	100

class IConsoleListener;

/** Класс консоли */
class CORE_API CConsole: public CSpeaker<IConsoleListener>
{
public:
	/** 
	 Регистрирует символ консоли
	 @param name имя по которому будут обращения к символу
	 @param symbol указатель на символ
	 @return сам себя
	*/
	virtual CConsole* registerSymbol(std::wstring name, CConsoleSymbol *symbol);
	/**
	 Удаляет символ консоли
	 @param name имя по которому шли обращения к символу
	 @return true - символ найден и удален
	*/
	bool unregisterSymbol(std::wstring name);
	/**
	 Получает описание символа консоли
	 @param name имя по которому обращаются к символу
	 @param desc ссылка на строку, в которую запишется описание
	 @return true - символ найден
	*/
	bool getSymbolDescription(std::wstring name, std::wstring &dest);
	/**
	 Записывает введённую команду в память и лог-файл
	 @param str строка для записи
	*/
	void addLogString(std::wstring str);
	/**
	 Возвращает строку лога по номеру
	 @param num номер записи
	 @return str - строка лога
	*/
	std::wstring getLogString(int num);
	/**
	 Возвращает количество строк в логе
	 @return int количество строк в логе
	 */
	int getLogStringCount();
	/**
	 Выполняет "символ"
	 @param cmd текст обращения к "символу"
	 @return true - символ найден и выполнен
	*/
	bool execute(std::wstring cmd);
	/**
	 Форматирует и записывает строку в лог
	 @param format формат строки
	 @param ... параметры форматированной строки строки
	*/
	void log(const wchar_t *format, ...);

private:
	void notify(IConsoleListener* listener, void* info);

protected:
	/** Карта имен символов и соответствующих классов */
	std::map<std::wstring, CConsoleSymbol*> symbols;
	/** Массив строк для лога */
	std::wstring logArray[CONSOLE_LOG_MAX_CELLS];
	/** Количество записей в логе */
	int logCount = 0;
}; 

#endif /* defined(__Core__CConsole__) */