//
//  CLinkedList.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_CLinkedList_h
#define CoopGameEngine_CLinkedList_h

#include <stddef.h>

/** @todo сделать итератор */
template <class T> class CLinkedList
{
public:
	struct sLinkedListNode {
		T pData;
		sLinkedListNode* pNext;
	};
	
	typedef sLinkedListNode* iterator;
	
	CLinkedList()
	{
		pRoot = NULL;
	}
	
	void add(T data)
	{
		sLinkedListNode* newNode = new sLinkedListNode;
		newNode->pData = data;
		newNode->pNext = NULL;
		
		sLinkedListNode** node = &pRoot;
		
		while((*node) != NULL) node = &((*node)->pNext);
		
		*node = newNode;
	}
	
	void remove(T data)
	{
		sLinkedListNode** node = &pRoot;
		
		while((*node) != NULL) {
			if((*node)->pData == data) {
				sLinkedListNode* todelete = (*node);
				
				(*node) = todelete->pNext;
				
				delete todelete;
			}
			else node = &(*node)->pNext;
		}
	}
	
	iterator begin() {
		return pRoot;
	}
	
private:
	sLinkedListNode* pRoot;
};

#endif
