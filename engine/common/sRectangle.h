//
//  sRectangle.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_sRectangle_h
#define CoopGameEngine_sRectangle_h

#include "sPoint.h"
#include "sSize.h"

template <typename T = int> struct sRectangle
{
	sPoint<T> position;
	sSize<T> size;
	
	bool havePoint(T x, T y) {
		return ((x > position.x) &&
				(y > position.y) &&
				(x < position.x + size.width) &&
				(y < position.y + size.height));
	}
	
	bool isIntersect(sRectangle<T> other) {
		T AX1 = position.x;
		T AX2 = position.x + size.width;
		T AY1 = position.y;
		T AY2 = position.y + size.height;
		
		T BX1 = other.position.x;
		T BX2 = other.position.x + other.size.width;
		T BY1 = other.position.y;
		T BY2 = other.position.y + other.size.height;
		
		if((AX1 > BX2) || (AX2 < BX1) || (AY1 > BY2) || (AY2 < BY1)) return false;
		else return true;
	}
};

#endif
