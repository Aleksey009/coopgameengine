//
//  sColor.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_sColor_h
#define CoopGameEngine_sColor_h

struct sColor
{
	float r;
	float g;
	float b;
	float a;
};

#endif
