//
//  sSize.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_sSize_h
#define CoopGameEngine_sSize_h

template <typename T = int> struct sSize
{
	T width;
	T height;
};

#endif
