//
//  CSpeaker.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 21.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __CoopGameEngine__CSpeaker__
#define __CoopGameEngine__CSpeaker__

#include <stddef.h>

/** Шаблон класса имеющего слушателей */
template <class T> class CSpeaker
{
	class CSpeakerNode
	{
		friend class CSpeaker<T>;

		CSpeakerNode() : next(0) {}

		CSpeakerNode* next;
		T* listener;
	};

public:
	CSpeaker() : head(0) {}

	~CSpeaker() {
		CSpeakerNode* node = head;
		CSpeakerNode* next = 0;
		while (node != 0) {
			next = node->next;

			delete node;

			node = next;
		}
	}

	void addListener(T* listener)
	{
		CSpeakerNode* node = new CSpeakerNode();

		node->listener = listener;
		node->next = head;
		head = node;
	}

	void removeListener(T* listener)
	{
		CSpeakerNode* node = head;
		CSpeakerNode* prev = 0;
		while (node != 0) {
			if (node->listener == listener) {
				if (prev != 0) prev->next = node->next;
				else head = node->next;

				delete node;

				node = prev;
			}

			prev = node;
			node = node->next;
		}
	}

protected:
	void notifyAll(void* info)
	{
		CSpeakerNode* node = head;
		while (node != 0) {
			notify(node->listener, info);
			node = node->next;
		}
	}

	virtual void notify(T* listener, void* info) {}

private:

	CSpeakerNode* head;
};

#endif /* defined(__CoopGameEngine__CSpeaker__) */
