//
//  CSingleton.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_CSingleton_h
#define CoopGameEngine_CSingleton_h

#include <stddef.h>

/** Родитель классов одиночек */
template <class T> class CSingleton
{
public:
	/**
	 Получить экземпляр класса-одиночки
	 @return единственный на все приложение экземпляр
	 */
	static T* getInstance()
	{
		if(pInstance == NULL) pInstance = new T();
		return pInstance;
	}
	
protected:
	CSingleton()
	{
	}
	~CSingleton()
	{
		if(pInstance) pInstance = NULL;
	}
	
	static T* pInstance;
};

template<class T> T* CSingleton<T>::pInstance = NULL;

#endif
