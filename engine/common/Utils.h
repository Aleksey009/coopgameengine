//
//  Utils.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 24.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_Utils_h
#define CoopGameEngine_Utils_h

#define INDEX4(A,B) (A*4 + B)
#define INDEX3(A,B) (A*3 + B)
#define INDEX2(A,B) (A*2 + B)

#endif
