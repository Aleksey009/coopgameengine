//
//  sPoint.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 03.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_sPoint_h
#define CoopGameEngine_sPoint_h

template <typename T = int> struct sPoint
{
	T x;
	T y;
};

#endif
