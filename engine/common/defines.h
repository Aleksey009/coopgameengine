//
//  defines.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 18.07.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_defines_h
#define CoopGameEngine_defines_h

#include <stddef.h>

#ifdef _WIN32

	#ifdef RENDER_EXPORTS
		#define RENDER_API __declspec(dllexport)
	#else
		#define RENDER_API __declspec(dllimport)
	#endif

	#ifdef CORE_EXPORTS
		#define CORE_API __declspec(dllexport)
	#else
		#define CORE_API __declspec(dllimport)
	#endif

	#ifdef INPUT_EXPORTS
		#define INPUT_API  __declspec(dllexport)
	#else
		#define INPUT_API __declspec(dllimport)
	#endif

#else
	#define RENDER_API
	#define CORE_API
	#define INPUT_API
#endif

#endif
