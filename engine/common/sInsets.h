//
//  sInsets.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 06.09.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_sInsets_h
#define CoopGameEngine_sInsets_h

template <typename T = int> struct sInsets
{
	T left;
	T top;
	T right;
	T bottom;
};

#endif
