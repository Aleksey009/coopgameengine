//
//  CRefCounter.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 09.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef CoopGameEngine_CRefCounter_h
#define CoopGameEngine_CRefCounter_h

/** Родитель классов с подсчетом ссылок */
class CRefCounter
{
public:
	CRefCounter():referenceCount(0) {}
	
	/**
	 Увеличивает количество ссылок на данный объект
	 */
	virtual void incCounter()
	{
		referenceCount++;
	}
	/**
	 Умешьнает количество ссылок на данный объект
	 @return false - если ссылок больше нет (количество ссылок = 0)
	 */
	virtual bool decCounter()
	{
		referenceCount--;
		return (referenceCount != 0);
	}
	
private:
	/** Количество ссылок на данный объект */
	unsigned long referenceCount;
};

#endif
