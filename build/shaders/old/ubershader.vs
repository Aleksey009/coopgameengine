in vec3 inVertexNormal;
in vec3 inVertexPos;
in vec2 inVertexUV;
in vec4 inVertexColor;

out vec3 normal;
out vec2 UV;
out vec4 color;

uniform mat4 uViewProjection; // mesh.xml + gbuffer.xml
uniform mat4 uModel;

void main() {
  normal = normalize(uViewProjection * inVertexNormal);
  vec4 tpos = uModel * vec4(inVertexPos, 1);
  gl_Position = uViewProjection * tpos;

  UV = inVertexUV;
  color = inVertexColor;
}
