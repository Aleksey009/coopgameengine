#version 330 core

in vec2 inVertexUV;
in vec4 inVertexColor;

out vec2 UV;
out vec4 color;

void main()
{
  UV = inVertexUV;
  color = inVertexColor;
}
