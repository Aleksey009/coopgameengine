in vec3 normal;
in vec2 UV;
in vec4 color;

out vec4 (location=0) albedoBuffer;
out vec4 (location=1) normalBuffer;
out vec4 outColor;

uniform sampler2D uTexture2d;

void main() {
  outColor = color * texture(uTexture2d, UV).rgba;
  albedoBuffer = outColor;
  normalBuffer = vec4(normal, 1);
}
