//
// Fragment Shader for deferred shading pass 1
//
varying vec3 pos;
varying vec3 n;

void main (void)
{
    vec3    n2   = normalize ( n );

    gl_FragData [0] = vec4 ( pos, gl_FragDepth );
    gl_FragData [1] = vec4 ( 0.5*n2 + vec3(0.5), 1.0 );
}
