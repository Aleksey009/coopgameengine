#version 330 core

in vec3 inVertexNormal;

out vec3 normal;

uniform mat4 uViewProjection;

void main()
{
  normal = normalize(inVertexNormal);
}
