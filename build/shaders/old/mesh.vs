#version 330 core

in vec3 inVertexPos;

uniform mat4 uViewProjection;
uniform mat4 uModel;

void main()
{
  vec4 tpos = uModel * vec4(inVertexPos, 1);
  gl_Position = uViewProjection * tpos;
}
