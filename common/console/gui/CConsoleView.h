﻿//
//  CConsoleView.h
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#ifndef __CoopGameEngine__CGUICollectionView__
#define __CoopGameEngine__CGUICollectionView__

#include "../../../engine/Render/gui/CGUIView.h"
#include "../../../engine/Render/gui/CGUITextfieldView.h"
#include "../../../engine/Render/gui/CGUICollectionViewDataSource.h"
#include "../../../engine/Core/console/IConsoleListener.h"

class CConsole;
class CGUICollectionView;
class CGUICollectionViewLayout;

/** Графический интерфейс консоли */
class CConsoleView : public CGUIView, CGUITextfieldView::IGUITextfieldViewListener, CGUICollectionViewDataSource, IConsoleListener
{
public:
	CConsoleView();
	virtual ~CConsoleView();
	
	/**
	 Изменяет закрепленный за интерфейсом экземпляр консоли
	 @param console указатель на консоль
	 @return сам себя
	*/
	virtual CConsoleView* setConsole(CConsole* console);
	/**
	 Получает закрепленный за интерфейсом экземпляр консоли
	 @return указатель на консоль
	*/
	virtual CConsole* getConsole();

	virtual void onInputCompleted(CGUITextfieldView* sender);
	virtual bool onInputCharacter(CGUITextfieldView* sender, wchar_t character);

	virtual void onConsoleLogChanged(CConsole* sender);

	virtual int getItemsCount();
	virtual CGUICollectionViewCell* getItemView(int itemid, CGUICollectionViewCell* reusableView);
	virtual sSize<> getItemSize(int itemid, int width, int height);
	virtual sInsets<> getItemInsets(int itemid);
	
	virtual CGUIView* setHidden(bool hidden);

protected:
	/** Указатель на экземпляр консоли */
	CConsole* pConsole;
	/** Поле ввода команд */
	CGUITextfieldView* pTextInputView;
	/** Коллекция сообщений консоли */
	CGUICollectionView* pLogView;
	/** Лэйаут коллекции */
	CGUICollectionViewLayout* pLogLayout;
};

#endif /* defined(__CoopGameEngine__CGUICollectionView__) */
