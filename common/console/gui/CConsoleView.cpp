﻿//
//  CConsoleView.cpp
//  CoopGameEngine
//
//  Created by Михайлов Алексей on 28.08.14.
//  Copyright (c) 2014 Aleksey.Mikhailov.ru@ya.ru. All rights reserved.
//

#include "CConsoleView.h"

#include "../../../engine/Render/gui/CGUITextfieldView.h"
#include "../../../engine/Render/gui/CGUICollectionView.h"
#include "../../../engine/Render/gui/CGUICollectionViewCell.h"
#include "../../../engine/Render/gui/CGUICollectionViewListLayout.h"
#include "../../../engine/Render/gui/CGUILabelView.h"

#include "../../../engine/Core/console/CConsole.h"

class CConsoleLogCell: public CGUICollectionViewCell
{
public:
	CConsoleLogCell() {
		pLabel = new CGUILabelView();
		pLabel->
			setLayoutSizeFlags(LAYOUT_SIZE_HEIGHT_MATH_PARENT | LAYOUT_SIZE_WIDTH_MATH_PARENT);
		
		addSubview(pLabel);
	}
	
	virtual ~CConsoleLogCell() {
		if(pLabel) delete pLabel;
	}
	
	virtual void setText(const wchar_t* text) {
		if(pLabel) pLabel->setText(text);
	}
	
private:
	CGUILabelView* pLabel;
};

CConsoleView::CConsoleView() : CGUIView()
{
	pConsole = NULL;
	
	pTextInputView = new CGUITextfieldView();
	pLogView = new CGUICollectionView();
	pLogLayout = new CGUICollectionViewListLayout();
	
	pTextInputView->
		setListener(this)->
		setReadonly(false)->
		setMultiline(false)->
		setHintText(L"Input command here...")->
		setBackgroundColor(1.0f, 1.0f, 1.0f, 0.9f)->
		setFrame({ {0, 0}, {0, 30} })->
		setLayoutAlignmentFlags(LAYOUT_ALIGNMENT_BOTTOM | LAYOUT_ALIGNMENT_LEFT)->
		setLayoutSizeFlags(LAYOUT_SIZE_WIDTH_MATH_PARENT);

	pLogView->
		setLayout(pLogLayout)->
		setDataSource(this)->
		setScrollBarsThinkness(10)->
		setVerticalScrollBarShowMode(SCROLL_BAR_SHOW_WHEN_SCROLLABLE)->
		setVerticalScrollBarColor(0, 0, 0, 1.0f)->
		setFrame({ { 0, 30 }, { 0, 30 } })->
		setLayoutAlignmentFlags(LAYOUT_ALIGNMENT_BOTTOM | LAYOUT_ALIGNMENT_LEFT)->
		setLayoutSizeFlags(LAYOUT_SIZE_WIDTH_MATH_PARENT | LAYOUT_SIZE_HEIGHT_MATH_PARENT);

	addSubview(pLogView);
	addSubview(pTextInputView);
	
	setBackgroundColor(1.0f, 1.0f, 1.0f, 0.8f);
	setFrame({ { 0, 0 }, { 0, 200 } });
	setLayoutAlignmentFlags(LAYOUT_ALIGNMENT_TOP | LAYOUT_ALIGNMENT_LEFT);
	setLayoutSizeFlags(LAYOUT_SIZE_WIDTH_MATH_PARENT);
}

CConsoleView::~CConsoleView()
{
	if (pTextInputView) delete pTextInputView;
	if (pLogView) delete pLogView;
	if (pLogLayout) delete pLogLayout;
}

CConsoleView* CConsoleView::setConsole(CConsole* console)
{
	this->pConsole = console;
	console->addListener(this);
	return this;
}

CConsole* CConsoleView::getConsole()
{
	return this->pConsole;
}

void CConsoleView::onInputCompleted(CGUITextfieldView* sender)
{
	if (pConsole) {
		std::wstring cmd = sender->getText();

		pConsole->addLogString(L"> " + cmd);
		pConsole->execute(cmd);

		sender->setText(L"");
	}
}

bool CConsoleView::onInputCharacter(CGUITextfieldView* sender, wchar_t character)
{
	if (character == L'`' || character == L'ё' || CGUIView::isHidden()) {
		return false;
	}
	return true;
}

void CConsoleView::onConsoleLogChanged(CConsole* sender)
{
	pLogView->reloadData();
}

int CConsoleView::getItemsCount()
{
	if (!pConsole) {
		return 0;
	}
	return pConsole->getLogStringCount();
}

CGUICollectionViewCell* CConsoleView::getItemView(int itemid, CGUICollectionViewCell* reusableView)
{
	CConsoleLogCell* cell = (CConsoleLogCell*)reusableView;
	if(cell == NULL) {
		cell = new CConsoleLogCell();
	}

	const wchar_t *temp = pConsole->getLogString(itemid).c_str();

	cell->setText(temp);
	
	return cell;
}

sSize<> CConsoleView::getItemSize(int itemid, int width, int height)
{
	sSize<> size;
	size.width = width;
	size.height = 20;
	
	return size;
}

sInsets<> CConsoleView::getItemInsets(int itemid)
{
	static sInsets<> insets;
	insets.left = 4;
	insets.top = 2;
	insets.right = 4;
	insets.bottom = 2;
	
	return insets;
}

CGUIView* CConsoleView::setHidden(bool hidden)
{
	CGUIView::setHidden(hidden);

	if (!hidden) {
		pTextInputView->setFocused();
	}
	return this;
}
