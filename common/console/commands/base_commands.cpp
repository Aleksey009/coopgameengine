//
//  base_commands.cpp
//  common
//
//  Created by Марочкин Сергей on 12.09.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#include "base_commands.h"
#include "../../../engine/Core/console/CConsoleCommand.h"

void base_register_commands(CConsole *console)
{
	console->
		registerSymbol(L"quit", new CConsoleCommand(quit, L"Quit."))->
		registerSymbol(L"help", new CConsoleCommand(help, L"Help with commands."))->
		registerSymbol(L"version", new CConsoleCommand(version, L"Show engine version."));
}

void quit(CConsole *console, int argc, wchar_t *argv[]) {
	exit(1);
}

void help(CConsole *console, int argc, wchar_t *argv[]) {
	if (argc == 0) {
		console->log(L"Usage: help <command>\n");
	}

	std::wstring desc;

	for (int i = 0; i < argc; i++) {
		if (wcslen(argv[i]) == 0) {
			continue;
		}

		if (!console->getSymbolDescription(argv[i], desc)) {
			console->log(L"%ls: command not found.\n", argv[i]);
		} else {
			console->log(L"%ls: %ls\n", argv[i], desc.c_str());
		}
	}
}

void version(CConsole *console, int argc, wchar_t *argv[]) {
	console->log(L"CoopGameEngine version ZERO");
}