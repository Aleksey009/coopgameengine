//
//  base_commands.р
//  common
//
//  Created by Марочкин Сергей on 12.09.14.
//  Copyright (c) 2014 xziggix@gmail.com. All rights reserved.
//

#include "../../../engine/Core/console/CConsole.h"

void base_register_commands(CConsole *console);
void quit(CConsole *console, int argc, wchar_t *argv[]);
void help(CConsole *console, int argc, wchar_t *argv[]);
void version(CConsole *console, int argc, wchar_t *argv[]);